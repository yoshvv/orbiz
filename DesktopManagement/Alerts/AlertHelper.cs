﻿using DesktopManagement.Properties;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;

namespace DesktopManagement.Alerts
{
    public enum AlertType 
    {
        Sucess,
        Error,
        Warning
    }

    public class AlertHelper
    {
        public static void SuccessAlert(string msg) 
        {
            Alert("Sucess", msg, new Bitmap(Resources.sucess_icon));
        }
       
        public static void WarningAlert(string msg)
        {
            Alert("Warning", msg, new Bitmap(Resources.warning_icon));
        }

        public static void ErrorAlert(string msg)
        {
            Alert("Error", msg, new Bitmap(Resources.error_icon));
        }


        public static bool YesNoAlert(string msg) 
        {
            var radMessage = new YesNoMessage(msg);
            radMessage.ShowDialog();
            var isOk = radMessage.IsOk;
            radMessage.Dispose();

            if (isOk) 
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        private static void  Alert(string title, string msg, Bitmap icon)
        {
            var radMessage = RadMessageBox.Instance;
            radMessage.Text = title;
            radMessage.MessageText = msg;
            radMessage.MessageIcon = icon;
            radMessage.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            radMessage.ButtonsConfiguration = MessageBoxButtons.OK;
            radMessage.ThemeName = Properties.Settings.Default.Theme;
            radMessage.MinimumSize = new Size(200,100);
            radMessage.AutoSizeMode = AutoSizeMode.GrowOnly;
            radMessage.ShowDialog();
        }
    }
}
