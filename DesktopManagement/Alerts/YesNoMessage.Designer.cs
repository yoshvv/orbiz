﻿namespace DesktopManagement.Alerts
{
    partial class YesNoMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YesNoMessage));
            this.material = new Telerik.WinControls.Themes.MaterialTheme();
            this.OK = new Telerik.WinControls.UI.RadButton();
            this.Cancel = new Telerik.WinControls.UI.RadButton();
            this.lbl_message = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_message)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(157, 75);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(120, 36);
            this.OK.TabIndex = 1;
            this.OK.Text = "OK";
            this.OK.ThemeName = "Material";
            this.OK.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(292, 75);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(120, 36);
            this.Cancel.TabIndex = 2;
            this.Cancel.Text = "Cancel";
            this.Cancel.ThemeName = "Material";
            this.Cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // lbl_message
            // 
            this.lbl_message.AutoSize = false;
            this.lbl_message.Location = new System.Drawing.Point(13, 13);
            this.lbl_message.Name = "lbl_message";
            this.lbl_message.Size = new System.Drawing.Size(399, 56);
            this.lbl_message.TabIndex = 2;
            this.lbl_message.Text = "message";
            this.lbl_message.ThemeName = "Material";
            // 
            // YesNoMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 138);
            this.Controls.Add(this.lbl_message);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(426, 175);
            this.MinimumSize = new System.Drawing.Size(426, 175);
            this.Name = "YesNoMessage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(426, 175);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confirmation message";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.YesNoMessage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_message)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme material;
        private Telerik.WinControls.UI.RadButton OK;
        private Telerik.WinControls.UI.RadButton Cancel;
        private Telerik.WinControls.UI.RadLabel lbl_message;
    }
}
