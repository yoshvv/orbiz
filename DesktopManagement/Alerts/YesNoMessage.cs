﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace DesktopManagement.Alerts
{
    public partial class YesNoMessage : Telerik.WinControls.UI.RadForm
    {
        public YesNoMessage(string msg)
        {
            InitializeComponent();
            this.lbl_message.Text = msg;

        }

        public bool IsOk = false;

        private void  btn_ok_Click(object sender, EventArgs e)
        {
            IsOk = true;
            this.Dispose();
        }

        private void  btn_cancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void  YesNoMessage_Load(object sender, EventArgs e)
        {

        }
    }
}
