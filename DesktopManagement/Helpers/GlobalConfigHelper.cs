﻿using DesktopManagement.Modules;
using DesktopManagement.Resx;
using Logic.Modules.Configuration;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    public class GlobalConfigHelper
    {
        private GlobalConfigHelper() 
        {
            this.configurationLogic = new GlobalConfigurationLogic();
            this.imageHelper = new Helpers.FileHelper();
        }

        private readonly GlobalConfigurationLogic configurationLogic;

        private readonly Helpers.FileHelper imageHelper;

        private static GlobalConfigHelper Instance { get; set; }

        public static GlobalConfigHelper GetInstance() 
        {
            if (Instance == null)
            {
                Instance = new GlobalConfigHelper();
            }

            return Instance;
        }

        public void SetAppName(Main main)
        {
            var currentConfig = this.configurationLogic.GetCurrentConfigurationOrDefault(Language.lbl_App_Title);
            main.Text = currentConfig.AppName;
            main.lbl_title.Text = currentConfig.AppName;
        }

        public void SetAppIcon(RadForm page)
        {
            var currentConfig = this.configurationLogic.GetConfiguration();

            if (currentConfig == null) currentConfig = this.configurationLogic.DefaultConfiguration();

            page.Icon = imageHelper.ArrayOfBytesToIco(currentConfig.ICO.File);
        }
    }
}
