﻿using DesktopManagement.Base;
using DesktopManagement.Properties;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    public class ComponentHelper
    {
        public ComponentHelper() { }

        /// <summary>
        /// Set a list of strings in a component
        /// </summary>
        /// <param name="list"></param>
        /// <param name="component"></param>
        public void  BindData(List<string> list, object component) 
        {
            //Binding source
            BindingSource bs = new BindingSource();
            bs.DataSource = list;

            if (component is RadGridView)
            {
                ((RadGridView)component).DataSource = bs;
            }
            else if (component is RadDropDownList) 
            {
                ((RadDropDownList)component).DataSource = bs;
            }
        }

        /// <summary>
        /// Checks if user is still typing
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public async Task<bool> UserKeepsTyping(RadTextBox textBox)
        {
            string txt = textBox.Text;   // remember text
            await Task.Delay(500);        // wait some
            return txt != textBox.Text;  // return that text chaged or not
        }

        /// <summary>
        /// Set the default options to a RadGridView
        /// </summary>
        /// <param name="grid"></param>
        public void  SetDefaultGridConfig(RadGridView grid)
        {
            grid.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            //grid.BestFitColumns(BestFitColumnMode.AllCells);
            grid.MasterTemplate.AutoGenerateColumns = false;
            grid.MasterTemplate.AllowAddNewRow = false;
            grid.MasterTemplate.AllowColumnChooser = false;
            grid.MasterTemplate.AllowDeleteRow = false;
            grid.MasterTemplate.AllowDragToGroup = false;
            grid.MasterTemplate.AllowEditRow = false;
            grid.MasterTemplate.AutoExpandGroups = true;

            grid.ShowGroupPanel = false;
            //Column text align center
            foreach (var col in grid.MasterTemplate.Columns) 
            {
                if (col is GridViewDateTimeColumn) col.FormatString = Configuration.GetDateFormat();
                if (col is GridViewDecimalColumn) 
                {
                    col.FormatInfo = Configuration.GetCurrencyCulture();
                    col.FormatString = "{0:C}";
                } 
                (col as GridViewDataColumn).TextAlignment = ContentAlignment.MiddleCenter;
            }
        }

        /// <summary>
        /// Set the custom search icon image in a RadTextBox
        /// </summary>
        /// <param name="txt"></param>
        public void  SetSearchIcon(RadTextBox txt, int width = 0, int height = 0)
        {
            ImagePrimitive searchIcon = new ImagePrimitive();
            searchIcon.Image = Resources.search_icon;
            searchIcon.Alignment = ContentAlignment.MiddleRight;
            //Set the icon
            txt.TextBoxElement.Children.Add(searchIcon);
            //Set the inner textboxitem size
            txt.TextBoxElement.TextBoxItem.Alignment = ContentAlignment.MiddleLeft;
            txt.TextBoxElement.TextBoxItem.StretchHorizontally = false;
            var finalWidth = width == 0 ? txt.Width : width;
            txt.TextBoxElement.TextBoxItem.HostedControl.MinimumSize = new Size(finalWidth - searchIcon.Image.Width - 20, 0);
            txt.TextBoxElement.TextBoxItem.HostedControl.MaximumSize = new Size(finalWidth - searchIcon.Image.Width, 0);
        }
    }
}
