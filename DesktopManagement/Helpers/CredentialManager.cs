﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopManagement.Helpers
{
    public static class CredentialManager
    {
        /// <summary>
        /// Gloval variable to get the current IdUser
        /// </summary>
        public static int IdUser { get; set; }
    }
}
