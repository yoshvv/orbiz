﻿using DesktopManagement.Properties;
using Logic.Modules.Configuration;
using Persistence.Models.Files;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    public enum FileType 
    {
        /// <summary>
        ///Only images .jpg and .png
        /// </summary>
        Images,
        /// <summary>
        /// Only PDF files
        /// </summary>
        Files,
        /// <summary>
        /// Eveything
        /// </summary>
        All
    }

    public class FileHelper
    {
        public byte[] ToArray(Image img)
        {
            //Convert the image to an array of byes
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(img, typeof(byte[]));
            return xByte;
        }

        /// <summary>
        /// Image to an array of bytes
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public FileData ImageToArrayOfBytes(Image img, FileData file)
        {
            if (file != null)
            {
                //Set the array in the FileData
                file.File = ToArray(img);

                return file;
            }
            else
            {
                //Null value
                return new FileData()
                {
                    File = null,
                    FileName = "",
                    Type = ""
                };
            }
        }

        /// <summary>
        /// Check if the file is null to return the default value for the FileData object
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public FileData ImageToFile(FileData file)
        {
            if (file != null)
            {
                return file;
            }
            else
            {
                //Null value
                return new FileData()
                {
                    File = null,
                    FileName = "",
                    Type = ""
                };
            }
        }

        /// <summary>
        /// Convert the array of bytes in an image, but if the array
        /// is null retrieve the default avatar's image
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public Image GetAvatar(byte[] array)
        {
            if (array == null)
            {
                return Resources.no_image;
            }
            else
            {
                return ArrayOfBytesToImage(array);
            }
        }

        /// <summary>
        /// Array of bytes to an Image
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image ArrayOfBytesToImage(byte[] array)
        {
            Image img = (Bitmap)((new ImageConverter()).ConvertFrom(array));
            return img;
        }

        public Icon ArrayOfBytesToIco(byte[] array)
        {
            if (array == null) return null;

            using (MemoryStream ms = new MemoryStream(array))
            {
                return new Icon(ms);
            }
        }

        /// <summary>
        /// Resize an image
        /// </summary>
        /// <param name="img"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public Image ResizeImage(Image img, int width, int height)
        {
            return (Image)(new Bitmap(img, new Size(width, height)));
        }

        public Image ResizeImage(Image img, PictureBox pictureBox)
        {
            return ResizeImage(img, pictureBox.Width, pictureBox.Height);
        }

        /// <summary>
        /// Open a dialog to upload an image (.png or .jpg) from the user's computer
        /// </summary>
        /// <returns></returns>
        public List<FileData> FilesFromPC(FileType type, bool multiSelect = false) 
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            var imageFilter = "Images (*.PNG;*.JPG)|*.PNG;*.JPG";
            var fileFilter = "Files (*.PDF;)|*.PDF;";
            var allFilter = string.Join("|",new List<string> { "All files (*.*)|*.*", imageFilter, fileFilter});

            openFileDialog.Filter = type == FileType.Images ? imageFilter : type == FileType.Files ? fileFilter : allFilter;

            // Allow the user to select multiple images.
            openFileDialog.Multiselect = multiSelect;
            openFileDialog.Title = "Search for files";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Files from computer
                var files = openFileDialog.FileNames.ToList();

                //Files converted to FileData
                var fileDataList =
                files.Select(x => new FileData()
                {
                    File = File.ReadAllBytes(x),
                    FileName = x.Split('\\').Last(),
                    Type = x.Split('.').Last()
                })
                .ToList();

                return fileDataList;
            }
            else 
            {
                return null;
            }
        }

        /// <summary>
        /// Open a dialog to upload an ico (.ico) from the user's computer
        /// </summary>
        /// <returns></returns>
        public FileData IcoFromPC()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            var imageFilter = "Images (*.ICO) | *.ICO";
            
            openFileDialog.Filter = imageFilter;

            // Allow the user to select multiple images.
            openFileDialog.Multiselect = false;
            openFileDialog.Title = "Search for icons";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Files from computer
                var file = openFileDialog.FileNames.First();

                //Files converted to FileData
                return new FileData()
                {
                    File = File.ReadAllBytes(file),
                    FileName = file.Split('\\').Last(),
                    Type = file.Split('.').Last()
                };
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Load an imagen from the computer into a PictureBox control
        /// and returns the image as a <see cref="FileData"/>
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public FileData LoadImageInPictureBox(PictureBox img) 
        {
            var files = FilesFromPC(FileType.Images);

            if (files != null)
            {
                var file = files.FirstOrDefault();
                img.Image = ResizeImage(ArrayOfBytesToImage(file.File), img.Width, img.Height);
                return file;
            }

            return null;
        }

        public FileData LoadIcoInPictureBox(PictureBox img)
        {
            var file = IcoFromPC();

            if (file != null)
            {
                img.Image = ResizeImage(ArrayOfBytesToImage(file.File), img);
                return file;
            }

            return null;
        }

        public FileData ClearPictureBox(PictureBox img) 
        {
            //Clean the picture box
            img.Image = Resources.no_image;
            return null;
        }

        public void SetLogotype(PictureBox picture)
        {
            var globalConfigLogic = new GlobalConfigurationLogic();
            var currentConfig = globalConfigLogic.GetConfiguration();

            Image logo = null;

            if (currentConfig != null && currentConfig.Logo.File != null)
            {
                logo = ArrayOfBytesToImage(currentConfig.Logo.File);
            }
            else 
            {
                logo = Properties.Resources.Logo;
            }

            picture.Image = ResizeImage(logo, picture.Width, picture.Height);
        }

        public byte[] GetLogo()
        {
            var globalConfigLogic = new GlobalConfigurationLogic();
            var currentConfig = globalConfigLogic.GetConfiguration();

            if (currentConfig != null && currentConfig.Logo.File != null)
            {
                return currentConfig.Logo.File;
            }
            else
            {
                return ToArray(Properties.Resources.Logo);
            }
        }
    }
}
