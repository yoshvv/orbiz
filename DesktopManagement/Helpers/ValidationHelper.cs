﻿using System;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    public class ValidationHelper
    {
        public bool IsValidEmail(RadMaskedEditBox txt)
        {
            bool valid = string.IsNullOrEmpty(txt.Text) || (txt.MaskedEditBoxElement.Provider as EMailMaskTextBoxProvider).Validate(txt.Text);
            return valid;
        }

        public bool IsValidDate(RadMaskedEditBox txt) 
        {
            //Removing the characters of the phone mask
            var txtCleaned = txt.Text
                .Replace(" ", "")
                .Replace("_", "");

            //Is empty if removing that last mask character
            var isEmpty = string.IsNullOrEmpty(txtCleaned.Replace("-", ""));
           
            //Default datetime null value
            var nullDate = default(DateTime);

            var times = txtCleaned.Replace("-","/");
            DateTime date = nullDate;
            DateTime.TryParse(times, out date);

            return isEmpty || (date != nullDate);
        }

        public bool IsValidPhoneNumber(RadMaskedEditBox txt)
        {
            return IsPhoneNumber(txt.Text);
        }

        public bool IsValidPhoneNumber(RadTextBox txt) 
        {
            return IsPhoneNumber(txt.Text);
        }

        private bool IsPhoneNumber(string txt) 
        {
            //Removing the characters of the phone mask
            var txtCleaned = txt
                .Replace(" ", "")
                .Replace("(","")
                .Replace(")", "")
                .Replace("-", "")
                .Replace("_", "");

            return string.IsNullOrEmpty(txtCleaned) || txtCleaned.Length == 10;
        }
    }
}