﻿using DesktopManagement.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    /// <summary>
    /// Class to handle the different convertions between types
    /// </summary>
    public class ParseHelper
    {
        public ParseHelper() { }

        public int ToInt(string txt)
        {
            if (string.IsNullOrEmpty(txt) || string.IsNullOrWhiteSpace(txt))
            {
                return 0;
            }
            else 
            {
                return int.Parse(txt);
            }
        }

        public decimal ToDecimal(string txt) 
        {
            var culture = Configuration.CurrencySymbol();
            var txtClean = txt.Replace(culture, "").Replace("%","");

            if (string.IsNullOrEmpty(txtClean) || string.IsNullOrWhiteSpace(txtClean))
            {
                return 0;
            }
            else
            {
                var num = txtClean.Split('.');
                var number = decimal.Parse(num[0].Replace(",", ""));

                var numDec = num.Count() > 1 ? decimal.Parse(num[1])/100 : 0;
                var ret = number + numDec;
                return ret;
            }
        }

        public DateTime? ToDateTime(RadMaskedEditBox txt) 
        {
            var defaultTime = default(DateTime);
            DateTime time = defaultTime;

            DateTime.TryParse(txt.Text, out time);

            if (time == defaultTime)
            {
                return null;
            }
            else 
            {
                return time;
            }
        }

        public DateTime? ToDateTime(RadDateTimePicker date) 
        {

            if (date.Value == default(DateTime))
            {
                return null;
            }
            else
            {
                return date.Value;
            }
        }
    }
}
