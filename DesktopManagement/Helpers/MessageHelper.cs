﻿using DesktopManagement.Alerts;
using DesktopManagement.Resx;

namespace DesktopManagement.Helpers
{
    public static class MessageHelper
    {
        public static void WihoutPermissionToAddMessage(string entity)
        {
            AlertHelper.WarningAlert(string.Format(Language.message_Without_Permission_Add, entity));
        }

        public static void WihoutPermissionToEditMessage(string entity)
        {
            AlertHelper.WarningAlert(string.Format(Language.message_Without_Permission_Edit, entity));
        }

        public static void WihoutPermissionToDeleteMessage(string entity)
        {
            AlertHelper.WarningAlert(string.Format(Language.message_Without_Permission_Delete, entity));
        }

        public static string NotEmptyString(string entity) 
        {
            return string.Format(Language.message_Cannot_Be_Empty, entity);
        }
    }
}
