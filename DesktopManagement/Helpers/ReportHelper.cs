﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopManagement.Helpers
{
    public static class ReportHelper
    {
        public static bool DateTimeIsNull(DateTime? d) => (d == null || d == default(DateTime));

        public static string ReportPath(string directory, string reportName) 
        {
            //Get path of the RDLC file
            string exeFolder = Path.GetDirectoryName(Application.ExecutablePath).ToLower().Replace(@"\bin", "").Replace(@"\debug", "");
            string reportPath = Path.Combine(exeFolder, @"" + directory + reportName + ".rdlc");
            return reportPath;
        }

    }
}
