﻿using DesktopManagement.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI;

namespace DesktopManagement.Helpers
{
    public class MenuHelper
    {
        public MenuHelper() 
        {
            this.imageHelper = new FileHelper();
        }

        private FileHelper imageHelper { get; }


        /// <summary>
        /// Get the custom icon from a menu item given a name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Image GetMenuImage(string name)
        {
            var img = Resources.default_icon;

            switch (name)
            {
                case "Article":
                    img = Resources.article_icon;
                    break;
                case "Trainers":
                    img = Resources.weight_icon;
                    break;
                case "Security":
                    img = Resources.security_icon;
                    break;
                case "PettyCash":
                    img = Resources.cash_icon;
                    break;
                case "Members":
                    img = Resources.member;
                    break;
                case "Payment":
                    img = Resources.payment;
                    break;
                case "FrontDesk":
                    img = Resources.front_desk_icon;
                    break;
                case "Configuration":
                    img = Resources.configuration_icon;
                    break;
                case "Subscriptions":
                    img = Resources.credential_icon;
                    break;
                case "Supplier":
                    img = Resources.supplier_icon;
                    break;
                case "Vouchers":
                    img = Resources.voucher;
                    break;
                case "Series":
                    img = Resources.serie_icon;
                    break;
                case "Reports":
                    img = Resources.report;
                    break;

            }
            return imageHelper.ResizeImage(img, 30, 30);
        }
    }
}