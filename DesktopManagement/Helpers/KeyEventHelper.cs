﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopManagement.Helpers
{
    public static class KeyEventHelper
    {
        /// <summary>
        /// True if the user is pressing a key to saving changes
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool IsSaving(KeyEventArgs e) 
        {
            //if the user press the key combination Ctrl + S
            //or press enter
            return (e.Modifiers == Keys.Control && e.KeyCode == Keys.S) ||
                   (e.Modifiers == Keys.Enter);
        }
        
        public static bool IsSaving(KeyPressEventArgs e)
        {
            //Enter key pressed
            return e.KeyChar == (char)13;
        }

        public static bool IsSaving(PreviewKeyDownEventArgs e)
        {
            //if the user press the key combination Ctrl + S
            //or press enter
            return (e.Modifiers == Keys.Control && e.KeyCode == Keys.S) ||
                   (e.Modifiers == Keys.Enter);
        }
    }
}
