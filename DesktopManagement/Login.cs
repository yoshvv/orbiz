﻿using DesktopManagement.Alerts;
using DesktopManagement.Helpers;
using DesktopManagement.Modules;
using DesktopManagement.Resx;
using Logic.Modules;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace DesktopManagement
{
    public partial class Login : RadForm
    {
        public Login()
        {
            InitializeComponent();
            this.logic = new LoginLogic();
            this.fileHelper = new FileHelper();
            Text = Language.lbl_Login;
            lbl_userName.Text = Language.lbl_UserName;
            lbl_password.Text = Language.lbl_Password;
            btn_login.Text = Language.btn_Login;
            SetSysVersion();
            fileHelper.SetLogotype(this.img_logo);
            RefreshIcon();
        }

        private readonly LoginLogic logic;

        private FileHelper fileHelper { get; }

        public void RefreshIcon()
        {
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
        }

        public void SetSysVersion() 
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            this.lbl_version.Text = string.Format(Language.lbl_Version, fvi.FileVersion);
        }

        private void Login_LoadAsync(object sender, EventArgs e)
        {
            logic.CheckSuperAdmin();
        }

        public void ExitAll()
        {
            this.Dispose();
            RestartApplication();
        }

        public void RestartApplication() 
        {
            Process.Start($@"{Settings.RestartApp}");
        }

        private void DoLogin() 
        {
            var userID = logic.CheckLogin(txt_UserName.Text, txt_Password.Text);

            if (userID == 0)
            {
                AlertHelper.WarningAlert(Language.error_User_Incorrect);
            }
            else
            {
                CleanUserData();
                HideLogin();
                CredentialManager.IdUser = userID;
                var main = new Main(this);
                main.Show();
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            DoLogin();
        }

        private void CleanUserData() 
        {
            this.txt_UserName.Text = "";
            this.txt_Password.Text = "";
        }

        private void HideLogin()
        {
            this.Visible = false;
        }

        public void ShowLogin()
        {
            this.RefreshIcon();
            this.Visible = true;
            this.txt_UserName.Focus();
        }

        private void txt_UserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                DoLogin();
            }
        }

        private void txt_Password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                DoLogin();
            }
        }
    }
}
