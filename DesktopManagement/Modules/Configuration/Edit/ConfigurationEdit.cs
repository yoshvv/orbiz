﻿using DesktopManagement.Alerts;
using DesktopManagement.Helpers;
using DesktopManagement.Resx;
using Logic.Modules.Configuration;
using Persistence;
using Persistence.Models.Files;
using System;
using Entity = Persistence.Models.Configuration.GlobalConfiguration;

namespace DesktopManagement.Modules.Configuration.Edit
{
    public partial class ConfigurationEdit : Telerik.WinControls.UI.RadForm
    {
        public ConfigurationEdit(Main main)
        {
            InitializeComponent();
            this.main = main;
            this.logic = new GlobalConfigurationLogic();
            this.imageHelper = new Helpers.FileHelper();
            this.parseHelper = new ParseHelper();
            SetStrings();
            LoadExisting();
        }

        public string EntityName => Language.lbl_Configuration;

        private readonly Main main;

        private GlobalConfigurationLogic logic { get;  }

        private Entity Entity { get; set; }

        /* Logotype */
        private Helpers.FileHelper imageHelper { get; }

        private ParseHelper parseHelper { get; }

        private FileData Logo { get; set; } = null;

        private FileData Icon_img { get; set; } = null;

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_Edit, EntityName);
            lbl_logotype.Text = Language.lbl_Logotype;
            lbl_icon.Text = Language.lbl_Icon_For_Windows;
            lbl_applicationName.Text = Language.lbl_App_Name;
            btn_save.Text = Language.btn_Save;
        }

        public void LoadExisting()
        {
            //get current global configuration
            Entity config = logic.GetConfiguration();

            //if configuration is null we're creatin a default one
            if (config == null) 
            {
                logic.AddDefaultConfiguration();
                config = logic.GetConfiguration();
            }

            this.Entity = config;

            RefreshWindow();
        }

        public void RefreshWindow() 
        {
            txt_appName.Text = Entity.AppName;
            Logo = Entity.Logo;
            Icon_img = Entity.ICO;
            img_logo.Image = imageHelper.ResizeImage(imageHelper.GetAvatar(Logo.File), img_logo.Width,img_logo.Height);
            img_icon.Image = imageHelper.ResizeImage(imageHelper.GetAvatar(Icon_img.File), img_icon.Width, img_icon.Height);

            this.Icon = imageHelper.ArrayOfBytesToIco(Icon_img.File);
        }

        public void Save() 
        {
            using (var c = new Context()) 
            {
                if (!logic.ExistsConfiguration())
                    logic.AddDefaultConfiguration();

                var config = logic.GetConfiguration(c);

                config.AppName = txt_appName.Text;
                config.Logo = this.imageHelper.ImageToArrayOfBytes(this.img_logo.Image, this.Logo);
                config.ICO = this.imageHelper.ImageToFile(Icon_img);

                logic.Update(c,config);

                AlertHelper.SuccessAlert($"Configuration updated sucessfully!");
                main.RefreshIcon();
                Dispose();
            }
        }

        public bool DoValidation()
        {
            //If all the validations have passed it means we can save
            return true;
        }

        public void DoValidationAndSave()
        {
            if (DoValidation())
            {
                Save();
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            DoValidationAndSave();
        }

        private void btn_addLogo_Click(object sender, EventArgs e)
        {
            Logo = imageHelper.LoadImageInPictureBox(img_logo);
        }

        private void btn_removeLogo_Click(object sender, EventArgs e)
        {
            //Clean the picture box
            this.Logo = imageHelper.ClearPictureBox(img_logo);
        }

        private void btn_addIcon_Click(object sender, EventArgs e)
        {
            Icon_img = imageHelper.LoadIcoInPictureBox(img_icon);
        }

        private void btn_removeIcon_Click(object sender, EventArgs e)
        {
            this.Icon_img = imageHelper.ClearPictureBox(img_icon);
        }
    }
}
