﻿namespace DesktopManagement.Modules.Configuration.Edit
{
    partial class ConfigurationEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationEdit));
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.lbl_logotype = new Telerik.WinControls.UI.RadLabel();
            this.btn_addLogo = new Telerik.WinControls.UI.RadButton();
            this.img_logo = new System.Windows.Forms.PictureBox();
            this.btn_removeLogo = new Telerik.WinControls.UI.RadButton();
            this.lbl_icon = new Telerik.WinControls.UI.RadLabel();
            this.btn_addIcon = new Telerik.WinControls.UI.RadButton();
            this.img_icon = new System.Windows.Forms.PictureBox();
            this.btn_removeIcon = new Telerik.WinControls.UI.RadButton();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btn_save = new Telerik.WinControls.UI.RadButton();
            this.lbl_applicationName = new Telerik.WinControls.UI.RadLabel();
            this.txt_appName = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_logotype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_applicationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_appName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_logotype
            // 
            this.lbl_logotype.Location = new System.Drawing.Point(12, 196);
            this.lbl_logotype.Name = "lbl_logotype";
            this.lbl_logotype.Size = new System.Drawing.Size(67, 21);
            this.lbl_logotype.TabIndex = 23;
            this.lbl_logotype.Text = "Logotype";
            this.lbl_logotype.ThemeName = "Material";
            // 
            // btn_addLogo
            // 
            this.btn_addLogo.AutoSize = true;
            this.btn_addLogo.Image = global::DesktopManagement.Properties.Resources.icons8_add_image_64;
            this.btn_addLogo.Location = new System.Drawing.Point(185, 196);
            this.btn_addLogo.Name = "btn_addLogo";
            this.btn_addLogo.Size = new System.Drawing.Size(27, 27);
            this.btn_addLogo.TabIndex = 20;
            this.btn_addLogo.Click += new System.EventHandler(this.btn_addLogo_Click);
            // 
            // img_logo
            // 
            this.img_logo.Image = global::DesktopManagement.Properties.Resources.no_image;
            this.img_logo.Location = new System.Drawing.Point(12, 12);
            this.img_logo.Name = "img_logo";
            this.img_logo.Size = new System.Drawing.Size(200, 178);
            this.img_logo.TabIndex = 22;
            this.img_logo.TabStop = false;
            // 
            // btn_removeLogo
            // 
            this.btn_removeLogo.AutoSize = true;
            this.btn_removeLogo.Image = global::DesktopManagement.Properties.Resources.trash_icon;
            this.btn_removeLogo.Location = new System.Drawing.Point(147, 196);
            this.btn_removeLogo.Name = "btn_removeLogo";
            this.btn_removeLogo.Size = new System.Drawing.Size(27, 27);
            this.btn_removeLogo.TabIndex = 21;
            this.btn_removeLogo.Click += new System.EventHandler(this.btn_removeLogo_Click);
            // 
            // lbl_icon
            // 
            this.lbl_icon.Location = new System.Drawing.Point(255, 196);
            this.lbl_icon.Name = "lbl_icon";
            this.lbl_icon.Size = new System.Drawing.Size(119, 21);
            this.lbl_icon.TabIndex = 27;
            this.lbl_icon.Text = "Icon for Windows";
            this.lbl_icon.ThemeName = "Material";
            // 
            // btn_addIcon
            // 
            this.btn_addIcon.AutoSize = true;
            this.btn_addIcon.Image = global::DesktopManagement.Properties.Resources.icons8_add_image_64;
            this.btn_addIcon.Location = new System.Drawing.Point(428, 196);
            this.btn_addIcon.Name = "btn_addIcon";
            this.btn_addIcon.Size = new System.Drawing.Size(27, 27);
            this.btn_addIcon.TabIndex = 24;
            this.btn_addIcon.Click += new System.EventHandler(this.btn_addIcon_Click);
            // 
            // img_icon
            // 
            this.img_icon.Image = global::DesktopManagement.Properties.Resources.no_image;
            this.img_icon.Location = new System.Drawing.Point(255, 12);
            this.img_icon.Name = "img_icon";
            this.img_icon.Size = new System.Drawing.Size(200, 178);
            this.img_icon.TabIndex = 26;
            this.img_icon.TabStop = false;
            // 
            // btn_removeIcon
            // 
            this.btn_removeIcon.AutoSize = true;
            this.btn_removeIcon.Image = global::DesktopManagement.Properties.Resources.trash_icon;
            this.btn_removeIcon.Location = new System.Drawing.Point(390, 196);
            this.btn_removeIcon.Name = "btn_removeIcon";
            this.btn_removeIcon.Size = new System.Drawing.Size(27, 27);
            this.btn_removeIcon.TabIndex = 25;
            this.btn_removeIcon.Click += new System.EventHandler(this.btn_removeIcon_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radPanel1.Controls.Add(this.btn_save);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 318);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(473, 48);
            this.radPanel1.TabIndex = 35;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(335, 6);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(120, 36);
            this.btn_save.TabIndex = 17;
            this.btn_save.Text = "Save";
            this.btn_save.ThemeName = "Material";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // lbl_applicationName
            // 
            this.lbl_applicationName.Location = new System.Drawing.Point(12, 247);
            this.lbl_applicationName.Name = "lbl_applicationName";
            this.lbl_applicationName.Size = new System.Drawing.Size(122, 21);
            this.lbl_applicationName.TabIndex = 45;
            this.lbl_applicationName.Text = "Application Name";
            this.lbl_applicationName.ThemeName = "Material";
            // 
            // txt_appName
            // 
            this.txt_appName.Location = new System.Drawing.Point(12, 269);
            this.txt_appName.Name = "txt_appName";
            this.txt_appName.Size = new System.Drawing.Size(441, 36);
            this.txt_appName.TabIndex = 44;
            this.txt_appName.ThemeName = "Material";
            // 
            // ConfigurationEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 366);
            this.Controls.Add(this.lbl_applicationName);
            this.Controls.Add(this.txt_appName);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.lbl_icon);
            this.Controls.Add(this.btn_addIcon);
            this.Controls.Add(this.img_icon);
            this.Controls.Add(this.btn_removeIcon);
            this.Controls.Add(this.lbl_logotype);
            this.Controls.Add(this.btn_addLogo);
            this.Controls.Add(this.img_logo);
            this.Controls.Add(this.btn_removeLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(467, 321);
            this.Name = "ConfigurationEdit";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.lbl_logotype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_applicationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_appName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadLabel lbl_logotype;
        private Telerik.WinControls.UI.RadButton btn_addLogo;
        private System.Windows.Forms.PictureBox img_logo;
        private Telerik.WinControls.UI.RadButton btn_removeLogo;
        private Telerik.WinControls.UI.RadLabel lbl_icon;
        private Telerik.WinControls.UI.RadButton btn_addIcon;
        private System.Windows.Forms.PictureBox img_icon;
        private Telerik.WinControls.UI.RadButton btn_removeIcon;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btn_save;
        private Telerik.WinControls.UI.RadLabel lbl_applicationName;
        private Telerik.WinControls.UI.RadTextBox txt_appName;
    }
}
