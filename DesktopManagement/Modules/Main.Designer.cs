﻿namespace DesktopManagement.Modules
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.PanelContainer = new Telerik.WinControls.UI.RadScrollablePanelContainer();
            this.pnl_menu = new Telerik.WinControls.UI.RadPanel();
            this.menu = new Telerik.WinControls.UI.RadTreeView();
            this.txt_filter = new Telerik.WinControls.UI.RadTextBox();
            this.pnl_backgroud = new Telerik.WinControls.UI.RadPanel();
            this.lbl_title = new Telerik.WinControls.UI.RadLabel();
            this.pnl_title = new Telerik.WinControls.UI.RadPanel();
            this.btn_profile = new Telerik.WinControls.UI.RadSplitButton();
            this.btn_viewProfile = new Telerik.WinControls.UI.RadMenuItem();
            this.pnl_background = new Telerik.WinControls.UI.RadPanel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.lbl_users = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lbl_usersCount = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_menu)).BeginInit();
            this.pnl_menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_filter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_backgroud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_title)).BeginInit();
            this.pnl_title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_profile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_background)).BeginInit();
            this.pnl_background.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_users)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_usersCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = false;
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.None;
            this.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.PanelContainer.Size = new System.Drawing.Size(157, 389);
            // 
            // pnl_menu
            // 
            this.pnl_menu.Controls.Add(this.menu);
            this.pnl_menu.Controls.Add(this.txt_filter);
            this.pnl_menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_menu.Location = new System.Drawing.Point(0, 0);
            this.pnl_menu.Name = "pnl_menu";
            this.pnl_menu.Size = new System.Drawing.Size(300, 777);
            this.pnl_menu.TabIndex = 0;
            this.pnl_menu.ThemeName = "Material";
            // 
            // menu
            // 
            this.menu.ItemHeight = 36;
            this.menu.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(209)))), ((int)(((byte)(209)))));
            this.menu.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.menu.Location = new System.Drawing.Point(4, 42);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(293, 602);
            this.menu.TabIndex = 1;
            this.menu.Text = "radTreeView1";
            this.menu.ThemeName = "Material";
            this.menu.NodeMouseDoubleClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.menu_NodeMouseDoubleClick);
            // 
            // txt_filter
            // 
            this.txt_filter.Location = new System.Drawing.Point(0, 0);
            this.txt_filter.Name = "txt_filter";
            this.txt_filter.Size = new System.Drawing.Size(300, 36);
            this.txt_filter.TabIndex = 0;
            this.txt_filter.ThemeName = "Material";
            this.txt_filter.TextChanged += new System.EventHandler(this.txt_filter_TextChanged);
            // 
            // pnl_backgroud
            // 
            this.pnl_backgroud.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_backgroud.Location = new System.Drawing.Point(300, 0);
            this.pnl_backgroud.Name = "pnl_backgroud";
            this.pnl_backgroud.Size = new System.Drawing.Size(1051, 777);
            this.pnl_backgroud.TabIndex = 1;
            this.pnl_backgroud.ThemeName = "Material";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = false;
            this.lbl_title.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.lbl_title.Location = new System.Drawing.Point(199, 0);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(784, 57);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "title";
            this.lbl_title.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lbl_title.ThemeName = "Material";
            // 
            // pnl_title
            // 
            this.pnl_title.Controls.Add(this.btn_profile);
            this.pnl_title.Controls.Add(this.lbl_title);
            this.pnl_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_title.Location = new System.Drawing.Point(300, 0);
            this.pnl_title.Name = "pnl_title";
            this.pnl_title.Size = new System.Drawing.Size(1051, 60);
            this.pnl_title.TabIndex = 2;
            // 
            // btn_profile
            // 
            this.btn_profile.Image = global::DesktopManagement.Properties.Resources.avatar_icon;
            this.btn_profile.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btn_viewProfile});
            this.btn_profile.Location = new System.Drawing.Point(4, 4);
            this.btn_profile.Name = "btn_profile";
            this.btn_profile.Size = new System.Drawing.Size(189, 50);
            this.btn_profile.TabIndex = 2;
            this.btn_profile.Text = "User - Rol";
            this.btn_profile.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_profile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_profile.ThemeName = "Material";
            // 
            // btn_viewProfile
            // 
            this.btn_viewProfile.Name = "btn_viewProfile";
            this.btn_viewProfile.Text = "View profile";
            this.btn_viewProfile.Click += new System.EventHandler(this.btn_viewProfile_Click);
            // 
            // pnl_background
            // 
            this.pnl_background.Controls.Add(this.radLabel9);
            this.pnl_background.Controls.Add(this.radPanel2);
            this.pnl_background.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_background.Location = new System.Drawing.Point(300, 60);
            this.pnl_background.Name = "pnl_background";
            this.pnl_background.Size = new System.Drawing.Size(1051, 717);
            this.pnl_background.TabIndex = 3;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.radLabel9.Location = new System.Drawing.Point(10, 6);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(182, 44);
            this.radLabel9.TabIndex = 14;
            this.radLabel9.Text = "Dashboard";
            this.radLabel9.ThemeName = "Material";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.DarkKhaki;
            this.radPanel2.Controls.Add(this.lbl_users);
            this.radPanel2.Controls.Add(this.radLabel5);
            this.radPanel2.Controls.Add(this.lbl_usersCount);
            this.radPanel2.Location = new System.Drawing.Point(19, 56);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(184, 107);
            this.radPanel2.TabIndex = 5;
            // 
            // lbl_users
            // 
            this.lbl_users.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbl_users.ForeColor = System.Drawing.Color.White;
            this.lbl_users.Location = new System.Drawing.Point(117, 28);
            this.lbl_users.Name = "lbl_users";
            this.lbl_users.Size = new System.Drawing.Size(64, 19);
            this.lbl_users.TabIndex = 5;
            this.lbl_users.Text = "Members";
            this.lbl_users.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.radLabel5.ForeColor = System.Drawing.Color.White;
            this.radLabel5.Image = global::DesktopManagement.Properties.Resources.dashboard_member;
            this.radLabel5.Location = new System.Drawing.Point(63, 3);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(58, 63);
            this.radLabel5.TabIndex = 4;
            this.radLabel5.ThemeName = "Material";
            // 
            // lbl_usersCount
            // 
            this.lbl_usersCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.lbl_usersCount.ForeColor = System.Drawing.Color.White;
            this.lbl_usersCount.Location = new System.Drawing.Point(48, 69);
            this.lbl_usersCount.Name = "lbl_usersCount";
            this.lbl_usersCount.Size = new System.Drawing.Size(88, 24);
            this.lbl_usersCount.TabIndex = 3;
            this.lbl_usersCount.Text = "radLabel6";
            this.lbl_usersCount.ThemeName = "Material";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1351, 777);
            this.Controls.Add(this.pnl_background);
            this.Controls.Add(this.pnl_title);
            this.Controls.Add(this.pnl_backgroud);
            this.Controls.Add(this.pnl_menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Name of the application";
            this.ThemeName = "Material";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.ResizeEnd += new System.EventHandler(this.Main_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_menu)).EndInit();
            this.pnl_menu.ResumeLayout(false);
            this.pnl_menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_filter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_backgroud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_title)).EndInit();
            this.pnl_title.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_profile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_background)).EndInit();
            this.pnl_background.ResumeLayout(false);
            this.pnl_background.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_users)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_usersCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadScrollablePanelContainer PanelContainer;
        private Telerik.WinControls.UI.RadPanel pnl_menu;
        private Telerik.WinControls.UI.RadPanel pnl_backgroud;
        private Telerik.WinControls.UI.RadTreeView menu;
        private Telerik.WinControls.UI.RadTextBox txt_filter;
        private Telerik.WinControls.UI.RadPanel pnl_background;
        private Telerik.WinControls.UI.RadPanel pnl_title;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel lbl_users;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lbl_usersCount;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadSplitButton btn_profile;
        private Telerik.WinControls.UI.RadMenuItem btn_viewProfile;
        public Telerik.WinControls.UI.RadLabel lbl_title;
    }
}
