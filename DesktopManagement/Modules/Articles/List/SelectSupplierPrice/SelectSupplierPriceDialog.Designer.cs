﻿namespace DesktopManagement.Modules.Articles.List.SelectSupplierPrice
{
    partial class SelectSupplierPriceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectSupplierPriceDialog));
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.combo_supplier = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lbl_supplier = new Telerik.WinControls.UI.RadLabel();
            this.lbl_salePrice = new Telerik.WinControls.UI.RadLabel();
            this.txt_salePrice = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lbl_purchasePrice = new Telerik.WinControls.UI.RadLabel();
            this.txt_purchasePrice = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btn_save = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_supplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_salePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_salePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_purchasePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_purchasePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel9.ForeColor = System.Drawing.Color.Red;
            this.radLabel9.Location = new System.Drawing.Point(321, 14);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(13, 20);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "*";
            this.radLabel9.ThemeName = "Material";
            this.radLabel9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // combo_supplier
            // 
            this.combo_supplier.AutoSize = true;
            // 
            // combo_supplier.NestedRadGridView
            // 
            this.combo_supplier.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.combo_supplier.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_supplier.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.combo_supplier.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.combo_supplier.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.combo_supplier.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.combo_supplier.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.combo_supplier.EditorControl.MasterTemplate.EnableGrouping = false;
            this.combo_supplier.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.combo_supplier.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.combo_supplier.EditorControl.Name = "NestedRadGridView";
            this.combo_supplier.EditorControl.ReadOnly = true;
            this.combo_supplier.EditorControl.ShowGroupPanel = false;
            this.combo_supplier.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.combo_supplier.EditorControl.TabIndex = 0;
            this.combo_supplier.Location = new System.Drawing.Point(10, 40);
            this.combo_supplier.Name = "combo_supplier";
            this.combo_supplier.Size = new System.Drawing.Size(324, 36);
            this.combo_supplier.TabIndex = 29;
            this.combo_supplier.TabStop = false;
            this.combo_supplier.Text = "Select a supplier";
            this.combo_supplier.ThemeName = "Material";
            this.combo_supplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // lbl_supplier
            // 
            this.lbl_supplier.Location = new System.Drawing.Point(10, 13);
            this.lbl_supplier.Name = "lbl_supplier";
            this.lbl_supplier.Size = new System.Drawing.Size(60, 21);
            this.lbl_supplier.TabIndex = 30;
            this.lbl_supplier.Text = "Supplier";
            this.lbl_supplier.ThemeName = "Material";
            this.lbl_supplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // lbl_salePrice
            // 
            this.lbl_salePrice.Location = new System.Drawing.Point(12, 187);
            this.lbl_salePrice.Name = "lbl_salePrice";
            this.lbl_salePrice.Size = new System.Drawing.Size(72, 21);
            this.lbl_salePrice.TabIndex = 35;
            this.lbl_salePrice.Text = "Sale Price";
            this.lbl_salePrice.ThemeName = "Material";
            this.lbl_salePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // txt_salePrice
            // 
            this.txt_salePrice.Culture = new System.Globalization.CultureInfo("en");
            this.txt_salePrice.Location = new System.Drawing.Point(12, 214);
            this.txt_salePrice.Mask = "C";
            this.txt_salePrice.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txt_salePrice.Name = "txt_salePrice";
            this.txt_salePrice.Size = new System.Drawing.Size(322, 36);
            this.txt_salePrice.TabIndex = 34;
            this.txt_salePrice.TabStop = false;
            this.txt_salePrice.Text = "$0.00";
            this.txt_salePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_salePrice.ThemeName = "Material";
            this.txt_salePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // lbl_purchasePrice
            // 
            this.lbl_purchasePrice.Location = new System.Drawing.Point(12, 100);
            this.lbl_purchasePrice.Name = "lbl_purchasePrice";
            this.lbl_purchasePrice.Size = new System.Drawing.Size(105, 21);
            this.lbl_purchasePrice.TabIndex = 33;
            this.lbl_purchasePrice.Text = "Purchase Price";
            this.lbl_purchasePrice.ThemeName = "Material";
            this.lbl_purchasePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // txt_purchasePrice
            // 
            this.txt_purchasePrice.Culture = new System.Globalization.CultureInfo("en");
            this.txt_purchasePrice.Location = new System.Drawing.Point(12, 127);
            this.txt_purchasePrice.Mask = "C";
            this.txt_purchasePrice.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.txt_purchasePrice.Name = "txt_purchasePrice";
            this.txt_purchasePrice.Size = new System.Drawing.Size(322, 36);
            this.txt_purchasePrice.TabIndex = 32;
            this.txt_purchasePrice.TabStop = false;
            this.txt_purchasePrice.Text = "$0.00";
            this.txt_purchasePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_purchasePrice.ThemeName = "Material";
            this.txt_purchasePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radPanel1.Controls.Add(this.btn_save);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 263);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(348, 48);
            this.radPanel1.TabIndex = 36;
            this.radPanel1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(214, 7);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(120, 36);
            this.btn_save.TabIndex = 4;
            this.btn_save.Text = "Save";
            this.btn_save.ThemeName = "Material";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            this.btn_save.KeyDown += new System.Windows.Forms.KeyEventHandler(this.radLabel9_KeyDown);
            // 
            // SelectSupplierPriceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 311);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.lbl_salePrice);
            this.Controls.Add(this.txt_salePrice);
            this.Controls.Add(this.lbl_purchasePrice);
            this.Controls.Add(this.txt_purchasePrice);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.combo_supplier);
            this.Controls.Add(this.lbl_supplier);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(350, 348);
            this.MinimumSize = new System.Drawing.Size(350, 348);
            this.Name = "SelectSupplierPriceDialog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(350, 348);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_supplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_supplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_salePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_salePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_purchasePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_purchasePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadMultiColumnComboBox combo_supplier;
        private Telerik.WinControls.UI.RadLabel lbl_supplier;
        private Telerik.WinControls.UI.RadLabel lbl_salePrice;
        private Telerik.WinControls.UI.RadMaskedEditBox txt_salePrice;
        private Telerik.WinControls.UI.RadLabel lbl_purchasePrice;
        private Telerik.WinControls.UI.RadMaskedEditBox txt_purchasePrice;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btn_save;
    }
}
