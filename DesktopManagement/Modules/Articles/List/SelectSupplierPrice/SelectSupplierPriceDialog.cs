﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Resx;
using Logic.Modules.Suppliers;
using Persistence;
using Persistence.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace DesktopManagement.Modules.Articles.List.SelectSupplierPrice
{
    public partial class SelectSupplierPriceDialog : Telerik.WinControls.UI.RadForm
    {
        public SelectSupplierPriceDialog(bool isNew)
        {
            InitializeComponent();
            IsNew = isNew;
            this.supplierLogic = new SupplierLogic();
            this.parseHelper = new ParseHelper();
            this.componentHelper = new ComponentHelper();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            if (isNew) Entity.CreatedDate = DateTime.Now;
            SetStrings();
            this.Language();
            SetTitle();
            LoadSupplier();
        }

        private bool IsNew { get; set; }

        public string EntityName => Language.lbl_Supplier_Price;

        public IReadOnlyList<Supplier> Suppliers { get; set; }

        private SupplierLogic supplierLogic { get; }

        private ParseHelper parseHelper { get; }

        private ComponentHelper componentHelper { get; }

        public SupplierPriceDTO Entity { get; set; } = new SupplierPriceDTO();

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_Edit, EntityName);
            lbl_supplier.Text = Language.lbl_Supplier;
            combo_supplier.Text = Language.lbl_Select_Supplier;
            lbl_purchasePrice.Text = Language.lbl_Purchase_Price;
            lbl_salePrice.Text = Language.lbl_Sale_Price;
            btn_save.Text = Language.btn_Save;
        }

        public void  LoadSupplier()
        {
            using (var c = new Context())
            {
                //getting supplier list
                Suppliers = supplierLogic.Query();

                this.componentHelper.SetDefaultGridConfig(combo_supplier.EditorControl);

                combo_supplier.MultiColumnComboBoxElement.Columns.Add(new GridViewTextBoxColumn(uniqueName: "Code", fieldName: "Code"));
                combo_supplier.MultiColumnComboBoxElement.Columns.Add(new GridViewTextBoxColumn(uniqueName: "Name", fieldName: "Name"));
                combo_supplier.MultiColumnComboBoxElement.Columns.Add(new GridViewTextBoxColumn(uniqueName: "FullName", fieldName: "FullName"));

                combo_supplier.MultiColumnComboBoxElement.Columns["Name"].HeaderText = Language.lbl_Company_Name;

                combo_supplier.EditorControl.ReadOnly = true;
                combo_supplier.DropDownSizingMode = SizingMode.UpDownAndRightBottom;
                combo_supplier.DropDownMinSize = new Size(420, 300);

                //bind list to combo
                this.combo_supplier.BindDataToGrid(Suppliers);
                this.combo_supplier.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Set the current title depending if the user is
        /// adding or editing an entity
        /// </summary>
        public void  SetTitle()
        {
            if (IsNew)
            {
                this.Text = string.Format(Language.title_Add_New, EntityName);
            }
            else
            {
                this.Text = string.Format(Language.title_Edit_Existing, EntityName);
            }
        }

        public void  LoadExisting(SupplierPriceDTO entity)
        {
            this.Entity = entity;
            this.combo_supplier.Enabled = false;
            this.combo_supplier.SelectedIndex = this.Suppliers
                .Select((x, i) => new { Entity = x, Index = i })
                .Where(x => x.Entity.Code.ToString() == entity.SupplierCode)
                .Select(x => x.Index)
                .FirstOrDefault();

            this.txt_purchasePrice.Text = entity.PurchasePrice.ToString();
            this.txt_salePrice.Text = entity.SalePrice.ToString();
        }

        public void  Save()
        {
            this.Entity.SupplierCode = this.Suppliers[combo_supplier.SelectedIndex].Code.ToString();
            this.Entity.SupplierName = this.Suppliers[combo_supplier.SelectedIndex].Name;
            this.Entity.IdSupplier = this.Suppliers[combo_supplier.SelectedIndex].IdSupplier;
            this.Entity.PurchasePrice = parseHelper.ToDecimal(this.txt_purchasePrice.Text);
            this.Entity.SalePrice = parseHelper.ToDecimal(this.txt_salePrice.Text);

            //Close the window
            Dispose();
        }

        public bool DoValidation()
        {
            if (this.combo_supplier.SelectedIndex < 0) 
            {
                AlertHelper.WarningAlert(Language.validation_Must_Select_Supplier);
                return false;
            }

            if (parseHelper.ToDecimal(this.txt_purchasePrice.Text) <= 0)
            {
                AlertHelper.WarningAlert(Language.validation_Purchase_Price_Not_Zero);
                return false;
            }

            return true;
        }

        public void  DoValidationAndSave()
        {
            if (DoValidation())
            {
                Save();
            }
        }

        private void  btn_save_Click(object sender, EventArgs e)
        {
            DoValidationAndSave();
        }

        private void  radLabel9_KeyDown(object sender, KeyEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
                DoValidationAndSave();
        }
    }
}