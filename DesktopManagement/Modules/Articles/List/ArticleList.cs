﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Modules.Articles.Edit;
using DesktopManagement.Resx;
using Logic.Modules.Articles;
using Logic.Modules.Security;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Telerik.WinControls.UI;
using Entity = Persistence.Models.Article;

namespace DesktopManagement.Modules.Articles.List
{
    public partial class ArticleList : RadForm, IBaseList<Entity>
    {
        public ArticleList()
        {
            InitializeComponent();
            this.logic = new ArticleLogic();
            this.componentHelper = new ComponentHelper();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            this.componentHelper.SetDefaultGridConfig(grid_list);
            SetStrings();
            this.Language();
            SetFilters();
            LoadData();
            SetPermissions();
        }

        public string EntityName => Language.lbl_Articles;

        private ArticleLogic logic { get; }

        private ComponentHelper componentHelper { get; }

        /// <summary>
        /// List of elements of type <see cref="Entity"/>
        /// </summary>
        public IReadOnlyList<Entity> List { get; set; }

        /// <summary>
        /// Permissions for this entity
        /// </summary>
        public Permission Permissions { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_List, Language.lbl_Article);
            radGroup_Actions.Text = Language.lbl_Actions;
            radGroup_Filters.Text = Language.lbl_Filters;
            btn_add.Text = Language.btn_Add;
            btn_edit.Text = Language.btn_Edit;
            btn_delete.Text = Language.btn_Delete;
            lbl_name.Text = Language.lbl_Name;
            lbl_code.Text = Language.lbl_Code;
        }

        /// <summary>
        /// Set the current user permissions for this window
        /// </summary>
        /// <returns></returns>
        public void SetPermissions()
        {
            var logicPermissions = new PermissionLogic();
            this.Permissions = logicPermissions.GetLoggedUserPermissions(CredentialManager.IdUser);
        }

        public void SetFilters()
        {
            componentHelper.SetSearchIcon(txt_name);
            componentHelper.SetSearchIcon(txt_code);
        }

        public void LoadData()
        {
            var parameters = new ArticleParams()
            {
                Name = txt_name.Text,
                Code = txt_code.Text
            };

            this.List = logic.Query(parameters);

            grid_list.BindDataToGrid(List);
        }

        // <summary>
        /// Get the selected object of type <see cref="Entity"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity GetSelectedElement()
        {
            var rowSelected = this.grid_list.CurrentRow;

            //If nothing selected returns null
            if (rowSelected == null) return null;


            var selected = rowSelected.Cells["Code"].Value.ToString();

            var entitySelected = this.List
                .Where(x => x.Code.ToString() == selected)
                .FirstOrDefault();

            return entitySelected;
        }

        /// <summary>
        /// Open the add form for this entity
        /// </summary>
        /// <returns></returns>
        public void Add()
        {
            if (!Permissions.Article.Add)
            {
                AlertHelper.WarningAlert($"User with no permission to add {EntityName}");
                return;
            }

            new ArticleEdit(true).ShowDialog();
            LoadData();
        }

        /// <summary>
        /// Open the edit form
        /// </summary>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Edit()
        {
            if (!Permissions.Article.Modify)
            {
                AlertHelper.WarningAlert($"User with no permission to edit {EntityName}");
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert("There is no item selected!");
                return;
            }

            var idKey = selected.IdArticle;
            var editForm = new ArticleEdit(false);
            editForm.LoadExisting(idKey);
            editForm.ShowDialog();
            LoadData();
        }

        public void Remove()
        {
            if (!Permissions.Article.Remove)
            {
                AlertHelper.WarningAlert($"User with no permission to delete {EntityName}");
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert("There is no item selected!");
                return;
            }

            if (AlertHelper.YesNoAlert("Delete this element?"))
            {
                var idKey = selected.IdArticle;

                //if (!logic.CanDelete(idKey))
                //{
                //    AlertHelper.WarningAlert("Article cannot be deleted because he has many subscriptios and/or is paying a current one.");
                //    return;
                //}

                logic.Delete(idKey);
                AlertHelper.SuccessAlert("Element deleted sucessfully!");
                LoadData();
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            Add();
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            Remove();
        }

        private void grid_list_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }

        private async void txt_name_TextChanged(object sender, EventArgs e)
        {
            if (!await this.componentHelper.UserKeepsTyping((RadTextBox)sender))
            {
                LoadData();
            };
        }

        private async void txt_code_TextChanged(object sender, EventArgs e)
        {
            if (!await this.componentHelper.UserKeepsTyping((RadTextBox)sender))
            {
                LoadData();
            };
        }
    }
}
