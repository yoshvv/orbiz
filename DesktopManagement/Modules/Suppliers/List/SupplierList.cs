﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Modules.Suppliers.Edit;
using DesktopManagement.Resx;
using Logic.Modules.Security;
using Logic.Modules.Suppliers;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Telerik.WinControls.UI;
using Entity = Logic.Modules.Suppliers.SupplierDTO;

namespace DesktopManagement.Modules.Suppliers.List
{
    public partial class SupplierList : RadForm
    {
        public SupplierList()
        {
            InitializeComponent();
            this.logic = new SupplierLogic();
            this.componentHelper = new ComponentHelper();
            this.componentHelper.SetDefaultGridConfig(grid_list);
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            SetStrings();

            this.Language();
            SetFilters();
            LoadData();
            SetPermissions();
        }

        public string EntityName => Language.lbl_Suppliers;

        private SupplierLogic logic { get; }

        private ComponentHelper componentHelper { get; }

        /// <summary>
        /// List of elements of type <see cref="Entity"/>
        /// </summary>
        public IReadOnlyList<Entity> List { get; set; }

        /// <summary>
        /// Permissions for this entity
        /// </summary>
        public Permission Permissions { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_List, Language.lbl_Supplier);
            radGroup_Actions.Text = Language.lbl_Actions;
            radGroup_Filters.Text = Language.lbl_Filters;
            btn_add.Text = Language.btn_Add;
            btn_edit.Text = Language.btn_Edit;
            btn_delete.Text = Language.btn_Delete;
            lbl_name.Text = Language.lbl_Name;
            lbl_code.Text = Language.lbl_Code;
        }

        /// <summary>
        /// Set the current user permissions for this window
        /// </summary>
        /// <returns></returns>
        public void SetPermissions()
        {
            var logicPermissions = new PermissionLogic();
            this.Permissions = logicPermissions.GetLoggedUserPermissions(CredentialManager.IdUser);
        }

        public void  SetFilters()
        {
            componentHelper.SetSearchIcon(txt_name);
            componentHelper.SetSearchIcon(txt_code);
        }

        public void  LoadData()
        {
            var parameters = new SupplierParams()
            {
                Name = txt_name.Text,
                Code = txt_code.Text
            };

            this.List = logic.Query(parameters);

            grid_list.BindDataToGrid(List);
        }

        // <summary>
        /// Get the selected object of type <see cref="Entity"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity GetSelectedElement()
        {
            var rowSelected = this.grid_list.CurrentRow;

            //If nothing selected returns null
            if (rowSelected == null) return null;


            var selected = rowSelected.Cells["Code"].Value.ToString();

            var entitySelected = this.List
                .Where(x => x.Code.ToString() == selected)
                .FirstOrDefault();

            return entitySelected;
        }

        /// <summary>
        /// Open the add form for this entity
        /// </summary>
        /// <returns></returns>
        public void Add()
        {
            if (!Permissions.Supplier.Add)
            {
                MessageHelper.WihoutPermissionToAddMessage(EntityName);
                return;
            }

            new SupplierEdit(true).ShowDialog();
            LoadData();
        }

        /// <summary>
        /// Open the edit form
        /// </summary>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Edit()
        {
            if (!Permissions.Supplier.Modify)
            {
                MessageHelper.WihoutPermissionToEditMessage(EntityName);
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            var idKey = selected.IdSupplier;
            var editForm = new SupplierEdit(false);
            editForm.LoadExisting(idKey);
            editForm.ShowDialog();
            LoadData();
        }

        public void Remove()
        {
            if (!Permissions.Supplier.Remove)
            {
                MessageHelper.WihoutPermissionToDeleteMessage(EntityName);
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            if (AlertHelper.YesNoAlert(Language.message_Delete_Element))
            {
                var idKey = selected.IdSupplier;

                if (!logic.CanDelete(idKey))
                {
                    AlertHelper.WarningAlert(Language.validation_Supplier_Cannot_Be_Deleted);
                    return;
                }

                logic.Delete(idKey);
                AlertHelper.SuccessAlert(Language.message_Deleted_Element);
                LoadData();
            }
        }

        private void  btn_add_Click(object sender, EventArgs e)
        {
            Add();
        }

        private void  btn_edit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void  btn_delete_Click(object sender, EventArgs e)
        {
            Remove();
        }

        private void  grid_list_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }
    }
}