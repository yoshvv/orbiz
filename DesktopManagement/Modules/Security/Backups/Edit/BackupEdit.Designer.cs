﻿namespace DesktopManagement.Modules.Security.Backups.Edit
{
    partial class BackupEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupEdit));
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.page_backup = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btn_backup = new Telerik.WinControls.UI.RadButton();
            this.lbl_currentDB = new Telerik.WinControls.UI.RadLabel();
            this.txt_currentDB = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel46 = new Telerik.WinControls.UI.RadLabel();
            this.lbl_backupName = new Telerik.WinControls.UI.RadLabel();
            this.txt_dbName = new Telerik.WinControls.UI.RadTextBox();
            this.page_restore = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.btn_restore = new Telerik.WinControls.UI.RadButton();
            this.lbl_restoreBackupName = new Telerik.WinControls.UI.RadLabel();
            this.txt_backupName = new Telerik.WinControls.UI.RadTextBox();
            this.btn_upload = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.page_backup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_backup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_currentDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_currentDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_backupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_dbName)).BeginInit();
            this.page_restore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_restore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_restoreBackupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_backupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_upload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.page_backup);
            this.radPageView1.Controls.Add(this.page_restore);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.page_backup;
            this.radPageView1.Size = new System.Drawing.Size(405, 294);
            this.radPageView1.TabIndex = 0;
            this.radPageView1.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.ItemList;
            // 
            // page_backup
            // 
            this.page_backup.Controls.Add(this.radPanel1);
            this.page_backup.Controls.Add(this.lbl_currentDB);
            this.page_backup.Controls.Add(this.txt_currentDB);
            this.page_backup.Controls.Add(this.radLabel46);
            this.page_backup.Controls.Add(this.lbl_backupName);
            this.page_backup.Controls.Add(this.txt_dbName);
            this.page_backup.ItemSize = new System.Drawing.SizeF(77F, 49F);
            this.page_backup.Location = new System.Drawing.Point(6, 55);
            this.page_backup.Name = "page_backup";
            this.page_backup.Size = new System.Drawing.Size(393, 233);
            this.page_backup.Text = "Backup";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radPanel1.Controls.Add(this.btn_backup);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 185);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(393, 48);
            this.radPanel1.TabIndex = 55;
            // 
            // btn_backup
            // 
            this.btn_backup.Image = global::DesktopManagement.Properties.Resources.database_backup;
            this.btn_backup.Location = new System.Drawing.Point(193, 5);
            this.btn_backup.Name = "btn_backup";
            this.btn_backup.Size = new System.Drawing.Size(154, 36);
            this.btn_backup.TabIndex = 7;
            this.btn_backup.Text = "Generate";
            this.btn_backup.ThemeName = "Material";
            this.btn_backup.Click += new System.EventHandler(this.btn_backup_Click);
            // 
            // lbl_currentDB
            // 
            this.lbl_currentDB.Location = new System.Drawing.Point(6, 7);
            this.lbl_currentDB.Name = "lbl_currentDB";
            this.lbl_currentDB.Size = new System.Drawing.Size(77, 21);
            this.lbl_currentDB.TabIndex = 51;
            this.lbl_currentDB.Text = "Current DB";
            this.lbl_currentDB.ThemeName = "Material";
            // 
            // txt_currentDB
            // 
            this.txt_currentDB.Enabled = false;
            this.txt_currentDB.Location = new System.Drawing.Point(6, 40);
            this.txt_currentDB.Name = "txt_currentDB";
            this.txt_currentDB.ReadOnly = true;
            this.txt_currentDB.Size = new System.Drawing.Size(341, 36);
            this.txt_currentDB.TabIndex = 50;
            this.txt_currentDB.TabStop = false;
            this.txt_currentDB.ThemeName = "Material";
            // 
            // radLabel46
            // 
            this.radLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel46.ForeColor = System.Drawing.Color.Red;
            this.radLabel46.Location = new System.Drawing.Point(334, 96);
            this.radLabel46.Name = "radLabel46";
            this.radLabel46.Size = new System.Drawing.Size(13, 20);
            this.radLabel46.TabIndex = 22;
            this.radLabel46.Text = "*";
            this.radLabel46.ThemeName = "Material";
            // 
            // lbl_backupName
            // 
            this.lbl_backupName.Location = new System.Drawing.Point(6, 95);
            this.lbl_backupName.Name = "lbl_backupName";
            this.lbl_backupName.Size = new System.Drawing.Size(95, 21);
            this.lbl_backupName.TabIndex = 21;
            this.lbl_backupName.Text = "Backup name";
            this.lbl_backupName.ThemeName = "Material";
            // 
            // txt_dbName
            // 
            this.txt_dbName.Location = new System.Drawing.Point(6, 119);
            this.txt_dbName.Name = "txt_dbName";
            this.txt_dbName.Size = new System.Drawing.Size(341, 36);
            this.txt_dbName.TabIndex = 20;
            this.txt_dbName.ThemeName = "Material";
            // 
            // page_restore
            // 
            this.page_restore.Controls.Add(this.radPanel2);
            this.page_restore.Controls.Add(this.lbl_restoreBackupName);
            this.page_restore.Controls.Add(this.txt_backupName);
            this.page_restore.Controls.Add(this.btn_upload);
            this.page_restore.ItemSize = new System.Drawing.SizeF(79F, 49F);
            this.page_restore.Location = new System.Drawing.Point(6, 55);
            this.page_restore.Name = "page_restore";
            this.page_restore.Size = new System.Drawing.Size(393, 233);
            this.page_restore.Text = "Restore";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radPanel2.Controls.Add(this.btn_restore);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel2.Location = new System.Drawing.Point(0, 185);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(393, 48);
            this.radPanel2.TabIndex = 56;
            // 
            // btn_restore
            // 
            this.btn_restore.Enabled = false;
            this.btn_restore.Image = global::DesktopManagement.Properties.Resources.backup_icon;
            this.btn_restore.Location = new System.Drawing.Point(182, 6);
            this.btn_restore.Name = "btn_restore";
            this.btn_restore.Size = new System.Drawing.Size(165, 36);
            this.btn_restore.TabIndex = 7;
            this.btn_restore.Text = "Restore";
            this.btn_restore.ThemeName = "Material";
            this.btn_restore.Click += new System.EventHandler(this.btn_restore_Click);
            // 
            // lbl_restoreBackupName
            // 
            this.lbl_restoreBackupName.Location = new System.Drawing.Point(3, 8);
            this.lbl_restoreBackupName.Name = "lbl_restoreBackupName";
            this.lbl_restoreBackupName.Size = new System.Drawing.Size(95, 21);
            this.lbl_restoreBackupName.TabIndex = 53;
            this.lbl_restoreBackupName.Text = "Backup name";
            this.lbl_restoreBackupName.ThemeName = "Material";
            // 
            // txt_backupName
            // 
            this.txt_backupName.Enabled = false;
            this.txt_backupName.Location = new System.Drawing.Point(3, 41);
            this.txt_backupName.Name = "txt_backupName";
            this.txt_backupName.ReadOnly = true;
            this.txt_backupName.Size = new System.Drawing.Size(309, 36);
            this.txt_backupName.TabIndex = 52;
            this.txt_backupName.TabStop = false;
            this.txt_backupName.ThemeName = "Material";
            // 
            // btn_upload
            // 
            this.btn_upload.Image = global::DesktopManagement.Properties.Resources.upload_db;
            this.btn_upload.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_upload.Location = new System.Drawing.Point(318, 41);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(35, 36);
            this.btn_upload.TabIndex = 54;
            this.btn_upload.ThemeName = "Material";
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // BackupEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 294);
            this.Controls.Add(this.radPageView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BackupEdit";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.page_backup.ResumeLayout(false);
            this.page_backup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_backup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_currentDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_currentDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_backupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_dbName)).EndInit();
            this.page_restore.ResumeLayout(false);
            this.page_restore.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_restore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_restoreBackupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_backupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_upload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage page_backup;
        private Telerik.WinControls.UI.RadPageViewPage page_restore;
        private Telerik.WinControls.UI.RadLabel radLabel46;
        private Telerik.WinControls.UI.RadLabel lbl_backupName;
        private Telerik.WinControls.UI.RadTextBox txt_dbName;
        private Telerik.WinControls.UI.RadLabel lbl_currentDB;
        private Telerik.WinControls.UI.RadTextBox txt_currentDB;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btn_backup;
        private Telerik.WinControls.UI.RadLabel lbl_restoreBackupName;
        private Telerik.WinControls.UI.RadTextBox txt_backupName;
        private Telerik.WinControls.UI.RadButton btn_upload;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadButton btn_restore;
    }
}
