﻿using DesktopManagement.Alerts;
using DesktopManagement.Helpers;
using DesktopManagement.Queries;
using DesktopManagement.Resx;
using Logic.Modules;
using Logic.Modules.Security;
using Persistence.Models.Security;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DesktopManagement.Modules.Security.Backups.Edit
{
    public partial class BackupEdit : Telerik.WinControls.UI.RadForm
    {
        public BackupEdit(Main main)
        {
            InitializeComponent();
            SetCurrentConnection();
            SetPermissions();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            SetStrings();
            this.main = main;
            this.txt_currentDB.Text = this.currentDB_Name;
            this.backupName = $"{this.currentDB_Name}_{DateTime.Now.ToShortDateString()}";
            this.txt_dbName.Text = this.backupName;
            this.configLogic = new ConfigLogic();
        }

        public string EntityName => Language.lbl_Backup_Manager;

        private Main main { get; }
        private ConfigLogic configLogic { get; }
        private string currentDB_Name = "gymfitness360";
        private string backupName = "";
        private string path = $@"{Settings.Backup_Path}";
        private string currentDB_Conn = "";
        private string restore_db = "";
        private SqlConnection con;
        private SqlCommand cmd;
        private SqlDataReader dr;

        /// <summary>
        /// Permissions for this entity
        /// </summary>
        public Permission Permissions { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = EntityName;
            page_backup.Text = Language.tab_Backup;
            page_restore.Text = Language.tab_Restore;

            lbl_currentDB.Text = Language.lbl_Current_DB;
            lbl_backupName.Text = Language.lbl_Backup_Name;
            lbl_restoreBackupName.Text = Language.lbl_Backup_Name;

            btn_backup.Text = Language.btn_Generate;
        }

        public void SetCurrentConnection()
        {
            this.currentDB_Conn = ConfigurationManager.ConnectionStrings["Context"].ConnectionString;
            this.currentDB_Name = currentDB_Conn.
                Split(new string[] { "Database=" }, StringSplitOptions.None)[1]
                .Split(';')[0];
        }

        /// <summary>
        /// Set the current user permissions for this window
        /// </summary>
        /// <returns></returns>
        public void SetPermissions()
        {
            var logicPermissions = new PermissionLogic();
            this.Permissions = logicPermissions.GetLoggedUserPermissions(CredentialManager.IdUser);
        }

        public bool BackupValidations() 
        {
            if (!this.Permissions.Database.Backup) 
            {
                AlertHelper.WarningAlert(Language.validation_No_Backup_Permission);
                return false;
            }

            if (string.IsNullOrEmpty(this.backupName)) 
            {
                AlertHelper.WarningAlert(MessageHelper.NotEmptyString(Language.tab_Backup));
                return false;
            }

            //replace the wrong characters
            this.backupName = this.txt_dbName.Text.Replace("-","").Replace("/","").Replace("\\", "");

            if (!Directory.Exists(this.path))
            {
                Directory.CreateDirectory(this.path);
            }

            //we need to delete the current backup if exists
            var file = this.path + this.backupName + ".bak";
            if (File.Exists(file))
            {
                try 
                {
                    File.Delete(file);
                }
                catch (Exception e) {}
            }

            return true;
        }

        public void GenerateBackup() 
        {
            try 
            {
                var query = $@"{string.Format(Query.Backup, this.backupName, this.currentDB_Name, this.path)}";
                Execute(query);
                AlertHelper.SuccessAlert(Language.message_Backup_Created);
                Process.Start(Settings.Explorer, this.path);
            }
            catch (Exception e) 
            {
                AlertHelper.ErrorAlert(string.Format(Language.message_Error, e.Message));
            }
            
        }

        public string GetConnectionString()
        {
            return $"Server=localhost;Database={this.currentDB_Name};Trusted_Connection=True";
        }

        public void Execute(string query) 
        {
            con = new SqlConnection(GetConnectionString());
            con.Open();
            cmd = new SqlCommand(query, con);
            dr = cmd.ExecuteReader();
            dr.Close();
        }

        private void btn_backup_Click(object sender, EventArgs e)
        {
            if (BackupValidations()) 
            {
                GenerateBackup();
            }
        }

        public void UploadBackup() 
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.AddExtension = true;
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = Settings.Dialog_Filter_BAK;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                btn_restore.Enabled = true;
                foreach (string fileName in openFileDialog.FileNames)
                {
                    restore_db = fileName;
                    this.txt_backupName.Text = restore_db;
                }
            }
        }

        /// <summary>
        /// Restore the uploaded database in SQL Server
        /// and change the database in the App.Config
        /// </summary>
        public void Restore() 
        {
            var restore_db_name = this.restore_db.Split('\\').ToList().Last().Split('.')[0];
            var query = string.Format(Query.Restore,this.restore_db,  restore_db_name);
            var isRestored = false;
            var closeApplication = false;
            try 
            {
                Execute(query);
                isRestored = true;
            } catch (Exception e) 
            {
                AlertHelper.ErrorAlert(string.Format(Language.message_Error, e.Message));
            }

            if (isRestored) 
            {
                try 
                {
                    this.currentDB_Name = restore_db_name;
                    //Change the App.config connection string with the new database
                    ChangeConnectionString();
                    //Change the connection string in the Logic project
                    configLogic.UpdateConnectionConfig(GetConnectionString());
                    //Change the backup database name with the new one in the App.config
                    SetCurrentConnection();
                    //Set the current permissions from the user to test if the new database works
                    SetPermissions();
                    closeApplication = true;
                }
                catch (Exception ee) 
                {
                    AlertHelper.ErrorAlert(string.Format(Language.message_Error_Restoring, ee.Message));
                }
            }

            if (closeApplication) 
            {
                //Close the application
                ExitApplication();
            }
        }

        public void ExitApplication()
        {
            AlertHelper.SuccessAlert(Language.message_Database_Restored);
            this.main.SetAskToClose(false);
            this.main.ExitAll();
        }

        public void ChangeConnectionString() 
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["Context"].ConnectionString = GetConnectionString();
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        private void btn_restore_Click(object sender, EventArgs e)
        {
            Restore();
        }

        private void btn_upload_Click(object sender, EventArgs e)
        {
            UploadBackup();
        }
    }
}