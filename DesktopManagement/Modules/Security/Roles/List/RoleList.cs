﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Modules.Security.RoleForm;
using DesktopManagement.Resx;
using Logic.Modules.Security;
using Logic.Modules.Security.Roles;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using EntityDTO = Logic.Modules.Security.Roles.RoleDTO;

namespace DesktopManagement.Modules.Security
{
    public partial class RoleList : RadForm, IBaseList<EntityDTO>
    {
        public RoleList()
        {
            InitializeComponent();

            this.logic = new RoleLogic();
            this.componentHelper = new ComponentHelper();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            this.componentHelper.SetDefaultGridConfig(grid_list);
            SetFilters();
            LoadData();
            SetPermissions();
        }

        public string EntityName => Language.lbl_Roles;

        private RoleLogic logic { get; }
    
        private ComponentHelper componentHelper { get; }

        /// <summary>
        /// List of elements of type <see cref="RoleDTO"/>
        /// </summary>
        public IReadOnlyList<EntityDTO> List { get; set; }

        /// <summary>
        /// Permissions for this entity
        /// </summary>
        public Permission Permissions { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_List, Language.lbl_Roles);
            radGroup_Actions.Text = Language.lbl_Actions;
            radGroup_Filters.Text = Language.lbl_Filters;
            btn_add.Text = Language.btn_Add;
            btn_edit.Text = Language.btn_Edit;
            btn_delete.Text = Language.btn_Delete;
            lbl_name.Text = Language.lbl_Name;
        }

        /// <summary>
        /// Set the current user permissions for this window
        /// </summary>
        /// <returns></returns>
        public void SetPermissions()
        {
            var logicPermissions = new PermissionLogic();
            this.Permissions = logicPermissions.GetLoggedUserPermissions(CredentialManager.IdUser);
        }

        public void SetFilters()
        {
            componentHelper.SetSearchIcon(txt_name);
        }

        public void LoadData()
        {
            var parameters = new RoleParams()
            {
                Name = txt_name.Text
            };

            this.List = logic.Query(parameters);

            BindingSource bs = new BindingSource();
            bs.DataSource = this.List;
            grid_list.DataSource = bs;
        }

        /// <summary>
        /// Get the selected object of type <see cref="RoleDTO"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public EntityDTO GetSelectedElement() 
        {
            var selectedRow = this.grid_list.CurrentRow;

            if (selectedRow == null) return null;

            var selected = selectedRow.Cells["Name"].Value.ToString();
            var roleSelected = this.List
                .FirstOrDefault(x => x.Name.ToLower() == selected.ToLower());

            return roleSelected;
        }

        /// <summary>
        /// Open the add form for this entity
        /// </summary>
        /// <returns></returns>
        public void Add() 
        {
            if (!Permissions.Roles.Add)
            {
                MessageHelper.WihoutPermissionToAddMessage(EntityName);
                return;
            }

            new RoleEdit(true).ShowDialog();
            LoadData();
        }

        /// <summary>
        /// Open the edit form of Role and load the data
        /// of the existing entity
        /// </summary>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Edit() 
        {
            if (!Permissions.Roles.Modify)
            {
                MessageHelper.WihoutPermissionToEditMessage(EntityName);
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            var idRole = selected.IdRole;
            var editRole = new RoleEdit(false);
            editRole.LoadExisting(idRole);
            editRole.ShowDialog();
            LoadData();
        }

        public void Remove() 
        {
            if (!Permissions.Roles.Remove)
            {
                MessageHelper.WihoutPermissionToDeleteMessage(EntityName);
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            if (AlertHelper.YesNoAlert(Language.message_Delete_Element))
            {
                var idRole = selected.IdRole;

                if (!logic.CanDelete(idRole)) 
                {
                    AlertHelper.WarningAlert(Language.validation_Cannot_Delete_Role);
                    return;
                }

                logic.Remove(idRole);
                AlertHelper.SuccessAlert(Language.message_Deleted_Element);
                LoadData();
            }

        }

        private async void txt_name_TextChanged(object sender, EventArgs e)
        {
            if (!await this.componentHelper.UserKeepsTyping(txt_name))
            {
                LoadData();
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            Add();
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            Remove();
        }

        private void grid_list_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }
    }
}