﻿namespace DesktopManagement.Modules.Security
{
    partial class RoleList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoleList));
            this.grid_list = new Telerik.WinControls.UI.RadGridView();
            this.radGroup_Filters = new Telerik.WinControls.UI.RadGroupBox();
            this.lbl_name = new Telerik.WinControls.UI.RadLabel();
            this.txt_name = new Telerik.WinControls.UI.RadTextBox();
            this.pnl_filters = new Telerik.WinControls.UI.RadPanel();
            this.pnl_actions = new Telerik.WinControls.UI.RadPanel();
            this.radGroup_Actions = new Telerik.WinControls.UI.RadGroupBox();
            this.btn_delete = new Telerik.WinControls.UI.RadButton();
            this.btn_add = new Telerik.WinControls.UI.RadButton();
            this.btn_edit = new Telerik.WinControls.UI.RadButton();
            this.pnl_list = new Telerik.WinControls.UI.RadPanel();
            this.material = new Telerik.WinControls.Themes.MaterialTheme();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Filters)).BeginInit();
            this.radGroup_Filters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_filters)).BeginInit();
            this.pnl_filters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_actions)).BeginInit();
            this.pnl_actions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Actions)).BeginInit();
            this.radGroup_Actions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_list)).BeginInit();
            this.pnl_list.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // grid_list
            // 
            this.grid_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid_list.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.grid_list.MasterTemplate.AllowAddNewRow = false;
            this.grid_list.MasterTemplate.AllowColumnChooser = false;
            this.grid_list.MasterTemplate.AllowDeleteRow = false;
            this.grid_list.MasterTemplate.AllowDragToGroup = false;
            this.grid_list.MasterTemplate.AllowEditRow = false;
            this.grid_list.MasterTemplate.AutoExpandGroups = true;
            this.grid_list.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "Name";
            gridViewTextBoxColumn1.MinWidth = 50;
            gridViewTextBoxColumn1.Name = "Name";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "Description";
            gridViewTextBoxColumn2.HeaderText = "Description";
            gridViewTextBoxColumn2.MinWidth = 50;
            gridViewTextBoxColumn2.Name = "Description";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 250;
            this.grid_list.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.grid_list.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.grid_list.Name = "grid_list";
            this.grid_list.ShowGroupPanel = false;
            this.grid_list.Size = new System.Drawing.Size(746, 272);
            this.grid_list.TabIndex = 0;
            this.grid_list.Text = "grid_list";
            this.grid_list.ThemeName = "Material";
            this.grid_list.DoubleClick += new System.EventHandler(this.grid_list_DoubleClick);
            // 
            // radGroup_Filters
            // 
            this.radGroup_Filters.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_Filters.Controls.Add(this.lbl_name);
            this.radGroup_Filters.Controls.Add(this.txt_name);
            this.radGroup_Filters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroup_Filters.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroup_Filters.HeaderText = "Filters";
            this.radGroup_Filters.Location = new System.Drawing.Point(0, 0);
            this.radGroup_Filters.Name = "radGroup_Filters";
            this.radGroup_Filters.Size = new System.Drawing.Size(746, 100);
            this.radGroup_Filters.TabIndex = 1;
            this.radGroup_Filters.Text = "Filters";
            this.radGroup_Filters.ThemeName = "ControlDefault";
            // 
            // lbl_name
            // 
            this.lbl_name.Location = new System.Drawing.Point(13, 26);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(45, 21);
            this.lbl_name.TabIndex = 1;
            this.lbl_name.Text = "Name";
            this.lbl_name.ThemeName = "Material";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(13, 49);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(558, 36);
            this.txt_name.TabIndex = 0;
            this.txt_name.ThemeName = "Material";
            this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // pnl_filters
            // 
            this.pnl_filters.Controls.Add(this.radGroup_Filters);
            this.pnl_filters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_filters.Location = new System.Drawing.Point(0, 0);
            this.pnl_filters.Name = "pnl_filters";
            this.pnl_filters.Size = new System.Drawing.Size(746, 100);
            this.pnl_filters.TabIndex = 2;
            // 
            // pnl_actions
            // 
            this.pnl_actions.Controls.Add(this.radGroup_Actions);
            this.pnl_actions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_actions.Location = new System.Drawing.Point(0, 100);
            this.pnl_actions.Name = "pnl_actions";
            this.pnl_actions.Size = new System.Drawing.Size(746, 81);
            this.pnl_actions.TabIndex = 3;
            // 
            // radGroup_Actions
            // 
            this.radGroup_Actions.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_Actions.Controls.Add(this.btn_delete);
            this.radGroup_Actions.Controls.Add(this.btn_add);
            this.radGroup_Actions.Controls.Add(this.btn_edit);
            this.radGroup_Actions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroup_Actions.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold);
            this.radGroup_Actions.HeaderText = "Actions";
            this.radGroup_Actions.Location = new System.Drawing.Point(0, 0);
            this.radGroup_Actions.Name = "radGroup_Actions";
            this.radGroup_Actions.Size = new System.Drawing.Size(746, 81);
            this.radGroup_Actions.TabIndex = 3;
            this.radGroup_Actions.Text = "Actions";
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(292, 26);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(120, 36);
            this.btn_delete.TabIndex = 2;
            this.btn_delete.Text = "Delete";
            this.btn_delete.ThemeName = "Material";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(13, 26);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(120, 36);
            this.btn_add.TabIndex = 0;
            this.btn_add.Text = "Add";
            this.btn_add.ThemeName = "Material";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(154, 26);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(120, 36);
            this.btn_edit.TabIndex = 1;
            this.btn_edit.Text = "Edit";
            this.btn_edit.ThemeName = "Material";
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // pnl_list
            // 
            this.pnl_list.Controls.Add(this.grid_list);
            this.pnl_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_list.Location = new System.Drawing.Point(0, 181);
            this.pnl_list.Name = "pnl_list";
            this.pnl_list.Size = new System.Drawing.Size(746, 272);
            this.pnl_list.TabIndex = 4;
            // 
            // RoleList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 453);
            this.Controls.Add(this.pnl_list);
            this.Controls.Add(this.pnl_actions);
            this.Controls.Add(this.pnl_filters);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RoleList";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.grid_list.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Filters)).EndInit();
            this.radGroup_Filters.ResumeLayout(false);
            this.radGroup_Filters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_filters)).EndInit();
            this.pnl_filters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_actions)).EndInit();
            this.pnl_actions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Actions)).EndInit();
            this.radGroup_Actions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_list)).EndInit();
            this.pnl_list.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView grid_list;
        private Telerik.WinControls.UI.RadGroupBox radGroup_Filters;
        private Telerik.WinControls.UI.RadPanel pnl_filters;
        private Telerik.WinControls.UI.RadPanel pnl_actions;
        private Telerik.WinControls.UI.RadButton btn_delete;
        private Telerik.WinControls.UI.RadButton btn_edit;
        private Telerik.WinControls.UI.RadButton btn_add;
        private Telerik.WinControls.UI.RadPanel pnl_list;
        private Telerik.WinControls.UI.RadTextBox txt_name;
        private Telerik.WinControls.UI.RadLabel lbl_name;
        private Telerik.WinControls.Themes.MaterialTheme material;
        private Telerik.WinControls.UI.RadGroupBox radGroup_Actions;
    }
}
