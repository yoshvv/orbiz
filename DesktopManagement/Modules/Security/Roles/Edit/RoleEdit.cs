﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Resx;
using Logic.Modules;
using Logic.Modules.Security;
using Persistence;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Entity = Persistence.Models.Security.Role;

namespace DesktopManagement.Modules.Security.RoleForm
{
    public partial class RoleEdit : RadForm, IBaseEdit<Entity>
    {
        public RoleEdit(bool isNew)
        {
            InitializeComponent();
            this.IsNew = isNew;
            this.logic = new RoleLogic();
            this.permissionLogic = new PermissionLogic();
            this.componentHelper = new ComponentHelper();
            this.menuLogic = new MenuLogic();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            componentHelper.SetSearchIcon(txt_filter);
            SetStrings();
            SetTitle();
            CreateTreePermissions();
        }

        public string EntityName => Language.lbl_Role;

        private RoleLogic logic { get; }
        
        private MenuLogic menuLogic { get;  }

        private PermissionLogic permissionLogic { get; }

        public bool IsNew { get; }

        /// <summary>
        /// Main entity of this form
        /// </summary>
        public Entity Entity { get; set; } = new Entity();

        /// <summary>
        /// Role's permissions entity
        /// </summary>
        private Permission Permission { get; set; }

        private ComponentHelper componentHelper { get; }

        private RadTreeNode root = null;

        private bool IsFromParentNode { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_Edit, EntityName);
            radGroup_GeneralInfo.Text = Language.lbl_General_Info;
            radGroup_Permissions.Text = Language.lbl_Permissions;
            lbl_name.Text = Language.lbl_Name;
            lbl_description.Text = Language.lbl_Description;
            btn_save.Text = Language.btn_Save;
        }

        /// <summary>
        /// Set the current title depending if the user is
        /// adding or editing an entity
        /// </summary>
        public void SetTitle()
        {
            if (IsNew)
            {
                this.Text = string.Format(Language.title_Add_New, EntityName);
            }
            else
            {
                this.Text = string.Format(Language.title_Edit_Existing, EntityName);
            }
        }

        /// <summary>
        /// Get all the properties from the Permission class
        /// ignoring the members we know we don't need
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<PropertyInfo> GetMenuPermissions(Type type)
        {
            var permissionsList = type
                   .GetProperties()
                   .Where(x => x.Name != "IdRole")
                   .Where(x => x.Name != "Role")
                   .Where(x => x.Name != "IdPermission")
                   .OrderBy(x => x.Name)
                   .ToList()
                   ;

            return permissionsList;
        }

        public void CreateTreePermissions()
        {
            using (var c = new Context())
            {
                //Menu list
                var menuList = GetMenuPermissions(typeof(Permission));

                //Filling all the menu and sub menu items
                foreach (var m in menuList)
                {
                    root = this.tree_permissions.Nodes.Add(m.Name);
                    root.CheckType = CheckType.CheckBox;
                    root.Name = m.Name;
                    root.Text = menuLogic.MenuFormat(m.Name);
                    
                    var subMenuItems = m.PropertyType
                        .GetProperties()
                        .OrderBy(x => x.Name)
                        .ToList();

                    //add submenu items
                    foreach (var sm in subMenuItems)
                    {
                        var radNode = new RadTreeNode();
                        radNode.CheckType = CheckType.CheckBox;
                        radNode.Text = menuLogic.MenuFormat(sm.Name);
                        radNode.Name = sm.Name;
                        root.Nodes.Add(radNode);
                    }
                    
                    //expanding all the elements
                    this.tree_permissions.ExpandAll();
                }
            }
        }

        public void LoadExisting(int idKey)
        {
            using (var c = new Context())
            {
                Entity = logic.GetRole(c, idKey);
                Permission = permissionLogic.GetPermission(c, idKey);
                SetGeneralInfo(Entity);
                SetPermissions(Permission);
            }
        }

        /// <summary>
        /// Set the information of the entity
        /// </summary>
        /// <param name="entity"></param>
        public void SetGeneralInfo(Entity entity)
        {
            this.txt_name.Text = entity.Name;
            this.txt_description.Text = entity.Description;
        }

        public void SetPermissions(Permission entity)
        {
            //All properties from the Entity
            var properties = GetMenuPermissions(entity.GetType());

            //cast to object the entity
            object value = entity;

            //setting all the permissions into the rad tree view
            foreach (var p in properties)
            {
                //name of the permission
                string propertyName = p.Name;

                //Getting all the values for this permission
                var parameter = Expression.Parameter(typeof(object));
                var cast = Expression.Convert(parameter, value.GetType());
                var propertyGetter = Expression.Property(cast, propertyName);
                var castResult = Expression.Convert(propertyGetter, typeof(object));//for boxing
                var propertyRetriver = Expression.Lambda<Func<object, object>>(castResult, parameter).Compile();
                var retrivedPropertyValue = propertyRetriver(value);

                //Getting nodes from tree view
                var nodes = this.tree_permissions
                    .Nodes
                    .Where(x=>x.Name == propertyName)
                    .SelectMany(x=>x.Nodes)
                    .ToList();

                //If the permission
                var permissions = retrivedPropertyValue.GetType()
                  .GetProperties()
                  .OrderBy(x => x.Name)
                  .ToList();

                int i = 0;

                foreach (var per in permissions)
                {
                    nodes[i].Checked = (bool)per.GetValue(retrivedPropertyValue);
                    i++;
                }
            }
        }

        /// <summary>
        /// Get an entity of type <see cref="Role"/> with the 
        /// data modified by the user
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Entity GetValues(Entity entity)
        {
            entity.Name = this.txt_name.Text;
            entity.Description = this.txt_description.Text;
            return entity;
        }

        /// <summary>
        /// Get an entity of type <see cref="Permission"/> with the 
        /// data modified by the user
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Permission GetPermissions(Permission entity)
        {
            //All properties from the Entity
            var properties = GetMenuPermissions(entity.GetType());

            //cast to object the entity
            object value = entity;

            //setting all the permissions into the rad tree view
            foreach (var p in properties)
            {
                //name of the permission
                string propertyName = p.Name;

                //Getting all the values for this permission
                var parameter = Expression.Parameter(typeof(object));
                var cast = Expression.Convert(parameter, value.GetType());
                var propertyGetter = Expression.Property(cast, propertyName);
                var castResult = Expression.Convert(propertyGetter, typeof(object));//for boxing
                var propertyRetriver = Expression.Lambda<Func<object, object>>(castResult, parameter).Compile();
                var retrivedPropertyValue = propertyRetriver(value);

                //Getting nodes from tree view
                var nodes = this.tree_permissions
                    .Nodes
                    .Where(x => x.Name == propertyName)
                    .SelectMany(x => x.Nodes)
                    .ToList();

                //If the permission
                var permissions = retrivedPropertyValue.GetType()
                  .GetProperties()
                  .OrderBy(x => x.Name)
                  .ToList();

                int i = 0;

                foreach (var per in permissions)
                {
                    per.SetValue(retrivedPropertyValue, nodes[i].Checked);
                    i++;
                }
            }

            return entity;
        }

        /// <summary>
        /// Add or update the current entity
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            //we need to expand all because if the user has filter
            //nodes and try to save data the tree view is going to
            //throw an error when bind data between the entity and the nodes
            FilterTreeView("");
            try
            {
                using (var c = new Context())
                {
                    if (this.IsNew)
                    {
                        //Adding New entity
                        var role = GetValues(new Entity());
                        logic.Add(c, role);

                        var permissions = GetPermissions(new Permission() { IdRole = role.IdRole });
                        permissionLogic.Add(c, permissions);

                        AlertHelper.SuccessAlert(string.Format(Language.message_Saved, EntityName));
                    }
                    else
                    {
                        //Updating existing entity
                        var role = GetValues(logic.GetRole(c, this.Entity.IdRole));
                        var permission = GetPermissions(permissionLogic.GetPermission(c, this.Permission.IdRole));
                        logic.Update(c, role);
                        permissionLogic.Update(c, permission);
                        AlertHelper.SuccessAlert(string.Format(Language.message_Updated, EntityName));
                    }
                }
            }
            catch (Exception ee) 
            {
                AlertHelper.ErrorAlert(ee.Message);
            }
            
            //Close the window
            Dispose();
        }

        /// <summary>
        /// Check all the values for this form to be correctly
        /// </summary>
        /// <returns></returns>
        public bool DoValidation() 
        {
            var name = txt_name.Text;

            if (string.IsNullOrEmpty(name)) 
            {
                AlertHelper.WarningAlert($"The role's name cannot be empty");
                return false;
            }

            var isRepeated = logic.IsNameRepeated(name, Entity.IdRole);

            if (isRepeated) 
            {
                
            }

            return true;
        }

        public void DoValidationAndSave()
        {
            if (DoValidation())
            {
                Save();
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            DoValidationAndSave();
        }

        private void AddModifyRole_Load(object sender, EventArgs e)
        {

        }

        private void pnl_buttons_Paint(object sender, PaintEventArgs e)
        {

        }

        public void CheckAllPermissions(RadCheckBox obj, bool isFromCheck) 
        {
            var checkAllControl = obj;

            var parent = checkAllControl.Parent;

            var childs = parent.Controls.OfType<RadToggleSwitch>()
                .ToList();

            void SetValues(bool val)
            {
                foreach (var c in childs)
                {
                    c.Value = val;
                }
            }

            if (childs.Select(x => x.Value).Where(x => !x).Any())
            {
                if(isFromCheck) SetValues(true);
                checkAllControl.Checked = false;
            }
            else
            {
                if (isFromCheck) SetValues(false);
                checkAllControl.Checked = true;
            }
        }

        private void check_feePayment_Click(object sender, EventArgs e)
        {
            CheckAllPermissions((RadCheckBox)sender, true);
        }

        private void switch_feePayment_visualize_Click(object sender, EventArgs e)
        {
        }

        private void switch_feePayment_visualize_ValueChanged(object sender, EventArgs e)
        {

        }

        private void group_members_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        public void FilterTreeView(string text) 
        {
            this.tree_permissions.Filter = text;
        }

        private async void txt_filter_TextChanged(object sender, EventArgs e)
        {
            if (!await this.componentHelper.UserKeepsTyping(txt_name))
            {
                FilterTreeView(this.txt_filter.Text);
            };
        }

        private void tree_permissions_Click(object sender, EventArgs e)
        {
           
        }

        private void tree_permissions_NodeMouseClick(object sender, RadTreeViewEventArgs e)
        {
            e.Node.Checked = !e.Node.Checked;
        }

        private void tree_permissions_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            var optionClicked = e.Node.Name;
            var obj = (RadTreeViewElement)sender;
            var option = obj.Nodes.Where(x => x.Name == optionClicked).FirstOrDefault();

            //If option is null it means the user clicked
            //a parent menu, so we need to search in the childs
            if (option == null)
            {
                //Search in the submenus the element clicked
                var parents = obj.Nodes.SelectMany(x => x.Nodes).ToList();

                foreach (var child in parents)
                {
                    if (child.Name == optionClicked)
                    {
                        option = child;
                        break;
                    }
                }
            }
            else 
            {
                //Search in the submenus the element clicked
                IsFromParentNode = true;
                var childs = option.Nodes;

                foreach(var n in childs)
                {
                    n.Checked = option.Checked;
                }
            }
        }

        private void txt_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }

        private void txt_name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }
    }
}
