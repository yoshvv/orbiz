﻿namespace DesktopManagement.Modules.Security.RoleForm
{
    partial class RoleEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoleEdit));
            this.material = new Telerik.WinControls.Themes.MaterialTheme();
            this.radGroup_GeneralInfo = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel46 = new Telerik.WinControls.UI.RadLabel();
            this.lbl_name = new Telerik.WinControls.UI.RadLabel();
            this.lbl_description = new Telerik.WinControls.UI.RadLabel();
            this.txt_description = new Telerik.WinControls.UI.RadTextBox();
            this.txt_name = new Telerik.WinControls.UI.RadTextBox();
            this.pnl_generalInfo = new Telerik.WinControls.UI.RadPanel();
            this.pnl_buttons = new Telerik.WinControls.UI.RadPanel();
            this.btn_save = new Telerik.WinControls.UI.RadButton();
            this.tree_permissions = new Telerik.WinControls.UI.RadTreeView();
            this.txt_filter = new Telerik.WinControls.UI.RadTextBox();
            this.radGroup_Permissions = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_GeneralInfo)).BeginInit();
            this.radGroup_GeneralInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_description)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_description)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_generalInfo)).BeginInit();
            this.pnl_generalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_buttons)).BeginInit();
            this.pnl_buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tree_permissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_filter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Permissions)).BeginInit();
            this.radGroup_Permissions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroup_GeneralInfo
            // 
            this.radGroup_GeneralInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_GeneralInfo.Controls.Add(this.radLabel46);
            this.radGroup_GeneralInfo.Controls.Add(this.lbl_name);
            this.radGroup_GeneralInfo.Controls.Add(this.lbl_description);
            this.radGroup_GeneralInfo.Controls.Add(this.txt_description);
            this.radGroup_GeneralInfo.Controls.Add(this.txt_name);
            this.radGroup_GeneralInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroup_GeneralInfo.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold);
            this.radGroup_GeneralInfo.HeaderText = "General Info";
            this.radGroup_GeneralInfo.Location = new System.Drawing.Point(0, 0);
            this.radGroup_GeneralInfo.Name = "radGroup_GeneralInfo";
            this.radGroup_GeneralInfo.Size = new System.Drawing.Size(314, 593);
            this.radGroup_GeneralInfo.TabIndex = 0;
            this.radGroup_GeneralInfo.Text = "General Info";
            // 
            // radLabel46
            // 
            this.radLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel46.ForeColor = System.Drawing.Color.Red;
            this.radLabel46.Location = new System.Drawing.Point(274, 31);
            this.radLabel46.Name = "radLabel46";
            this.radLabel46.Size = new System.Drawing.Size(13, 20);
            this.radLabel46.TabIndex = 19;
            this.radLabel46.Text = "*";
            this.radLabel46.ThemeName = "Material";
            // 
            // lbl_name
            // 
            this.lbl_name.Location = new System.Drawing.Point(12, 30);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(45, 21);
            this.lbl_name.TabIndex = 3;
            this.lbl_name.Text = "Name";
            this.lbl_name.ThemeName = "Material";
            // 
            // lbl_description
            // 
            this.lbl_description.Location = new System.Drawing.Point(12, 103);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(81, 21);
            this.lbl_description.TabIndex = 2;
            this.lbl_description.Text = "Description";
            this.lbl_description.ThemeName = "Material";
            // 
            // txt_description
            // 
            this.txt_description.AutoSize = false;
            this.txt_description.Location = new System.Drawing.Point(12, 130);
            this.txt_description.Multiline = true;
            this.txt_description.Name = "txt_description";
            this.txt_description.Size = new System.Drawing.Size(275, 66);
            this.txt_description.TabIndex = 1;
            this.txt_description.ThemeName = "Material";
            this.txt_description.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_name_KeyDown);
            this.txt_description.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_name_KeyPress);
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(12, 49);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(275, 36);
            this.txt_name.TabIndex = 0;
            this.txt_name.ThemeName = "Material";
            this.txt_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_name_KeyDown);
            this.txt_name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_name_KeyPress);
            // 
            // pnl_generalInfo
            // 
            this.pnl_generalInfo.Controls.Add(this.radGroup_GeneralInfo);
            this.pnl_generalInfo.Location = new System.Drawing.Point(0, 0);
            this.pnl_generalInfo.Name = "pnl_generalInfo";
            this.pnl_generalInfo.Size = new System.Drawing.Size(314, 593);
            this.pnl_generalInfo.TabIndex = 1;
            this.pnl_generalInfo.Text = "radPanel1";
            // 
            // pnl_buttons
            // 
            this.pnl_buttons.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnl_buttons.Controls.Add(this.btn_save);
            this.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_buttons.Location = new System.Drawing.Point(0, 613);
            this.pnl_buttons.Name = "pnl_buttons";
            this.pnl_buttons.Size = new System.Drawing.Size(762, 46);
            this.pnl_buttons.TabIndex = 3;
            this.pnl_buttons.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_buttons_Paint);
            // 
            // btn_save
            // 
            this.btn_save.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_save.Location = new System.Drawing.Point(616, 7);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(120, 36);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "Save";
            this.btn_save.ThemeName = "Material";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            this.btn_save.KeyDown += new System.Windows.Forms.KeyEventHandler(this.group_members_KeyDown);
            // 
            // tree_permissions
            // 
            this.tree_permissions.ItemHeight = 36;
            this.tree_permissions.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(209)))), ((int)(((byte)(209)))));
            this.tree_permissions.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.tree_permissions.Location = new System.Drawing.Point(14, 77);
            this.tree_permissions.Name = "tree_permissions";
            this.tree_permissions.Size = new System.Drawing.Size(388, 505);
            this.tree_permissions.TabIndex = 0;
            this.tree_permissions.Text = "radTreeView1";
            this.tree_permissions.ThemeName = "Material";
            this.tree_permissions.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.tree_permissions_NodeMouseClick);
            this.tree_permissions.NodeCheckedChanged += new Telerik.WinControls.UI.TreeNodeCheckedEventHandler(this.tree_permissions_NodeCheckedChanged);
            this.tree_permissions.Click += new System.EventHandler(this.tree_permissions_Click);
            this.tree_permissions.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_name_KeyDown);
            this.tree_permissions.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_name_KeyPress);
            // 
            // txt_filter
            // 
            this.txt_filter.Location = new System.Drawing.Point(14, 29);
            this.txt_filter.Name = "txt_filter";
            this.txt_filter.Size = new System.Drawing.Size(388, 36);
            this.txt_filter.TabIndex = 4;
            this.txt_filter.ThemeName = "Material";
            this.txt_filter.TextChanged += new System.EventHandler(this.txt_filter_TextChanged);
            // 
            // radGroup_Permissions
            // 
            this.radGroup_Permissions.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_Permissions.Controls.Add(this.tree_permissions);
            this.radGroup_Permissions.Controls.Add(this.txt_filter);
            this.radGroup_Permissions.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold);
            this.radGroup_Permissions.HeaderText = "Permissions";
            this.radGroup_Permissions.Location = new System.Drawing.Point(320, 0);
            this.radGroup_Permissions.Name = "radGroup_Permissions";
            this.radGroup_Permissions.Size = new System.Drawing.Size(416, 593);
            this.radGroup_Permissions.TabIndex = 5;
            this.radGroup_Permissions.Text = "Permissions";
            // 
            // RoleEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 659);
            this.Controls.Add(this.radGroup_Permissions);
            this.Controls.Add(this.pnl_buttons);
            this.Controls.Add(this.pnl_generalInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(766, 696);
            this.MinimumSize = new System.Drawing.Size(746, 681);
            this.Name = "RoleEdit";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(766, 696);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.AddModifyRole_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_GeneralInfo)).EndInit();
            this.radGroup_GeneralInfo.ResumeLayout(false);
            this.radGroup_GeneralInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_description)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_description)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_generalInfo)).EndInit();
            this.pnl_generalInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl_buttons)).EndInit();
            this.pnl_buttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tree_permissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_filter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Permissions)).EndInit();
            this.radGroup_Permissions.ResumeLayout(false);
            this.radGroup_Permissions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme material;
        private Telerik.WinControls.UI.RadGroupBox radGroup_GeneralInfo;
        private Telerik.WinControls.UI.RadPanel pnl_generalInfo;
        private Telerik.WinControls.UI.RadLabel lbl_description;
        private Telerik.WinControls.UI.RadTextBox txt_description;
        private Telerik.WinControls.UI.RadTextBox txt_name;
        private Telerik.WinControls.UI.RadLabel lbl_name;
        private Telerik.WinControls.UI.RadPanel pnl_buttons;
        private Telerik.WinControls.UI.RadButton btn_save;
        private Telerik.WinControls.UI.RadLabel radLabel46;
        private Telerik.WinControls.UI.RadTreeView tree_permissions;
        private Telerik.WinControls.UI.RadTextBox txt_filter;
        private Telerik.WinControls.UI.RadGroupBox radGroup_Permissions;
    }
}
