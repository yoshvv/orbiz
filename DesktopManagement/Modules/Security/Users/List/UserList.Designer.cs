﻿namespace DesktopManagement.Modules.Security.Users.List
{
    partial class UserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserList));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGroup_Filters = new Telerik.WinControls.UI.RadGroupBox();
            this.lbl_roleName = new Telerik.WinControls.UI.RadLabel();
            this.txt_roleName = new Telerik.WinControls.UI.RadTextBox();
            this.lbl_name = new Telerik.WinControls.UI.RadLabel();
            this.txt_name = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radGroup_Actions = new Telerik.WinControls.UI.RadGroupBox();
            this.btn_delete = new Telerik.WinControls.UI.RadButton();
            this.btn_add = new Telerik.WinControls.UI.RadButton();
            this.btn_edit = new Telerik.WinControls.UI.RadButton();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.grid_list = new Telerik.WinControls.UI.RadGridView();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Filters)).BeginInit();
            this.radGroup_Filters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_roleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_roleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Actions)).BeginInit();
            this.radGroup_Actions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radGroup_Filters);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(512, 171);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.Text = "radPanel1";
            // 
            // radGroup_Filters
            // 
            this.radGroup_Filters.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_Filters.Controls.Add(this.lbl_roleName);
            this.radGroup_Filters.Controls.Add(this.txt_roleName);
            this.radGroup_Filters.Controls.Add(this.lbl_name);
            this.radGroup_Filters.Controls.Add(this.txt_name);
            this.radGroup_Filters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroup_Filters.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroup_Filters.HeaderText = "Filters";
            this.radGroup_Filters.Location = new System.Drawing.Point(0, 0);
            this.radGroup_Filters.Name = "radGroup_Filters";
            this.radGroup_Filters.Size = new System.Drawing.Size(512, 171);
            this.radGroup_Filters.TabIndex = 2;
            this.radGroup_Filters.Text = "Filters";
            this.radGroup_Filters.ThemeName = "ControlDefault";
            // 
            // lbl_roleName
            // 
            this.lbl_roleName.Location = new System.Drawing.Point(12, 101);
            this.lbl_roleName.Name = "lbl_roleName";
            this.lbl_roleName.Size = new System.Drawing.Size(78, 21);
            this.lbl_roleName.TabIndex = 3;
            this.lbl_roleName.Text = "Role Name";
            this.lbl_roleName.ThemeName = "Material";
            // 
            // txt_roleName
            // 
            this.txt_roleName.Location = new System.Drawing.Point(12, 124);
            this.txt_roleName.Name = "txt_roleName";
            this.txt_roleName.Size = new System.Drawing.Size(558, 36);
            this.txt_roleName.TabIndex = 2;
            this.txt_roleName.ThemeName = "Material";
            this.txt_roleName.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // lbl_name
            // 
            this.lbl_name.Location = new System.Drawing.Point(13, 26);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(45, 21);
            this.lbl_name.TabIndex = 1;
            this.lbl_name.Text = "Name";
            this.lbl_name.ThemeName = "Material";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(13, 49);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(558, 36);
            this.txt_name.TabIndex = 0;
            this.txt_name.ThemeName = "Material";
            this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radGroup_Actions);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel2.Location = new System.Drawing.Point(0, 171);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(512, 72);
            this.radPanel2.TabIndex = 1;
            this.radPanel2.Text = "radPanel2";
            // 
            // radGroup_Actions
            // 
            this.radGroup_Actions.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_Actions.Controls.Add(this.btn_delete);
            this.radGroup_Actions.Controls.Add(this.btn_add);
            this.radGroup_Actions.Controls.Add(this.btn_edit);
            this.radGroup_Actions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroup_Actions.Font = new System.Drawing.Font("Bell MT", 11.25F, System.Drawing.FontStyle.Bold);
            this.radGroup_Actions.HeaderText = "Actions";
            this.radGroup_Actions.Location = new System.Drawing.Point(0, 0);
            this.radGroup_Actions.Name = "radGroup_Actions";
            this.radGroup_Actions.Size = new System.Drawing.Size(512, 72);
            this.radGroup_Actions.TabIndex = 4;
            this.radGroup_Actions.Text = "Actions";
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(292, 26);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(120, 36);
            this.btn_delete.TabIndex = 2;
            this.btn_delete.Text = "Delete";
            this.btn_delete.ThemeName = "Material";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(13, 26);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(120, 36);
            this.btn_add.TabIndex = 0;
            this.btn_add.Text = "Add";
            this.btn_add.ThemeName = "Material";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(154, 26);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(120, 36);
            this.btn_edit.TabIndex = 1;
            this.btn_edit.Text = "Edit";
            this.btn_edit.ThemeName = "Material";
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.grid_list);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(0, 243);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(512, 189);
            this.radPanel3.TabIndex = 2;
            this.radPanel3.Text = "radPanel3";
            // 
            // grid_list
            // 
            this.grid_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid_list.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Username";
            gridViewTextBoxColumn1.HeaderText = "Username";
            gridViewTextBoxColumn1.Name = "Username";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.FieldName = "RoleName";
            gridViewTextBoxColumn2.HeaderText = "Role";
            gridViewTextBoxColumn2.Name = "RoleName";
            gridViewTextBoxColumn2.Width = 150;
            this.grid_list.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.grid_list.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.grid_list.Name = "grid_list";
            this.grid_list.ReadOnly = true;
            this.grid_list.ShowGroupPanel = false;
            this.grid_list.Size = new System.Drawing.Size(512, 189);
            this.grid_list.TabIndex = 0;
            this.grid_list.Text = "radGridView1";
            this.grid_list.ThemeName = "Material";
            this.grid_list.DoubleClick += new System.EventHandler(this.grid_list_DoubleClick);
            // 
            // UserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 432);
            this.Controls.Add(this.radPanel3);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserList";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Filters)).EndInit();
            this.radGroup_Filters.ResumeLayout(false);
            this.radGroup_Filters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_roleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_roleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_Actions)).EndInit();
            this.radGroup_Actions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_list.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadGridView grid_list;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadGroupBox radGroup_Filters;
        private Telerik.WinControls.UI.RadLabel lbl_name;
        private Telerik.WinControls.UI.RadTextBox txt_name;
        private Telerik.WinControls.UI.RadGroupBox radGroup_Actions;
        private Telerik.WinControls.UI.RadButton btn_delete;
        private Telerik.WinControls.UI.RadButton btn_add;
        private Telerik.WinControls.UI.RadButton btn_edit;
        private Telerik.WinControls.UI.RadLabel lbl_roleName;
        private Telerik.WinControls.UI.RadTextBox txt_roleName;
    }
}
