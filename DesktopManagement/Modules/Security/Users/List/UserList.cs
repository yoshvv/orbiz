﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Modules.Security.Users.Edit;
using DesktopManagement.Resx;
using Logic.Modules.Security;
using Logic.Modules.Security.Users;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Telerik.WinControls.UI;
using EntityDTO = Logic.Modules.Security.Users.UserDTO;

namespace DesktopManagement.Modules.Security.Users.List
{
    public partial class UserList : RadForm, IBaseList<EntityDTO>
    {
        public UserList()
        {
            InitializeComponent();
            this.logic = new UserLogic();
            this.componentHelper = new ComponentHelper();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            SetStrings();
            this.componentHelper.SetDefaultGridConfig(grid_list);
            SetFilters();
            LoadData();
            SetPermissions();
        }

        public string EntityName => Language.lbl_Users;

        private UserLogic logic { get; }
        private ComponentHelper componentHelper { get; }

        /// <summary>
        /// List of elements of type <see cref="UserDTO"/>
        /// </summary>
        public IReadOnlyList<EntityDTO> List { get; set; }

        /// <summary>
        /// Permissions for this entity
        /// </summary>
        public Permission Permissions { get; set; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_List, Language.lbl_User);
            radGroup_Actions.Text = Language.lbl_Actions;
            radGroup_Filters.Text = Language.lbl_Filters;
            btn_add.Text = Language.btn_Add;
            btn_edit.Text = Language.btn_Edit;
            btn_delete.Text = Language.btn_Delete;
            lbl_name.Text = Language.lbl_Name;
            lbl_roleName.Text = Language.lbl_Role_Name;
        }

        /// <summary>
        /// Set the current user permissions for this window
        /// </summary>
        /// <returns></returns>
        public void SetPermissions()
        {
            var logicPermissions = new PermissionLogic();
            this.Permissions = logicPermissions.GetLoggedUserPermissions(CredentialManager.IdUser);
        }

        public void SetFilters()
        {
            componentHelper.SetSearchIcon(txt_name);
            componentHelper.SetSearchIcon(txt_roleName);
        }

        public void LoadData()
        {
            var parameters = new UserParams()
            {
                User = txt_name.Text,
                RoleName = txt_roleName.Text
            };

            this.List = logic.QueryAsync(parameters);

            grid_list.BindDataToGrid(this.List);
        }

        // <summary>
        /// Get the selected object of type <see cref="EntityDTO"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public EntityDTO GetSelectedElement()
        {
            var selectedRow = this.grid_list.CurrentRow;

            if (selectedRow == null) return null;

            var selected = selectedRow.Cells["Username"].Value.ToString();

            var entitySelected = this.List
                .Where(x => x.Username.ToLower() == selected.ToLower())
                .FirstOrDefault();

            return entitySelected;
        }

        /// <summary>
        /// Open the add form for this entity
        /// </summary>
        /// <returns></returns>
        public void Add()
        {
            if (!Permissions.User.Add)
            {
                MessageHelper.WihoutPermissionToAddMessage(EntityName);
                return;
            }

            new UserEdit(true).ShowDialog();
            LoadData();
        }

        /// <summary>
        /// Open the edit form
        /// </summary>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Edit()
        {
            if (!Permissions.User.Modify)
            {
                MessageHelper.WihoutPermissionToEditMessage(EntityName);
                return;
            }


            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            var idKey = selected.IdUser;
            var editForm = new UserEdit(false);
            editForm.LoadExisting(idKey);
            editForm.ShowDialog();
            LoadData();
        }

        public void Remove()
        {
            if (!Permissions.User.Remove)
            {
                MessageHelper.WihoutPermissionToDeleteMessage(EntityName);
                return;
            }

            var selected = GetSelectedElement();

            if (selected == null)
            {
                AlertHelper.WarningAlert(Language.message_No_Item_Selected);
                return;
            }

            if (AlertHelper.YesNoAlert(Language.message_Delete_Element))
            {
                var idKey = selected.IdUser;
                logic.Remove(idKey);
                AlertHelper.SuccessAlert(Language.message_Deleted_Element);
                LoadData();
            }
        }

        private async void txt_name_TextChanged(object sender, EventArgs e)
        {
            if (!await this.componentHelper.UserKeepsTyping((RadTextBox)sender))
            {
                LoadData();
            };
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            Add();
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            Edit();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            Remove();
        }

        private void grid_list_DoubleClick(object sender, EventArgs e)
        {
            Edit();
        }
    }
}