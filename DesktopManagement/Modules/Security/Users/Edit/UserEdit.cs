﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Resx;
using Logic.Modules.Security;
using Logic.Modules.Security.Users;
using Persistence;
using Persistence.Models;
using Persistence.Models.Files;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Entity = Persistence.Models.User;

namespace DesktopManagement.Modules.Security.Users.Edit
{
    public partial class UserEdit : RadForm, IBaseEdit<Entity>
    {
        public UserEdit(bool isNew)
        {
            InitializeComponent();
            this.logic = new UserLogic();
            this.roleLogic = new RoleLogic();
            this.IsNew = isNew;
            this.Cryptography = new Cryptography();
            this.imageHelper = new Helpers.FileHelper();
            this.parseHelper = new ParseHelper();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            SetStrings();
            SetTitle();
            this.Language();
            CheckPageIndex();
            LoadRoles();
        }

        public string EntityName => Language.lbl_User;
        private UserLogic logic { get; }
        private RoleLogic roleLogic { get;  }
        public bool IsNew { get; }
        public Entity Entity { get; set; }

        public Profile Profile { get; set; }

        private Cryptography Cryptography { get; }
        private FileData ProfileImage { get; set; } = null;

        private Helpers.FileHelper imageHelper { get; }

        private ParseHelper parseHelper { get; }

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = string.Format(Language.title_Edit, EntityName);
            radGroup_BasicInfo.Text = Language.lbl_Basic_Info;

            lbl_userName.Text = Language.lbl_UserName;
            lbl_role.Text = Language.lbl_Role;
            lbl_password.Text = Language.lbl_Password;
            lbl_repeatPassword.Text = Language.lbl_Repeat_Password;
            lbl_firstName.Text = Language.lbl_First_Name;
            lbl_lastName.Text = Language.lbl_Last_Name;
            lbl_birthDate.Text = Language.lbl_Birth_Date;
            lbl_phone.Text = Language.lbl_Phone;
            lbl_email.Text = Language.lbl_E_Mail;
            lbl_profileImage.Text = Language.lbl_Profile_Image;

            dropdown_roles.Text = Language.lbl_Select_Role;
            page_userName.Text = Language.tab_Username_Password;
            page_profile.Text = Language.tab_Profile_Info;
            btn_save.Text = Language.btn_Save;
        }

        /// <summary>
        /// Set the form title depending if the user is adding or updating an existing entity
        /// </summary>
        public void SetTitle()
        {
            if (IsNew)
            {
                this.Text = string.Format(Language.title_Add_New, EntityName);
            }
            else
            {
                this.Text = string.Format(Language.title_Edit_Existing, EntityName);
            }
        }

        public void  SetSelectedPage(int index) 
        {
            radPageView.SelectedPage = page_userName;

            if (index == 2) radPageView.SelectedPage = page_profile;
        }

        public void LoadRoles()
        {
            var roles = roleLogic.Query();
            var list = roles
                .Select(x => x.Name)
                .ToList();

            this.dropdown_roles.BindDataToGrid(list);
            this.dropdown_roles.SelectedIndex = -1;
        }

        /// <summary>
        /// Load the Entity's data from the DB
        /// </summary>
        /// <param name="idKey"></param>
        public void LoadExisting(int idKey)
        {
            using (var c = new Context())
            {
                //loading user info
                this.Entity = logic.Get(c, idKey);
                this.txt_username.Text = Entity.Username;
                this.txt_pass1.Text = Cryptography.IsEcrypt(Entity.Password) ?
                    Cryptography.Decrypt(Entity.Password) :
                    Entity.Password;
                this.dropdown_roles.SelectedIndex = this.dropdown_roles.Items.IndexOf(Entity.Role.Name);

                //loading user profile
                this.Profile = logic.GetUserProfile(c, Entity.IdUser);
                this.txt_firstName.Text = Profile.BasicInfo.FirstName;
                this.txt_lastName.Text = Profile.BasicInfo.LastName;
                this.txt_phone.Text = Profile.BasicInfo.Phone;
                this.txt_email.Text = Profile.BasicInfo.Email;

                if (Profile.BasicInfo.BirthDate != null)
                    this.date_birthDate.Value = Profile.BasicInfo.BirthDate.Value;
                else this.date_birthDate.NullableValue = null;

                ProfileImage = Profile.BasicInfo.Avatar;
                img_profileImage.Image = imageHelper.GetAvatar(Profile.BasicInfo.Avatar.File);
            }
        }

        /// <summary>
        /// Establece los valores de la ventana a la entidad Usuario
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Entity GetValues(Entity entity)
        {
            //user values
            entity.Username = this.txt_username.Text;
            entity.Password = Cryptography.Encrypt(this.txt_pass1.Text);
            var role = roleLogic.GetRole(this.dropdown_roles.SelectedItem.Value.ToString());
            entity.IdRole = role.IdRole;

            if (IsNew)
            {
                entity.CreatedDate = DateTime.Now;
            }

            return entity;
        }

        /// <summary>
        /// Get the information of the user's profile
        /// </summary>
        /// <returns></returns>
        public Profile GetUserProfileValue(Profile entity) 
        {
            entity.BasicInfo = new BasicInfo()
            {
                FirstName = txt_firstName.Text,
                LastName = txt_lastName.Text,
                BirthDate = parseHelper.ToDateTime(date_birthDate),
                Email = txt_email.Text,
                Phone = txt_phone.Text,
                Avatar = this.imageHelper.ImageToArrayOfBytes(this.img_profileImage.Image, this.ProfileImage)
            };

            return entity;
        }

        /// <summary>
        /// Add or update an entity
        /// </summary>
        /// <returns></returns>
        public void Save()
        {
            using (var c = new Context())
            {
                try
                {
                    if (this.IsNew)
                    {
                        //Adding New entity
                        var entity = GetValues(new Entity());

                        //save user entity
                        logic.Add(c, entity);

                        //create user's profile entity
                        var profile = GetUserProfileValue(new Profile());
                        profile.IdUser = entity.IdUser;

                        //save user's profile entity
                        logic.Add(c, profile);

                        AlertHelper.SuccessAlert(string.Format(Language.message_Saved, EntityName));
                    }
                    else
                    {
                        //Modifying an existing entity
                        var existing = GetValues(logic.Get(c, this.Entity.IdUser));
                        logic.Update(c, existing);

                        //Modifying existing user profile
                        var existingProfile = GetUserProfileValue(logic.GetUserProfile(c, this.Entity.IdUser));
                        logic.Update(c, existingProfile);

                        AlertHelper.SuccessAlert(string.Format(Language.message_Updated, EntityName));
                    }
                }
                catch (Exception ee)
                {
                    AlertHelper.ErrorAlert(ee.Message);
                }
            }
            //Close the window
            Dispose();
        }

        /// <summary>
        /// Validate that all the controls' data has been set correctly
        /// correctamente.
        /// </summary>
        public bool DoValidation()
        {
            var userName = this.txt_username.Text;
            var pass = this.txt_pass1.Text;
            var passRepeated = this.txt_pass2.Text;

            var userNameRepeated = logic.UserNameRepeated(userName);

            if (userNameRepeated && this.IsNew)
            {
                AlertHelper.WarningAlert(string.Format(Language.validation_Username_Already_Used, userName));
                return false;
            }

            if (string.IsNullOrEmpty(userName))
            {
                AlertHelper.WarningAlert(MessageHelper.NotEmptyString(Language.lbl_UserName));
                return false;
            }

            if (this.dropdown_roles.SelectedIndex < 0)
            {
                AlertHelper.WarningAlert(Language.validation_Must_Select_Role);
                return false;
            }


            if (string.IsNullOrEmpty(pass))
            {
                AlertHelper.WarningAlert(MessageHelper.NotEmptyString(Language.lbl_Password));
                return false;
            }

            var passRepeat = txt_pass2.Text;

            //if the user is editing this user and haven't changed
            //the password the are not going to evaluate if the password
            //and repeat password are equals
            if (!(!IsNew && string.IsNullOrEmpty(passRepeat) && (logic.IsPasswordTheSame(Entity.IdUser, pass))))
            {
                if (pass != passRepeated)
                {
                    AlertHelper.WarningAlert(Language.validation_Passwords_Not_Match);
                    return false;
                }
            }

            /*******************************
            *******PROFILE VALIDATION******
            *******************************/
            var firstName = txt_firstName.Text;
            var lastName = txt_firstName.Text;

            if (string.IsNullOrEmpty(firstName)) 
            {
                AlertHelper.WarningAlert(MessageHelper.NotEmptyString(Language.lbl_First_Name));
                return false;
            }


            if (string.IsNullOrEmpty(lastName))
            {
                AlertHelper.WarningAlert(MessageHelper.NotEmptyString(Language.lbl_Last_Name));
                return false;
            }

            //If all the validations have passed it means we can save
            return true;
        }

        public void DoValidationAndSave()
        {
            if (DoValidation())
            {
                Save();
            }
        }

        private void  btn_save_Click(object sender, EventArgs e)
        {
            DoValidationAndSave();
        }

        private void  txt_username_KeyDown(object sender, KeyEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }

        public void  CheckPageIndex() 
        {
            if (radPageView.SelectedPage == page_userName)
            {
                this.Size = new Size(472, 337);
                this.btn_save.Location = new Point(327, 10);
            }
            else 
            {
                this.Size = new Size(694, 423);
                this.btn_save.Location = new Point(562, 10);
            }
        }

        private void  radPageView1_SelectedPageChanged(object sender, EventArgs e)
        {
            CheckPageIndex();
        }

        private void  btn_cleanBirthDate_Click(object sender, EventArgs e)
        {
            date_birthDate.NullableValue = null;
        }

        private void  btn_addProfileImg_Click(object sender, EventArgs e)
        {
            ProfileImage = imageHelper.LoadImageInPictureBox(img_profileImage);
        }

        private void  btn_removeProfileImg_Click(object sender, EventArgs e)
        {
            //Clean the picture box
            this.ProfileImage = imageHelper.ClearPictureBox(img_profileImage);
        }

        private void txt_phone_KeyDown(object sender, KeyEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }

        private void txt_phone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }

        private void txt_username_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (KeyEventHelper.IsSaving(e))
            {
                DoValidationAndSave();
            }
        }
    }
}