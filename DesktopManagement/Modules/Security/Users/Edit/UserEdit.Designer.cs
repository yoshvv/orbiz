﻿namespace DesktopManagement.Modules.Security.Users.Edit
{
    partial class UserEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void  Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void  InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserEdit));
            this.pnl_buttons = new Telerik.WinControls.UI.RadPanel();
            this.btn_save = new Telerik.WinControls.UI.RadButton();
            this.lbl_password = new Telerik.WinControls.UI.RadLabel();
            this.lbl_userName = new Telerik.WinControls.UI.RadLabel();
            this.dropdown_roles = new Telerik.WinControls.UI.RadDropDownList();
            this.txt_pass1 = new Telerik.WinControls.UI.RadTextBox();
            this.txt_username = new Telerik.WinControls.UI.RadTextBox();
            this.lbl_role = new Telerik.WinControls.UI.RadLabel();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.lbl_repeatPassword = new Telerik.WinControls.UI.RadLabel();
            this.txt_pass2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel46 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radPageView = new Telerik.WinControls.UI.RadPageView();
            this.page_userName = new Telerik.WinControls.UI.RadPageViewPage();
            this.page_profile = new Telerik.WinControls.UI.RadPageViewPage();
            this.btn_addProfileImg = new Telerik.WinControls.UI.RadButton();
            this.img_profileImage = new System.Windows.Forms.PictureBox();
            this.btn_removeProfileImg = new Telerik.WinControls.UI.RadButton();
            this.radGroup_BasicInfo = new Telerik.WinControls.UI.RadGroupBox();
            this.btn_cleanBirthDate = new Telerik.WinControls.UI.RadButton();
            this.date_birthDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.txt_email = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lbl_phone = new Telerik.WinControls.UI.RadLabel();
            this.lbl_email = new Telerik.WinControls.UI.RadLabel();
            this.txt_phone = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.lbl_birthDate = new Telerik.WinControls.UI.RadLabel();
            this.lbl_lastName = new Telerik.WinControls.UI.RadLabel();
            this.txt_lastName = new Telerik.WinControls.UI.RadTextBox();
            this.lbl_firstName = new Telerik.WinControls.UI.RadLabel();
            this.txt_firstName = new Telerik.WinControls.UI.RadTextBox();
            this.lbl_profileImage = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pnl_buttons)).BeginInit();
            this.pnl_buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_userName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdown_roles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_pass1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_username)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_repeatPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_pass2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).BeginInit();
            this.radPageView.SuspendLayout();
            this.page_userName.SuspendLayout();
            this.page_profile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addProfileImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_profileImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeProfileImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_BasicInfo)).BeginInit();
            this.radGroup_BasicInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanBirthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_birthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_phone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_phone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_birthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_lastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_lastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_firstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_firstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_profileImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_buttons
            // 
            this.pnl_buttons.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnl_buttons.Controls.Add(this.btn_save);
            this.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_buttons.Location = new System.Drawing.Point(0, 332);
            this.pnl_buttons.Name = "pnl_buttons";
            this.pnl_buttons.Size = new System.Drawing.Size(692, 54);
            this.pnl_buttons.TabIndex = 14;
            this.pnl_buttons.ThemeName = "Desert";
            this.pnl_buttons.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            // 
            // btn_save
            // 
            this.btn_save.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_save.Location = new System.Drawing.Point(562, 10);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(120, 36);
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "Save";
            this.btn_save.ThemeName = "Material";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            this.btn_save.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            // 
            // lbl_password
            // 
            this.lbl_password.Location = new System.Drawing.Point(3, 111);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(71, 21);
            this.lbl_password.TabIndex = 13;
            this.lbl_password.Text = "Password";
            this.lbl_password.ThemeName = "Material";
            // 
            // lbl_userName
            // 
            this.lbl_userName.Location = new System.Drawing.Point(3, 12);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(73, 21);
            this.lbl_userName.TabIndex = 12;
            this.lbl_userName.Text = "Username";
            this.lbl_userName.ThemeName = "Material";
            // 
            // dropdown_roles
            // 
            this.dropdown_roles.Location = new System.Drawing.Point(238, 45);
            this.dropdown_roles.Name = "dropdown_roles";
            this.dropdown_roles.Size = new System.Drawing.Size(202, 36);
            this.dropdown_roles.TabIndex = 2;
            this.dropdown_roles.Text = "Select a role";
            this.dropdown_roles.ThemeName = "Material";
            this.dropdown_roles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_username_KeyPress);
            this.dropdown_roles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            // 
            // txt_pass1
            // 
            this.txt_pass1.Location = new System.Drawing.Point(3, 140);
            this.txt_pass1.Name = "txt_pass1";
            this.txt_pass1.PasswordChar = '*';
            this.txt_pass1.Size = new System.Drawing.Size(202, 36);
            this.txt_pass1.TabIndex = 3;
            this.txt_pass1.ThemeName = "Material";
            this.txt_pass1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            this.txt_pass1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_username_KeyPress);
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(3, 45);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(202, 36);
            this.txt_username.TabIndex = 1;
            this.txt_username.ThemeName = "Material";
            this.txt_username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            this.txt_username.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_username_KeyPress);
            // 
            // lbl_role
            // 
            this.lbl_role.Location = new System.Drawing.Point(238, 15);
            this.lbl_role.Name = "lbl_role";
            this.lbl_role.Size = new System.Drawing.Size(36, 21);
            this.lbl_role.TabIndex = 15;
            this.lbl_role.Text = "Role";
            this.lbl_role.ThemeName = "Material";
            // 
            // lbl_repeatPassword
            // 
            this.lbl_repeatPassword.Location = new System.Drawing.Point(238, 111);
            this.lbl_repeatPassword.Name = "lbl_repeatPassword";
            this.lbl_repeatPassword.Size = new System.Drawing.Size(120, 21);
            this.lbl_repeatPassword.TabIndex = 17;
            this.lbl_repeatPassword.Text = "Repeat Password";
            this.lbl_repeatPassword.ThemeName = "Material";
            // 
            // txt_pass2
            // 
            this.txt_pass2.Location = new System.Drawing.Point(238, 140);
            this.txt_pass2.Name = "txt_pass2";
            this.txt_pass2.PasswordChar = '*';
            this.txt_pass2.Size = new System.Drawing.Size(202, 36);
            this.txt_pass2.TabIndex = 4;
            this.txt_pass2.ThemeName = "Material";
            this.txt_pass2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_username_KeyDown);
            this.txt_pass2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_username_KeyPress);
            // 
            // radLabel46
            // 
            this.radLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel46.ForeColor = System.Drawing.Color.Red;
            this.radLabel46.Location = new System.Drawing.Point(192, 16);
            this.radLabel46.Name = "radLabel46";
            this.radLabel46.Size = new System.Drawing.Size(13, 20);
            this.radLabel46.TabIndex = 20;
            this.radLabel46.Text = "*";
            this.radLabel46.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel5.ForeColor = System.Drawing.Color.Red;
            this.radLabel5.Location = new System.Drawing.Point(427, 13);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(13, 20);
            this.radLabel5.TabIndex = 21;
            this.radLabel5.Text = "*";
            this.radLabel5.ThemeName = "Material";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel6.ForeColor = System.Drawing.Color.Red;
            this.radLabel6.Location = new System.Drawing.Point(192, 112);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(13, 20);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "*";
            this.radLabel6.ThemeName = "Material";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel7.ForeColor = System.Drawing.Color.Red;
            this.radLabel7.Location = new System.Drawing.Point(427, 109);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(13, 20);
            this.radLabel7.TabIndex = 23;
            this.radLabel7.Text = "*";
            this.radLabel7.ThemeName = "Material";
            // 
            // radPageView
            // 
            this.radPageView.Controls.Add(this.page_userName);
            this.radPageView.Controls.Add(this.page_profile);
            this.radPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView.Location = new System.Drawing.Point(0, 0);
            this.radPageView.MaximumSize = new System.Drawing.Size(693, 371);
            this.radPageView.Name = "radPageView";
            // 
            // 
            // 
            this.radPageView.RootElement.MaxSize = new System.Drawing.Size(693, 371);
            this.radPageView.SelectedPage = this.page_userName;
            this.radPageView.Size = new System.Drawing.Size(692, 332);
            this.radPageView.TabIndex = 24;
            this.radPageView.Text = "radPageView1";
            this.radPageView.ThemeName = "Material";
            this.radPageView.SelectedPageChanged += new System.EventHandler(this.radPageView1_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.ItemList;
            // 
            // page_userName
            // 
            this.page_userName.AutoSize = true;
            this.page_userName.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.page_userName.Controls.Add(this.txt_pass1);
            this.page_userName.Controls.Add(this.radLabel7);
            this.page_userName.Controls.Add(this.lbl_role);
            this.page_userName.Controls.Add(this.radLabel6);
            this.page_userName.Controls.Add(this.txt_username);
            this.page_userName.Controls.Add(this.radLabel5);
            this.page_userName.Controls.Add(this.dropdown_roles);
            this.page_userName.Controls.Add(this.radLabel46);
            this.page_userName.Controls.Add(this.lbl_userName);
            this.page_userName.Controls.Add(this.lbl_repeatPassword);
            this.page_userName.Controls.Add(this.lbl_password);
            this.page_userName.Controls.Add(this.txt_pass2);
            this.page_userName.ItemSize = new System.Drawing.SizeF(142F, 49F);
            this.page_userName.Location = new System.Drawing.Point(6, 55);
            this.page_userName.Name = "page_userName";
            this.page_userName.Size = new System.Drawing.Size(680, 271);
            this.page_userName.Text = "Username & Pass";
            // 
            // page_profile
            // 
            this.page_profile.AutoSize = true;
            this.page_profile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.page_profile.Controls.Add(this.btn_addProfileImg);
            this.page_profile.Controls.Add(this.img_profileImage);
            this.page_profile.Controls.Add(this.btn_removeProfileImg);
            this.page_profile.Controls.Add(this.radGroup_BasicInfo);
            this.page_profile.Controls.Add(this.lbl_profileImage);
            this.page_profile.ItemSize = new System.Drawing.SizeF(100F, 49F);
            this.page_profile.Location = new System.Drawing.Point(6, 55);
            this.page_profile.Name = "page_profile";
            this.page_profile.Size = new System.Drawing.Size(679, 275);
            this.page_profile.Text = "Profile Info";
            // 
            // btn_addProfileImg
            // 
            this.btn_addProfileImg.AutoSize = true;
            this.btn_addProfileImg.Image = global::DesktopManagement.Properties.Resources.icons8_add_image_64;
            this.btn_addProfileImg.Location = new System.Drawing.Point(649, 197);
            this.btn_addProfileImg.Name = "btn_addProfileImg";
            this.btn_addProfileImg.Size = new System.Drawing.Size(27, 27);
            this.btn_addProfileImg.TabIndex = 26;
            this.btn_addProfileImg.Click += new System.EventHandler(this.btn_addProfileImg_Click);
            // 
            // img_profileImage
            // 
            this.img_profileImage.Image = global::DesktopManagement.Properties.Resources.no_image;
            this.img_profileImage.Location = new System.Drawing.Point(498, 13);
            this.img_profileImage.Name = "img_profileImage";
            this.img_profileImage.Size = new System.Drawing.Size(178, 178);
            this.img_profileImage.TabIndex = 29;
            this.img_profileImage.TabStop = false;
            // 
            // btn_removeProfileImg
            // 
            this.btn_removeProfileImg.AutoSize = true;
            this.btn_removeProfileImg.Image = global::DesktopManagement.Properties.Resources.trash_icon;
            this.btn_removeProfileImg.Location = new System.Drawing.Point(609, 197);
            this.btn_removeProfileImg.Name = "btn_removeProfileImg";
            this.btn_removeProfileImg.Size = new System.Drawing.Size(27, 27);
            this.btn_removeProfileImg.TabIndex = 27;
            this.btn_removeProfileImg.Click += new System.EventHandler(this.btn_removeProfileImg_Click);
            // 
            // radGroup_BasicInfo
            // 
            this.radGroup_BasicInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroup_BasicInfo.Controls.Add(this.btn_cleanBirthDate);
            this.radGroup_BasicInfo.Controls.Add(this.date_birthDate);
            this.radGroup_BasicInfo.Controls.Add(this.radLabel10);
            this.radGroup_BasicInfo.Controls.Add(this.radLabel9);
            this.radGroup_BasicInfo.Controls.Add(this.txt_email);
            this.radGroup_BasicInfo.Controls.Add(this.lbl_phone);
            this.radGroup_BasicInfo.Controls.Add(this.lbl_email);
            this.radGroup_BasicInfo.Controls.Add(this.txt_phone);
            this.radGroup_BasicInfo.Controls.Add(this.lbl_birthDate);
            this.radGroup_BasicInfo.Controls.Add(this.lbl_lastName);
            this.radGroup_BasicInfo.Controls.Add(this.txt_lastName);
            this.radGroup_BasicInfo.Controls.Add(this.lbl_firstName);
            this.radGroup_BasicInfo.Controls.Add(this.txt_firstName);
            this.radGroup_BasicInfo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radGroup_BasicInfo.HeaderText = "Basic info";
            this.radGroup_BasicInfo.Location = new System.Drawing.Point(0, 2);
            this.radGroup_BasicInfo.Name = "radGroup_BasicInfo";
            this.radGroup_BasicInfo.Size = new System.Drawing.Size(483, 270);
            this.radGroup_BasicInfo.TabIndex = 28;
            this.radGroup_BasicInfo.Text = "Basic info";
            this.radGroup_BasicInfo.ThemeName = "ControlDefault";
            // 
            // btn_cleanBirthDate
            // 
            this.btn_cleanBirthDate.Location = new System.Drawing.Point(183, 107);
            this.btn_cleanBirthDate.MaximumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanBirthDate.MinimumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanBirthDate.Name = "btn_cleanBirthDate";
            // 
            // 
            // 
            this.btn_cleanBirthDate.RootElement.MaxSize = new System.Drawing.Size(38, 20);
            this.btn_cleanBirthDate.RootElement.MinSize = new System.Drawing.Size(38, 20);
            this.btn_cleanBirthDate.Size = new System.Drawing.Size(38, 20);
            this.btn_cleanBirthDate.TabIndex = 41;
            this.btn_cleanBirthDate.Text = "x";
            this.btn_cleanBirthDate.ThemeName = "Material";
            this.btn_cleanBirthDate.Click += new System.EventHandler(this.btn_cleanBirthDate_Click);
            // 
            // date_birthDate
            // 
            this.date_birthDate.CalendarSize = new System.Drawing.Size(290, 320);
            this.date_birthDate.Location = new System.Drawing.Point(13, 142);
            this.date_birthDate.Name = "date_birthDate";
            this.date_birthDate.Size = new System.Drawing.Size(209, 36);
            this.date_birthDate.TabIndex = 39;
            this.date_birthDate.TabStop = false;
            this.date_birthDate.Text = "martes, 24 de diciembre de 2019";
            this.date_birthDate.ThemeName = "Material";
            this.date_birthDate.Value = new System.DateTime(2019, 12, 24, 16, 22, 45, 258);
            this.date_birthDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_phone_KeyDown);
            this.date_birthDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_phone_KeyPress);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel10.ForeColor = System.Drawing.Color.Red;
            this.radLabel10.Location = new System.Drawing.Point(446, 32);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(13, 20);
            this.radLabel10.TabIndex = 16;
            this.radLabel10.Text = "*";
            this.radLabel10.ThemeName = "Material";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.radLabel9.ForeColor = System.Drawing.Color.Red;
            this.radLabel9.Location = new System.Drawing.Point(209, 32);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(13, 20);
            this.radLabel9.TabIndex = 15;
            this.radLabel9.Text = "*";
            this.radLabel9.ThemeName = "Material";
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(245, 222);
            this.txt_email.MaskType = Telerik.WinControls.UI.MaskType.EMail;
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(214, 36);
            this.txt_email.TabIndex = 7;
            this.txt_email.TabStop = false;
            this.txt_email.ThemeName = "Material";
            this.txt_email.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_phone_KeyDown);
            this.txt_email.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_phone_KeyPress);
            // 
            // lbl_phone
            // 
            this.lbl_phone.Location = new System.Drawing.Point(13, 195);
            this.lbl_phone.Name = "lbl_phone";
            this.lbl_phone.Size = new System.Drawing.Size(48, 21);
            this.lbl_phone.TabIndex = 9;
            this.lbl_phone.Text = "Phone";
            this.lbl_phone.ThemeName = "Material";
            // 
            // lbl_email
            // 
            this.lbl_email.Location = new System.Drawing.Point(245, 195);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(48, 21);
            this.lbl_email.TabIndex = 5;
            this.lbl_email.Text = "E-Mail";
            this.lbl_email.ThemeName = "Material";
            // 
            // txt_phone
            // 
            this.txt_phone.Location = new System.Drawing.Point(13, 222);
            this.txt_phone.Mask = "(###) ###-####";
            this.txt_phone.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(209, 36);
            this.txt_phone.TabIndex = 6;
            this.txt_phone.TabStop = false;
            this.txt_phone.Text = "(___) ___-____";
            this.txt_phone.ThemeName = "Material";
            this.txt_phone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_phone_KeyDown);
            this.txt_phone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_phone_KeyPress);
            // 
            // lbl_birthDate
            // 
            this.lbl_birthDate.Location = new System.Drawing.Point(18, 113);
            this.lbl_birthDate.Name = "lbl_birthDate";
            this.lbl_birthDate.Size = new System.Drawing.Size(71, 21);
            this.lbl_birthDate.TabIndex = 7;
            this.lbl_birthDate.Text = "Birth Date";
            this.lbl_birthDate.ThemeName = "Material";
            // 
            // lbl_lastName
            // 
            this.lbl_lastName.Location = new System.Drawing.Point(245, 31);
            this.lbl_lastName.Name = "lbl_lastName";
            this.lbl_lastName.Size = new System.Drawing.Size(77, 21);
            this.lbl_lastName.TabIndex = 3;
            this.lbl_lastName.Text = "Last Name";
            this.lbl_lastName.ThemeName = "Material";
            // 
            // txt_lastName
            // 
            this.txt_lastName.Location = new System.Drawing.Point(245, 58);
            this.txt_lastName.Name = "txt_lastName";
            this.txt_lastName.Size = new System.Drawing.Size(214, 36);
            this.txt_lastName.TabIndex = 3;
            this.txt_lastName.Tag = "";
            this.txt_lastName.ThemeName = "Material";
            this.txt_lastName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_phone_KeyDown);
            this.txt_lastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_phone_KeyPress);
            // 
            // lbl_firstName
            // 
            this.lbl_firstName.Location = new System.Drawing.Point(13, 31);
            this.lbl_firstName.Name = "lbl_firstName";
            this.lbl_firstName.Size = new System.Drawing.Size(78, 21);
            this.lbl_firstName.TabIndex = 1;
            this.lbl_firstName.Text = "First Name";
            this.lbl_firstName.ThemeName = "Material";
            // 
            // txt_firstName
            // 
            this.txt_firstName.Location = new System.Drawing.Point(13, 58);
            this.txt_firstName.Name = "txt_firstName";
            this.txt_firstName.Size = new System.Drawing.Size(209, 36);
            this.txt_firstName.TabIndex = 2;
            this.txt_firstName.Tag = "";
            this.txt_firstName.ThemeName = "Material";
            this.txt_firstName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_phone_KeyDown);
            this.txt_firstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_phone_KeyPress);
            // 
            // lbl_profileImage
            // 
            this.lbl_profileImage.Location = new System.Drawing.Point(498, 197);
            this.lbl_profileImage.Name = "lbl_profileImage";
            this.lbl_profileImage.Size = new System.Drawing.Size(94, 21);
            this.lbl_profileImage.TabIndex = 30;
            this.lbl_profileImage.Text = "Profile Image";
            this.lbl_profileImage.ThemeName = "Material";
            // 
            // UserEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 386);
            this.Controls.Add(this.radPageView);
            this.Controls.Add(this.pnl_buttons);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(696, 423);
            this.MinimumSize = new System.Drawing.Size(472, 337);
            this.Name = "UserEdit";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(696, 423);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            ((System.ComponentModel.ISupportInitialize)(this.pnl_buttons)).EndInit();
            this.pnl_buttons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_userName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdown_roles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_pass1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_username)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_repeatPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_pass2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView)).EndInit();
            this.radPageView.ResumeLayout(false);
            this.radPageView.PerformLayout();
            this.page_userName.ResumeLayout(false);
            this.page_userName.PerformLayout();
            this.page_profile.ResumeLayout(false);
            this.page_profile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_addProfileImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img_profileImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_removeProfileImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroup_BasicInfo)).EndInit();
            this.radGroup_BasicInfo.ResumeLayout(false);
            this.radGroup_BasicInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanBirthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_birthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_phone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_phone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_birthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_lastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_lastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_firstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_firstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_profileImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel pnl_buttons;
        private Telerik.WinControls.UI.RadButton btn_save;
        private Telerik.WinControls.UI.RadLabel lbl_password;
        private Telerik.WinControls.UI.RadLabel lbl_userName;
        private Telerik.WinControls.UI.RadDropDownList dropdown_roles;
        private Telerik.WinControls.UI.RadTextBox txt_pass1;
        private Telerik.WinControls.UI.RadTextBox txt_username;
        private Telerik.WinControls.UI.RadLabel lbl_role;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadLabel lbl_repeatPassword;
        private Telerik.WinControls.UI.RadTextBox txt_pass2;
        private Telerik.WinControls.UI.RadLabel radLabel46;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadPageView radPageView;
        private Telerik.WinControls.UI.RadPageViewPage page_userName;
        private Telerik.WinControls.UI.RadPageViewPage page_profile;
        private Telerik.WinControls.UI.RadButton btn_addProfileImg;
        private System.Windows.Forms.PictureBox img_profileImage;
        private Telerik.WinControls.UI.RadButton btn_removeProfileImg;
        private Telerik.WinControls.UI.RadGroupBox radGroup_BasicInfo;
        private Telerik.WinControls.UI.RadButton btn_cleanBirthDate;
        private Telerik.WinControls.UI.RadDateTimePicker date_birthDate;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadMaskedEditBox txt_email;
        private Telerik.WinControls.UI.RadLabel lbl_phone;
        private Telerik.WinControls.UI.RadLabel lbl_email;
        private Telerik.WinControls.UI.RadMaskedEditBox txt_phone;
        private Telerik.WinControls.UI.RadLabel lbl_birthDate;
        private Telerik.WinControls.UI.RadLabel lbl_lastName;
        private Telerik.WinControls.UI.RadTextBox txt_lastName;
        private Telerik.WinControls.UI.RadLabel lbl_firstName;
        private Telerik.WinControls.UI.RadTextBox txt_firstName;
        private Telerik.WinControls.UI.RadLabel lbl_profileImage;
    }
}
