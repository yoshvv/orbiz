﻿using DesktopManagement.Alerts;
using DesktopManagement.Base;
using DesktopManagement.Helpers;
using DesktopManagement.Modules.Articles.List;
using DesktopManagement.Modules.Configuration.Edit;
using DesktopManagement.Modules.Security;
using DesktopManagement.Modules.Security.Users.Edit;
using DesktopManagement.Modules.Security.Users.List;
using DesktopManagement.Modules.Suppliers.List;
using DesktopManagement.Properties;
using DesktopManagement.Reports.Users;
using DesktopManagement.Resx;
using Logic.Modules;
using Logic.Modules.Dashboard;
using Logic.Modules.Security.Users;
using NLog;
using Persistence;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace DesktopManagement.Modules
{
    public partial class Main : Telerik.WinControls.UI.RadForm
    {
        public Main(Login l)
        {
            InitializeComponent();
            this.logic = new DashboardLogic();
            this.userLogic = new UserLogic();
            this.menuLogic = new MenuLogic();
            this.login = l;
            this.menuHelper = new MenuHelper();
            this.imageHelper = new Helpers.FileHelper();
            this.componentHelper = new ComponentHelper();
            this.RefreshIcon();
            this.Language();
            LoadProfile();
            LoadMenu();
            LoadDashboard();
        }

        private DashboardLogic logic { get; }

        private UserLogic userLogic { get; }

        private MenuLogic menuLogic { get; }

        private Login login { get; }

        private MenuHelper menuHelper { get; }

        private ComponentHelper componentHelper { get; }

        private Helpers.FileHelper imageHelper { get; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool AskToClose { get; set; } = true;

        public void SetAskToClose(bool val) 
        {
            this.AskToClose = val;
        }

        public void RefreshIcon()
        {
            GlobalConfigHelper.GetInstance().SetAppName(this);
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
        }

        public void LoadProfile()
        {
            using (var c = new Context()) 
            {
                var user = userLogic.GetUserProfileDTO(c, CredentialManager.IdUser);

                this.btn_profile.Text = $"{user.UserName} {user.RoleName}";
                if (user.BasicInfo.Avatar.File != null)
                    this.btn_profile.Image = imageHelper.ResizeImage(imageHelper.ArrayOfBytesToImage(user.BasicInfo.Avatar.File), 48, 48);
                else
                    this.btn_profile.Image = Resources.avatar_icon;
            }
        }

        public void LoadDashboard()
        {
            DateTime? init = null;
            DateTime? end = null;

            var getCards = logic.GetCardDTO(init, end);
            this.lbl_users.Text = Language.lbl_Users;
            this.lbl_usersCount.Text = string.Format("{0:#,0}", getCards.Users);
        }

        public void RefreshTreeView()
        {
            this.menu.Width = this.pnl_menu.Width;
            this.menu.Height = this.pnl_menu.Height - this.txt_filter.Height;
        }

        public void LoadMenu()
        {
            //Search filter textbox
            componentHelper.SetSearchIcon(txt_filter);

            //Menu list
            var menuList = this.menuLogic.GetMenuItems(CredentialManager.IdUser);
            this.menu.ThemeName = Properties.Settings.Default.Theme;
            this.menu.Location = new Point(0, this.txt_filter.Height);
            RefreshTreeView();

            RadTreeNode root = null;

            //Filling all the menu and sub menu items
            foreach (var m in menuList)
            {
                root = this.menu.Nodes.Add(m.DisplayName);
                root.Image = menuHelper.GetMenuImage(m.Name);
                root.Name = m.Name;
                root.Text = menuLogic.MenuFormat(m.DisplayName);

                var subMenuItems = this.menuLogic.GetSubMenuItems(m.IdMenuItem, CredentialManager.IdUser);
                foreach (var sm in subMenuItems)
                {
                    var radNode = new RadTreeNode();
                    radNode.Text = menuLogic.MenuFormat(sm.DisplayName);
                    radNode.Name = sm.Name;
                    root.Nodes.Add(radNode);
                }
            }

            //expanding all the elements
            this.menu.ExpandAll();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.login.ShowLogin();
        }

        private void txt_filter_TextChanged(object sender, EventArgs e)
        {
            this.menu.Filter = this.txt_filter.Text;
        }

        private void menu_NodeMouseDoubleClick(object sender, RadTreeViewEventArgs e)
        {
            var optionClicked = e.Node.Name;
            var obj = (RadTreeViewElement)sender;
            var option = obj.Nodes.FirstOrDefault(x => x.Name == optionClicked);

            //If option is null it means the user clicked
            //a parent menu, so we need to search in the childs
            if (option == null)
            {
                //Search in the submenus the element clicked
                var parents = obj.Nodes.SelectMany(x => x.Nodes).ToList();

                foreach (var child in parents)
                {
                    if (child.Name == optionClicked)
                    {
                        option = child;
                        break;
                    }
                }
            }

            //If there are no more noded it means the user clicked a module
            if (!option.Nodes.Any())
            {
                OpenCatalog(option.Name);
            }
        }

        public void OpenCatalog(string name)
        {
            switch (name)
            {
                case "Article":
                    new ArticleList().Show();
                    break;
                case "Supplier":
                    new SupplierList().Show();
                    break;
                case "Roles":
                    new RoleList().Show();
                    break;
                case "Users":
                    new UserList().Show();
                    break;
                case "Configuration":
                    new ConfigurationEdit(this).Show();
                    break;
                ///////Reports///////////
                case "UserReport":
                    new UsersReport().Show();
                    break;
            }
        }

        private void Main_ResizeEnd(object sender, EventArgs e)
        {
            RefreshTreeView();
        }

        public bool AskForExit() 
        {
            return AskToClose && AlertHelper.YesNoAlert(Language.message_Exit);
        }

        public void Exit() 
        {
            this.AskToClose = false;
            try
            {
                //saving all open forms
                List<Form> openForms = new List<Form>();

                //adding to another list the forms that will be closed
                foreach (var form in Application.OpenForms)
                {
                    var f = ((Form)form);
                    if (f.Visible && f.Name != "Main") 
                    {
                        openForms.Add(f);
                    }
                }

                //closing all the forms except the main form and the loginform
                foreach (var form in openForms) form.Close();

                //close main form
                this.Close();
            }
            catch (Exception ee) 
            {
                logger.Error(ee.InnerException);
            }
        }

        public void ExitAll()
        {
            this.login.ExitAll();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            LoadDashboard();
        }

        private void btn_viewProfile_Click(object sender, EventArgs e)
        {
            var idKey = CredentialManager.IdUser;
            var editForm = new UserEdit(false);
            editForm.LoadExisting(idKey);
            editForm.SetSelectedPage(2);
            editForm.ShowDialog();
            LoadProfile();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            if (AskForExit()) Exit();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AskForExit()) Exit();
            else e.Cancel = true;
        }
    }
}
