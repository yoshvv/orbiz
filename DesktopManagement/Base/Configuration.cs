﻿using Logic.Modules.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace DesktopManagement.Base
{
    public static class Configuration
    {
        public static CultureInfo GetCurrencyCulture() 
        {
            //Creating the culture
            var culture = new CultureInfo("ar-AE");
            
            return culture;
        }

        public static CultureInfo GetDateCulture()
        {
            //Creating the culture
            var culture = new CultureInfo("en-IN");

            return culture;
        }

        public static string GetDateFormat()
        {
            return "{0: dd/MMM/yyyy}";
        }

        public static string CurrencySymbol()
        {
            //Creating the culture
            var symbol = new RegionInfo(GetCurrencyCulture().LCID).CurrencySymbol;
            return symbol;
        }

        public static void Language(this RadForm form)
        {
            //Creating the culture
            var currencyCulture = GetCurrencyCulture();
            var dateCulture = GetDateCulture();

            //Getting all the controls of type RadMaskedEditBox
            var textBoxes = GetAll(form, typeof(RadMaskedEditBox))
              .Select(x=>(RadMaskedEditBox)x)
              .ToList();

            var dateTimePickers = GetAll(form, typeof(RadDateTimePicker))
                .Select(x=>(RadDateTimePicker)x)
                .ToList();

            //setting culture to controls
            foreach (var control in textBoxes) control.Culture = currencyCulture;
            foreach (var control in dateTimePickers) control.Culture = dateCulture;

        }

        public static IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
    }
}
