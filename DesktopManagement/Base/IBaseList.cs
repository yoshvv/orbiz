﻿using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopManagement.Base
{
    /// <summary>
    /// Interface for setting the architecture 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseList<T>
    {
        /// <summary>
        /// List for the grid list
        /// </summary>
        IReadOnlyList<T> List { get; set; }

        /// <summary>
        /// Get the data for the <see cref="List"/>
        /// </summary>
        /// <returns></returns>
        void  LoadData();

        /// <summary>
        /// Returns the selected row entity from the <see cref="List"/>.
        /// </summary>
        /// <remarks>
        /// Returns null if nothing selected
        /// </remarks>
        /// <returns></returns>
        T GetSelectedElement();

        /// <summary>
        /// Open the edit form with the new default entity values
        /// </summary>
        /// <returns></returns>
        void  Add();

        /// <summary>
        /// Open the edit form with the exiting entity values loaded
        /// </summary>
        /// <returns></returns>
        void  Edit();

        /// <summary>
        /// Ask the user for removing the selected entity
        /// </summary>
        /// <returns></returns>
        void  Remove();

        /// <summary>
        /// Set the curret user logged permission for this window
        /// </summary>
        /// <returns></returns>
        void  SetPermissions();

        /// <summary>
        /// Name of the current entity
        /// </summary>
        string EntityName { get; }

        /// <summary>
        /// Current user's permissions
        /// </summary>
        Permission Permissions { get; set; }
    }
}