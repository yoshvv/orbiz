﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopManagement.Base
{
    /// <summary>
    /// Interface for the edit form
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseEdit<T>
    {
        string EntityName { get; }

        bool IsNew { get; }

        /// <summary>
        /// Main object to store the entity data
        /// </summary>
        T Entity { get; set; }

        /// <summary>
        /// Set the current entity's title into the edit form
        /// </summary>
        void  SetTitle();

        /// <summary>
        /// Function to load the existing entity value to the form
        /// </summary>
        /// <param name="idKey"></param>
        /// <returns></returns>
        void LoadExisting(int idKey);

        /// <summary>
        /// Do all the validation for the current entity
        /// </summary>
        /// <returns></returns>
        bool DoValidation();

        /// <summary>
        /// Bind all the values in the form to the <see cref="Entity"/>
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T GetValues(T entity);

        /// <summary>
        /// Add or update the current <see cref="Entity"/> into the DB
        /// </summary>
        /// <returns></returns>
        void Save();

        /// <summary>
        /// Check the validations of the form and after that save the entity
        /// </summary>
        /// <returns></returns>
        void DoValidationAndSave();
    }
}