﻿using DesktopManagement.Helpers;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;

namespace DesktopManagement.Reports.Balance
{
    public partial class BaseReport : RadForm
    {
        public BaseReport()
        {
            InitializeComponent();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
        }

        public void LoadReport(List<ReportDataSource> rds, string reportPath) 
        {
            foreach(var r in rds)
            this.reportViewer1.LocalReport.DataSources.Add(r);

            this.reportViewer1.LocalReport.ReportPath = reportPath;
            this.reportViewer1.RefreshReport();
        }

        private void BalanceReportVM_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
