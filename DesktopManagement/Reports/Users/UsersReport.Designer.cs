﻿namespace DesktopManagement.Reports.Users
{
    partial class UsersReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersReport));
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.combo_members = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lbl_Role = new Telerik.WinControls.UI.RadLabel();
            this.btn_cleanEndDate = new Telerik.WinControls.UI.RadButton();
            this.btn_cleanStartDate = new Telerik.WinControls.UI.RadButton();
            this.date_end = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lbl_endDate = new Telerik.WinControls.UI.RadLabel();
            this.date_start = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lbl_startDate = new Telerik.WinControls.UI.RadLabel();
            this.lbl_title = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btn_generate = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.combo_members)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_members.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_members.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_Role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_endDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_startDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_generate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // combo_members
            // 
            this.combo_members.AutoSize = true;
            // 
            // combo_members.NestedRadGridView
            // 
            this.combo_members.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.combo_members.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_members.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.combo_members.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.combo_members.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.combo_members.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.combo_members.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.combo_members.EditorControl.MasterTemplate.EnableGrouping = false;
            this.combo_members.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.combo_members.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.combo_members.EditorControl.Name = "NestedRadGridView";
            this.combo_members.EditorControl.ReadOnly = true;
            this.combo_members.EditorControl.ShowGroupPanel = false;
            this.combo_members.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.combo_members.EditorControl.TabIndex = 0;
            this.combo_members.Location = new System.Drawing.Point(12, 271);
            this.combo_members.Name = "combo_members";
            this.combo_members.Size = new System.Drawing.Size(344, 36);
            this.combo_members.TabIndex = 44;
            this.combo_members.TabStop = false;
            this.combo_members.Text = "Select a Role";
            this.combo_members.ThemeName = "Material";
            // 
            // lbl_Role
            // 
            this.lbl_Role.Location = new System.Drawing.Point(12, 244);
            this.lbl_Role.Name = "lbl_Role";
            this.lbl_Role.Size = new System.Drawing.Size(36, 21);
            this.lbl_Role.TabIndex = 45;
            this.lbl_Role.Text = "Role";
            this.lbl_Role.ThemeName = "Material";
            // 
            // btn_cleanEndDate
            // 
            this.btn_cleanEndDate.Location = new System.Drawing.Point(318, 156);
            this.btn_cleanEndDate.MaximumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanEndDate.MinimumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanEndDate.Name = "btn_cleanEndDate";
            // 
            // 
            // 
            this.btn_cleanEndDate.RootElement.MaxSize = new System.Drawing.Size(38, 20);
            this.btn_cleanEndDate.RootElement.MinSize = new System.Drawing.Size(38, 20);
            this.btn_cleanEndDate.Size = new System.Drawing.Size(38, 20);
            this.btn_cleanEndDate.TabIndex = 41;
            this.btn_cleanEndDate.Text = "x";
            this.btn_cleanEndDate.ThemeName = "Material";
            this.btn_cleanEndDate.Click += new System.EventHandler(this.btn_cleanEndDate_Click);
            // 
            // btn_cleanStartDate
            // 
            this.btn_cleanStartDate.Location = new System.Drawing.Point(318, 69);
            this.btn_cleanStartDate.MaximumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanStartDate.MinimumSize = new System.Drawing.Size(38, 20);
            this.btn_cleanStartDate.Name = "btn_cleanStartDate";
            // 
            // 
            // 
            this.btn_cleanStartDate.RootElement.MaxSize = new System.Drawing.Size(38, 20);
            this.btn_cleanStartDate.RootElement.MinSize = new System.Drawing.Size(38, 20);
            this.btn_cleanStartDate.Size = new System.Drawing.Size(38, 20);
            this.btn_cleanStartDate.TabIndex = 40;
            this.btn_cleanStartDate.Text = "x";
            this.btn_cleanStartDate.ThemeName = "Material";
            this.btn_cleanStartDate.Click += new System.EventHandler(this.btn_cleanStartDate_Click);
            // 
            // date_end
            // 
            this.date_end.CalendarSize = new System.Drawing.Size(290, 320);
            this.date_end.Culture = new System.Globalization.CultureInfo("en-IN");
            this.date_end.Location = new System.Drawing.Point(12, 182);
            this.date_end.Name = "date_end";
            this.date_end.Size = new System.Drawing.Size(344, 36);
            this.date_end.TabIndex = 39;
            this.date_end.TabStop = false;
            this.date_end.ThemeName = "Material";
            this.date_end.Value = new System.DateTime(((long)(0)));
            // 
            // lbl_endDate
            // 
            this.lbl_endDate.Location = new System.Drawing.Point(11, 156);
            this.lbl_endDate.Name = "lbl_endDate";
            this.lbl_endDate.Size = new System.Drawing.Size(65, 21);
            this.lbl_endDate.TabIndex = 38;
            this.lbl_endDate.Text = "End Date";
            this.lbl_endDate.ThemeName = "Material";
            // 
            // date_start
            // 
            this.date_start.CalendarSize = new System.Drawing.Size(290, 320);
            this.date_start.Culture = new System.Globalization.CultureInfo("en-IN");
            this.date_start.Location = new System.Drawing.Point(12, 95);
            this.date_start.Name = "date_start";
            this.date_start.Size = new System.Drawing.Size(344, 36);
            this.date_start.TabIndex = 37;
            this.date_start.TabStop = false;
            this.date_start.ThemeName = "Material";
            this.date_start.Value = new System.DateTime(((long)(0)));
            // 
            // lbl_startDate
            // 
            this.lbl_startDate.Location = new System.Drawing.Point(11, 69);
            this.lbl_startDate.Name = "lbl_startDate";
            this.lbl_startDate.Size = new System.Drawing.Size(72, 21);
            this.lbl_startDate.TabIndex = 36;
            this.lbl_startDate.Text = "Start Date";
            this.lbl_startDate.ThemeName = "Material";
            // 
            // lbl_title
            // 
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Underline);
            this.lbl_title.Location = new System.Drawing.Point(12, 11);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(143, 25);
            this.lbl_title.TabIndex = 43;
            this.lbl_title.Text = "Report of Users";
            this.lbl_title.ThemeName = "Material";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radPanel1.Controls.Add(this.btn_generate);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 317);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(368, 48);
            this.radPanel1.TabIndex = 42;
            // 
            // btn_generate
            // 
            this.btn_generate.Image = global::DesktopManagement.Properties.Resources.char_icon;
            this.btn_generate.Location = new System.Drawing.Point(207, 7);
            this.btn_generate.Name = "btn_generate";
            this.btn_generate.Size = new System.Drawing.Size(149, 36);
            this.btn_generate.TabIndex = 23;
            this.btn_generate.Text = "Generate";
            this.btn_generate.ThemeName = "Material";
            this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
            // 
            // UsersReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 365);
            this.Controls.Add(this.combo_members);
            this.Controls.Add(this.lbl_Role);
            this.Controls.Add(this.btn_cleanEndDate);
            this.Controls.Add(this.btn_cleanStartDate);
            this.Controls.Add(this.date_end);
            this.Controls.Add(this.lbl_endDate);
            this.Controls.Add(this.date_start);
            this.Controls.Add(this.lbl_startDate);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.radPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(370, 402);
            this.MinimumSize = new System.Drawing.Size(370, 402);
            this.Name = "UsersReport";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(370, 402);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.UsersReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.combo_members.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_members.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.combo_members)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_Role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cleanStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_endDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_startDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_generate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadMultiColumnComboBox combo_members;
        private Telerik.WinControls.UI.RadLabel lbl_Role;
        private Telerik.WinControls.UI.RadButton btn_cleanEndDate;
        private Telerik.WinControls.UI.RadButton btn_cleanStartDate;
        private Telerik.WinControls.UI.RadDateTimePicker date_end;
        private Telerik.WinControls.UI.RadLabel lbl_endDate;
        private Telerik.WinControls.UI.RadDateTimePicker date_start;
        private Telerik.WinControls.UI.RadLabel lbl_startDate;
        private Telerik.WinControls.UI.RadLabel lbl_title;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btn_generate;
    }
}
