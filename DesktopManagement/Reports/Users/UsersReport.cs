﻿using DesktopManagement.Helpers;
using DesktopManagement.Reports.Balance;
using DesktopManagement.Resx;
using Logic.Modules.Dashboard;
using Logic.Modules.Security;
using Logic.Modules.Security.Roles;
using Logic.Modules.Security.Users;
using Microsoft.Reporting.WinForms;
using Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using DataReport = DesktopManagement.Reports.Users.UsersDataReport;
using Filters = DesktopManagement.Reports.Users.UsersDataReportHeader;

namespace DesktopManagement.Reports.Users
{
    public partial class UsersReport : Telerik.WinControls.UI.RadForm
    {
        public UsersReport()
        {
            InitializeComponent();
            roleLogic = new RoleLogic();
            userLogic = new UserLogic();
            dashboardLogic = new DashboardLogic();
            componentHelper = new ComponentHelper();
            this.fileHelper = new FileHelper();
            SetStrings();
            GlobalConfigHelper.GetInstance().SetAppIcon(this);
            LoadMembers();
        }

        private FileHelper fileHelper { get; }

        private RoleLogic roleLogic { get; }

        private UserLogic userLogic { get; }

        private DashboardLogic dashboardLogic { get; }

        private ComponentHelper componentHelper { get; }

        private IReadOnlyList<RoleDTO> Roles;

        /// <summary>
        /// Set all the different text's values of the components
        /// </summary>
        public void SetStrings()
        {
            Text = Language.report_Users_Page_Title;
            lbl_title.Text = Language.report_Users_Title;
            lbl_startDate.Text = Language.lbl_Start_Date;
            lbl_endDate.Text = Language.lbl_End_Date;
            lbl_Role.Text = Language.lbl_Role;
            combo_members.Text = Language.lbl_Select_Role;
            btn_generate.Text = Language.btn_Generate;
        }

        public void LoadMembers()
        {
            Roles = roleLogic.Query();
            this.componentHelper.SetDefaultGridConfig(combo_members.EditorControl);

            combo_members.MultiColumnComboBoxElement.Columns.Add(new GridViewTextBoxColumn(uniqueName: "Name", fieldName: "Name"));
            combo_members.MultiColumnComboBoxElement.Columns["Name"].HeaderText = "Role";

            combo_members.EditorControl.ReadOnly = true;
            combo_members.DropDownSizingMode = SizingMode.UpDownAndRightBottom;
            combo_members.DropDownMinSize = new Size(420, 300);

            //Binding list into combo
            combo_members.BindDataToGrid(Roles);
            combo_members.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            combo_members.DisplayMember = "Name";

            FilterDescriptor descriptor = new FilterDescriptor(this.combo_members.DisplayMember, FilterOperator.StartsWith, string.Empty);
            this.combo_members.EditorControl.FilterDescriptors.Add(descriptor);
            combo_members.SelectedIndex = -1;
        }

        public IReadOnlyList<DataReport> GetReportInfo()
        {
            using (var c = new Context())
            {
                int? idRole = null;

                if (combo_members.SelectedIndex >= 0)
                {
                    idRole = Roles[combo_members.SelectedIndex].IdRole;
                }

                var payments = userLogic.Query(idRole: idRole)
                     .Select(x => new DataReport()
                     {
                         UserName = x.Username,
                         RoleName = x.RoleName,
                         CreatedDate = x.CreatedDate,
                     })
                     .OrderBy(x => x.RoleName)
                     .ToList();

                return payments;
            }
        }

        public void GenerateReport()
        {
            //Get all the data of the report
            var infoReporte = GetReportInfo();

            //Create the datasource to send it to the RDLC report
            ReportDataSource rds = new ReportDataSource("UsersDataSet", infoReporte);

            string ini = "";
            string end = "";

            if (ReportHelper.DateTimeIsNull(date_start.Value)) ini = infoReporte.Select(x => x.CreatedDate).Min().ToString("MM/dd/yyyy");
            else ini = date_start.Value.ToString("MM/dd/yyyy");

            if (ReportHelper.DateTimeIsNull(date_end.Value)) end = infoReporte.Max(x => x.CreatedDate).ToString("MM/dd/yyyy");
            else end = date_end.Value.ToString("MM/dd/yyyy");

            var header = new UsersDataReportHeader()
            {
                Logo = fileHelper.GetLogo(),
                Title = "Users Report",
                Ini = ini,
                End = end,
            };

            ReportDataSource rds_filters = new ReportDataSource("UsersHeaderDataSet", new Filters[] { header });

            var report = new BaseReport();
            var reportPath = string.Join("//", this.GetType().Namespace.Split('.').Skip(1)) + "//" + Name + ".rdlc";
            report.LoadReport(new List<ReportDataSource>() { rds, rds_filters }, reportPath);
            report.Show();
        }

        private void btn_generate_Click(object sender, EventArgs e)
        {
            GenerateReport();
        }

        private void btn_cleanStartDate_Click(object sender, EventArgs e)
        {
            date_start.NullableValue = null;
        }

        private void btn_cleanEndDate_Click(object sender, EventArgs e)
        {
            date_end.NullableValue = null;
        }

        private void UsersReport_Load(object sender, EventArgs e)
        {

        }
    }
}
