﻿namespace DesktopManagement.Reports.Users
{
    public class UsersDataReportHeader
    {
        public string Title { get; set; }

        public byte[] Logo { get; set; }

        public string Ini { get; set; }

        public string End { get; set; }
    }
}
