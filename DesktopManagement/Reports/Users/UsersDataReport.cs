﻿using System;

namespace DesktopManagement.Reports.Users
{
    public class UsersDataReport
    {
        public string UserName { get; set; }

        public string RoleName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}