﻿using System;

namespace DesktopManagement.Reports.Users
{
    public class UsersFiltersDataReport
    {
        public int? IdRole { get; set; }

        public DateTime? Ini { get; set; }

        public DateTime? End { get; set; }
    }
}