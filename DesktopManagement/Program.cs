﻿using DesktopManagement.Modules;
using DesktopManagement.Modules.Security;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DesktopManagement
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void  Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }
    }
}