﻿using LinqKit;
using Logic.Modules.Base;
using Persistence;
using Persistence.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Entity = Persistence.Models.Suppliers.Supplier;
using EntityDTO = Logic.Modules.Suppliers.SupplierDTO;
using Parameters = Logic.Modules.Suppliers.SupplierParams;

namespace Logic.Modules.Suppliers
{
    public class SupplierLogic
    {

        public Expression<Func<SupplierPrice, SupplierPriceDTO>> ToSupplierPriceDTO = (x) =>
         new SupplierPriceDTO
         {
             IdArticle = x.IdArticle,
             IdSupplierPrice = x.IdSupplierPrice,
             IdSupplier = x.IdSupplier,
             CreatedDate = x.CreatedDate,
             PurchasePrice = x.PurchasePrice,
             SalePrice = x.SalePrice,
             SupplierCode = x.Supplier.Code + "",
             SupplierName = x.Supplier.Name,
         }
        ;

        public Expression<Func<Entity, EntityDTO>> ToDTO = (x) =>
           new EntityDTO
           {
               IdSupplier = x.IdSupplier,
                FullName =  ExpressionLogic.Name.Invoke(x),
                Name = x.Name,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Code = x.Code,
                Phone = x.Phone,
                EMail = x.EMail,
                Address = x.Address,
                CreatedDate = x.CreatedDate
           }
        ;

        /// <summary>
        /// List of <see cref="Entity"/>
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IReadOnlyList<EntityDTO> Query(Parameters p)
        {
            using (var c = new Context())
            {
                return c.Supplier
                    .AsExpandable()
                    .Select(ToDTO)
                    .WhereContains(x => x.Name, p.Name)
                    .WhereContains(x => x.Code + "", p.Code)
                    .ToList();
            }
        }

        public IReadOnlyList<SupplierPriceDTO> QuerySupplierPrice(int idArticle)
        {
            using (var c = new Context())
            {
                return c.SupplierPrice
                    .AsExpandable()
                    .Where(x=>x.IdArticle == idArticle)
                    .Select(x => ToSupplierPriceDTO.Invoke(x))
                    .AsExpandable()
                    .ToList();
            }
        }
        public IReadOnlyList<EntityDTO> Query()
        {
            var parameters = new Parameters()
            {
                Name = "",
                Code = "",
            };

            return Query(parameters);
        }

        /// <summary>
        /// Add a new entity of type <see cref="Entity"/> to the context
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Add(Context c, Entity entity)
        {
            c.Supplier.Add(entity);
            c.SaveChanges();
        }

        /// <summary>
        /// Save the changes in the current context
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Update(Context c, Entity entity)
        {
            c.SaveChanges();
        }

        /// <summary>
        /// Validate if a Supplier can be deleted
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CanDelete(int id)
        {
            using (var c = new Context())
            {
                var supplierHasPrices = c.SupplierPrice
                    .Where(x => x.IdSupplier == id)
                    .Any();

                return !supplierHasPrices;
            }
        }

        /// <summary>
        /// Delete the entity <see cref="Entity"/> given the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Delete(int id)
        {
            using (var c = new Context())
            {
                //remove the Supplier
                c.Supplier.Remove(Get(c, id));

                //save changes in the database
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Get the entity <see cref="Entity"/> given the id
        /// </summary>
        /// <param name="c"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity Get(Context c, int id)
        {
            return c.Supplier
                .Where(x => x.IdSupplier == id)
                .FirstOrDefault()
                ;
        }

        /// <summary>
        /// True if the code is repeated in entity <see cref="Supplier"/>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool IsCodeRepeated(int code)
        {
            using (var c = new Context())
            {
                return c.Supplier
                    .Where(x => x.Code == code)
                    .Any();
            }
        }

        public bool SupplierNameIsRepeated(int idKey, string name)
        {
            using (var c = new Context())
            {
                return c.Supplier
                    .Where(x => x.IdSupplier != idKey)
                    .Where(x => x.Name.ToLower() == name.ToLower())
                    .Any();
            }
        }

        /// <summary>
        /// Returns the next available number in entity <see cref="Supplier"/>
        /// </summary>
        /// <returns></returns>
        public int NextCode()
        {
            using (var c = new Context())
            {
                var maxCode = c.Supplier.Select(x => (int?)x.Code).Max();

                if (maxCode == null) return 1;
                else return maxCode.Value + 1;
            }
        }
    }
}
