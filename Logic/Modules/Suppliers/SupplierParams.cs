﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Suppliers
{
    public class SupplierParams
    {
        public string Name { get; set; }

        public string Code { get; set; }
    }
}
