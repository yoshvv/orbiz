﻿using Persistence.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Suppliers
{
    public class SupplierDTO: Supplier
    {
        public string FullName { get; set; }
    }
}
