﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Dashboard
{
    public class DataPoint
    {
        public int Total { get; set; }

        public string Name { get; set; }
    }
}
