﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Dashboard
{
    public class CardDTO
    {
        public int Users { get; set; }
        public int Members { get; set; }
        public int Trainers { get; set; }
        public int Vouchers { get; set; }
    }
}
