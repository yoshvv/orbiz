﻿using LinqKit;
using Logic.Modules.Base;
using Logic.Modules.Configuration;
using Persistence;
using System;
using System.Linq;

namespace Logic.Modules.Dashboard
{
    public class DashboardLogic
    {
        public DashboardLogic() 
        {
            this.globalConfigLogic = new GlobalConfigurationLogic();
        }

        private GlobalConfigurationLogic globalConfigLogic { get; }

        /// <summary>
        /// Get the totals of the users, members, trainers and vouchers
        /// </summary>
        /// <returns></returns>
        public CardDTO GetCardDTO(DateTime? ini, DateTime? end) 
        {
            using (var c = new Context())
            {
                var users = c.User
                    .AsExpandable()
                    .BetweenRange(x => x.CreatedDate, ini , end)
                    .Count();

                return new CardDTO() 
                {
                    Users = users,
                    Members = 0,
                    Trainers = 0,
                    Vouchers = 0
                };
            }
        }
    }
}