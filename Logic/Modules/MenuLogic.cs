﻿using LinqKit;
using Persistence;
using Persistence.Models;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules
{
    public class MenuLogic
    {
        public static Expression<Func<Context, MenuItem, Permission, bool>> GetMenuPermissionExpr = (c, menu, p) =>
            menu.Name.ToLower() == "article" ? p.Article.Visualize :
            menu.Name.ToLower() == "security" ? p.User.Visualize || p.Roles.Visualize || p.Database.Visualize :
            menu.Name.ToLower() == "supplier" ? p.Supplier.Visualize :
            //Reports
            menu.Name.ToLower() == "reports" ? p.UserReport.Visualize
            : false
        ;

        public static Expression<Func<Context, SubMenuItem, Permission, bool>> GetSubMenuPermissionExpr = (c, menu, p) =>
            menu.Name.ToLower() == "configuration" ? p.Configuration.Visualize :
            menu.Name.ToLower() == "users" ? p.User.Visualize :
            menu.Name.ToLower() == "roles" ? p.Roles.Visualize :
            menu.Name.ToLower() == "databasemanagement" ? p.Database.Visualize :
            //Reports
            menu.Name.ToLower() == "userreport" ? p.UserReport.Visualize : false
        ;

        /// <summary>
        /// Get all the menu items from the DB
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<MenuItem> GetMenuItems(int idUser)
        {
            using (var c = new Context())
            {
                //Get the user's role
                var idRole = c.Role
                    .Where(x => x.IdRole == (c.User.Where(y => y.IdUser == idUser).Select(y => y.IdRole).FirstOrDefault()))
                    .Select(x => x.IdRole)
                    .FirstOrDefault();


                return c.MenuItem
                    .AsExpandable()
                    .OrderBy(x => x.DisplayName)
                    .Select(x => new 
                    {
                        Menu = x,
                        Visible = GetMenuPermissionExpr.Invoke(c, x, c.Permission
                        .Where(y => y.IdRole == idRole)
                        .FirstOrDefault()),
                    })
                    .Where(x => x.Visible)
                    .Select(x => x.Menu)
                    .ToList();
            }
        }

        /// <summary>
        /// Get all the submenu items given an <see cref="idMenuItem"/>
        /// </summary>
        /// <param name="idMenuItem"></param>
        /// <returns></returns>
        public IReadOnlyList<SubMenuItem> GetSubMenuItems(int idMenuItem, int idUser)
        {
            using (var c = new Context())
            {
                //Get the user's role
                var idRole = c.Role
                    .Where(x => x.IdRole == (c.User.Where(y => y.IdUser == idUser).Select(y => y.IdRole).FirstOrDefault()))
                    .Select(x => x.IdRole)
                    .FirstOrDefault();

                var subMenus = c.SubMenuItem
                        .AsExpandable()
                        .Where(x => x.IdMenuItem == idMenuItem)
                      .Select(x => new
                      {
                          Menu = x,
                          Visible = GetSubMenuPermissionExpr.Invoke(c, x, c.Permission
                                .Where(y => y.IdRole == idRole)
                                .FirstOrDefault()),
                      })
                    .Where(x => x.Visible)
                    .Select(x => x.Menu)
                    .OrderBy(x => x.Name)
                    .ToList();

                return subMenus;
            }
        }

        public string MenuFormat(string str)
        {
            var menuList = str
                .Select((x, i) => i == 0 ? Char.ToUpper(x).ToString() :
                                        (Char.IsUpper(x) ? " " : "") + Char.ToLower(x).ToString())
                .ToList();

            return string.Join("", menuList);
        }
    }
}