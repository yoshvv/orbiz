﻿using Logic.Modules.Interfaces;
using Persistence;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Base
{
    public abstract class BaseEntity<T> where T: BaseEntity<T>
    {

        /// <summary>
        /// Returns a list of entity given the parameters
        /// </summary>
        /// <param name="c"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public abstract IReadOnlyList<T> Query(Context c, IParams parameters);

        /// <summary>
        /// Add to the current context a new entity
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        public void Add(Context c, T entity)
        {
            c.Set<T>().Add((T)this);
            c.SaveChanges();
        }

        /// <summary>
        /// Remove from database an existig identity
        /// </summary>
        /// <param name="id"></param>
        public abstract void  Delete(int id);

        /// <summary>
        /// Get the entity given the id
        /// </summary>
        /// <param name="c"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract T Get(Context c, int id);
        
        /// <summary>
        /// True if this code is repeated in this entity
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public abstract bool IsCodeRepeated(int code);

        /// <summary>
        /// Returns the next available code of this entity
        /// </summary>
        /// <returns></returns>
        public abstract int NextCode();
    }
}
