﻿using LinqKit;
using Logic.Modules.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Base
{
    public static class BaseExpression
    {
        /// <summary>
        /// Check if the principal date is between the <see cref="init"/> and the <see cref="end"/> dates
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryable"></param>
        /// <param name="selector"></param>
        /// <param name="init"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static IQueryable<T> BetweenRange<T>(this IQueryable<T> queryable, Expression<Func<T, DateTime>> selector, DateTime? init, DateTime? end)
        {
            var predicate = selector.Compose(myDate =>
                            DateTimeIsNull.Invoke(init) && DateTimeIsNull.Invoke(end) ? true :
                            !DateTimeIsNull.Invoke(init) && DateTimeIsNull.Invoke(end) ? ToDate.Invoke(myDate) == ToDate.Invoke(init) :
                            DateTimeIsNull.Invoke(init) && !DateTimeIsNull.Invoke(end) ? ToDate.Invoke(myDate) == ToDate.Invoke(end) :
                            (ToDate.Invoke(myDate) >= ToDate.Invoke(init) && ToDate.Invoke(myDate) <= ToDate.Invoke(end)))
                            ;

            return queryable
                .Where(predicate);
        }

        public static Expression<Func<TFirstParam, TResult>> Compose<TFirstParam, TIntermediate, TResult>(
            this Expression<Func<TFirstParam, TIntermediate>> first,
            Expression<Func<TIntermediate, TResult>> second)
        {
            var param = Expression.Parameter(typeof(TFirstParam), "param");

            var newFirst = first.Body.Replace(first.Parameters[0], param);
            var newSecond = second.Body.Replace(second.Parameters[0], newFirst);

            return Expression.Lambda<Func<TFirstParam, TResult>>(newSecond, param);
        }

        public static Expression Replace(this Expression expression, Expression searchEx, Expression replaceEx)
        {
            return new ReplaceVisitor(searchEx, replaceEx).Visit(expression);
        }

        internal class ReplaceVisitor : System.Linq.Expressions.ExpressionVisitor
        {
            private readonly Expression from, to;
            public ReplaceVisitor(Expression from, Expression to)
            {
                this.from = from;
                this.to = to;
            }
            public override Expression Visit(Expression node)
            {
                return node == from ? to : base.Visit(node);
            }
        }

        public static Expression<Func<DateTime?, bool>> DateTimeIsNull = d =>
          (d == null || d == default(DateTime));

        public static Expression<Func<DateTime?, DateTime?>> ToDate = x =>
           DbFunctions.CreateDateTime(x.Value.Year, x.Value.Month, x.Value.Day, 0, 0, 0);

        /// <summary>
        /// Chech if <see cref="search"/> string is contained in <see cref="str"/> string.
        /// </summary>
        public static IQueryable<T> WhereContains<T>(this IQueryable<T> queryable, Expression<Func<T, string>> selector, string search)
        {
            var predicate = selector.Compose(x => search == "" ? true :
                        x.ToLower().Contains(search.ToLower()));

            return queryable
                .Where(predicate);
        }

        public static IQueryable<T> WhereEqualsNullable<T>(this IQueryable<T> queryable, Expression<Func<T, Enum>> selector, Enum search)
        {
            var predicate = selector.Compose(x => search == null ? true :
                        x == search);

            return queryable
                .Where(predicate);
        }

        public static IQueryable<T> WhereEqualsNullable<T>(this IQueryable<T> queryable, Expression<Func<T, int>> selector, int? search)
        {
            var predicate = selector.Compose(x => search == null ? true :
                        x == search);

            return queryable
                .Where(predicate);
        }


        public static IQueryable<T> WhereEqualsNullable<T>(this IQueryable<T> queryable, Expression<Func<T, string>> selector, string search)
        {
            var predicate = selector.Compose(x => search == null ? true : x.ToLower() == search.ToLower());

            return queryable
                .Where(predicate);
        }

        public static Expression<Func<string,string>> TrimExpr = str => str.Replace(" ", "");
        
    }
}
