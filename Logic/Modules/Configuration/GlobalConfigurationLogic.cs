﻿using Persistence;
using System.Linq;
using Entity = Persistence.Models.Configuration.GlobalConfiguration;
namespace Logic.Modules.Configuration
{
    public class GlobalConfigurationLogic
    {
        public Entity GetConfiguration(Context c)
        {
            return c.GlobalConfiguration.FirstOrDefault();
        }

        public Entity GetConfiguration() 
        {
            using (var c = new Context()) 
            {
                return c.GlobalConfiguration.FirstOrDefault();
            }
        }

        public Entity GetCurrentConfigurationOrDefault(string appName = "")
        {
            var config = GetConfiguration();
            if (config == null) 
            {
                config = DefaultConfiguration(appName);
            }
            return config;
        }

        public Entity DefaultConfiguration(string appName = "")
        {
            return new Entity() 
            {
                AppName = appName,
                ICO = new Persistence.Models.Files.FileData()
                {
                    File = null,
                    FileName = "",
                    Type = ""
                },
                Logo = new Persistence.Models.Files.FileData()
                {
                    File = null,
                    FileName = "",
                    Type = ""
                }
            };
        }

        public void AddDefaultConfiguration() 
        {
            using (var c = new Context())
            {
                c.GlobalConfiguration.Add(DefaultConfiguration());
                c.SaveChanges();
            }
        }

        public bool ExistsConfiguration()
        {
            using (var c = new Context())
            {
                return c.GlobalConfiguration.Any();
            }
        }

        public void Update(Context c, Entity entity)
        {
            c.SaveChanges();
        }
    }
}