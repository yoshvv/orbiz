﻿using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Security.Roles
{
    public class RoleDTO: Role
    {
        public bool AllPermissions { get; set; }
    }
}