﻿using Logic.Modules.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Security
{
    /// <summary>
    /// Parameters to filter the role query
    /// </summary>
    public class RoleParams: IParams
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Name { get; set; }

        public int? IdBranch { get; set; }
    }
}
