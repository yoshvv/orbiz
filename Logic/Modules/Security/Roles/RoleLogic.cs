﻿using LinqKit;
using Logic.Modules.Base;
using Logic.Modules.Security.Roles;
using Persistence;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity = Persistence.Models.Security.Role;
namespace Logic.Modules.Security
{
    public class RoleLogic
    {

        /// <summary>
        /// Get a Role entity given the IdRole
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Entity GetRole(Context c, int idRole)
        {
            return c.Role
                .Where(x => x.IdRole == idRole)
                .FirstOrDefault();
        }


        public Entity GetRole(string name)
        {
            using (var c = new Context()) 
            {
                return c.Role
                     .Where(x => x.Name.ToLower() == name.ToLower())
                     .FirstOrDefault();
            }
        }

        /// <summary>
        /// Get a Role entity given a name
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Entity GetRole(Context c, string name) 
        {
            return c.Role.Where(x => x.Name.ToLower() == name).FirstOrDefault();
        }

        /// <summary>
        /// Check if the name is already registered by another role
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsNameRepeated(string name, int idRole = 0) 
        {
            using (var c = new Context()) 
            {
                return c.Role
                    .Where(x => x.Name.ToLower() == name.ToLower() && x.IdRole != idRole)
                    .Any();
            }
        }

        /// <summary>
        /// List of roles filtered by <see cref="RoleParams"/>
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IReadOnlyList<RoleDTO> Query(RoleParams p) 
        {
            using (var c = new Context()) 
            {
                return c.Role
                    .AsExpandable()
                    .WhereContains(x => x.Name, p.Name)
                    .Select(x => new RoleDTO 
                    {
                        IdRole = x.IdRole,
                        Name = x.Name,
                        Description = x.Description
                    })
                    .ToList();
            }
        }

        public IReadOnlyList<RoleDTO> Query()
        {
            var parameters = new RoleParams() 
            {
                Name = "",
                StartDate = null,
                EndDate = null
            };

            return Query(parameters);
        }

        public void Add(Context c, string name, string description)
        {
            var entity = new Role()
            {
                Name = name,
                Description = description
            };

            //Save changes
            Add(c, entity);
        }

        /// <summary>
        /// Add the new role to the database
        /// </summary>
        /// <param name="c"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public void Add(Context c, Entity entity)
        {
            //Save changes
            c.Role.Add(entity);
            c.SaveChanges();
        }

        /// <summary>
        /// Save the current entity's changes into the database
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Update(Context c, Entity entity)
        {
            c.SaveChanges();
        }

        /// <summary>
        /// True if this rol can be deleted
        /// </summary>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public bool CanDelete(int idRole)
        {
            using (var c = new Context())
            {
                return !(c.User.Where(x=>x.IdRole == idRole).Any());
            }
        }

        /// <summary>
        /// Remove an existing role entity
        /// </summary>
        /// <param name="c"></param>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Remove(int idRole)
        {
            using (var c = new Context())
            {
                var role = GetRole(c, idRole);
                c.Role.Remove(role);
                c.SaveChanges();
            }                
        }
    }
}