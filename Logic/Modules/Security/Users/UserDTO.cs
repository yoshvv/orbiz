﻿using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Security.Users
{
    public class UserDTO: User
    {
        public string FullName { get; set; }
        public string RoleName { get; set; }
    }
}