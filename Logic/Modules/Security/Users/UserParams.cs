﻿using Logic.Modules.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Security.Users
{
    public class UserParams : IParams
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? IdBranch { get; set; }

        public string User { get; set; }

        public string RoleName { get; set; }

    }
}
