﻿using LinqKit;
using Logic.Modules.Base;
using Persistence;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Entity = Persistence.Models.User;
using EntityDTO = Logic.Modules.Security.Users.UserDTO;
namespace Logic.Modules.Security.Users
{
    public class UserLogic
    {
        public UserLogic() 
        {
            this.roleLogic = new RoleLogic();
            this.permissionLogic = new PermissionLogic();
        }

        private RoleLogic roleLogic { get; }
        private PermissionLogic permissionLogic { get; }

        /// <summary>
        /// List of users filtered by <see cref="UserParams"/>
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IReadOnlyList<EntityDTO> QueryAsync(UserParams p)
        {
            using (var c = new Context())
            {
                return c.User
                    .Select(x => new EntityDTO
                    {
                        IdUser = x.IdUser,
                        Username = x.Username,
                        RoleName = x.Role.Name
                    })
                    .WhereContains(x => x.Username, p.User)
                    .WhereContains(x => x.RoleName, p.RoleName)
                    .ToList();
            }
        }

        public IReadOnlyList<EntityDTO> Query(int? idRole = null)
        {
            using (var c = new Context())
            {
                return c.User
                    .WhereEqualsNullable(x => x.IdRole, idRole)
                    .AsExpandable()
                    .Select(x => new EntityDTO
                    {
                        IdUser = x.IdUser,
                        Username = x.Username,
                        RoleName = x.Role.Name,
                        CreatedDate = x.CreatedDate,
                        FullName = c.Profile
                        .AsExpandable()
                        .Where(y=>y.IdUser == x.IdUser)
                        .Select(y=> ExpressionLogic.FullName.Invoke(y))
                        .FirstOrDefault(),
                    })
                    .ToList();
            }
        }

        /// <summary>
        /// True if the username is currentyl used
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public bool UserNameRepeated(string userName)
        {
            using (var c = new Context())
            {
                return c.User
                    .Where(x => x.Username.ToLower() == userName.ToLower())
                    .Any();
            }
        }

        /// <summary>
        /// Get the list of all the users in the database
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetAllUsers()
        {
            using (var c = new Context())
            {
                return c.User.ToList();
            }
        }

        /// <summary>
        /// Get the user given its ID
        /// </summary>
        /// <param name="c"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity Get(Context c, int id)
        {
            return c.User
                .Where(x => x.IdUser == id)
                .Include(x => x.Role)
                .FirstOrDefault();
        }

        public Profile GetUserProfile(Context c, int idUser)
        {
            return c.Profile
                .Where(x => x.IdUser == idUser)
                .FirstOrDefault();
        }

        public ProfileDTO GetUserProfileDTO(Context c, int idUser)
        {
            return c.Profile
                .Where(x => x.IdUser == idUser)
                .Select(x => new ProfileDTO 
                {
                    RoleName = x.User.Role.Name,
                    UserName = x.User.Username,
                    BasicInfo = x.BasicInfo
                })
                .FirstOrDefault();
        }

        /// <summary>
        /// Get the user's full name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetUserFullName(Context c, int id)
        {
            var user = c.Profile
                     .AsExpandable()
                     .Where(x => x.IdUser == id)
                     .Select(x => ExpressionLogic.FullName.Invoke(x))
                     .FirstOrDefault();

            return user;
        }

        public bool IsPasswordTheSame(int idUser, string password) 
        {
            Cryptography keys = new Cryptography();
            using (var c = new Context()) 
            {
                var userPassword = c.User
                    .Where(x => x.IdUser == idUser)
                    .Select(x => x.Password)
                    .FirstOrDefault();

                return (keys.Decrypt(userPassword) == password || userPassword == password);
            }
        }

        /// <summary>
        /// Add a user in the database
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Add(Context c, User entity)
        {
            c.User.Add(entity);
            c.SaveChanges();
        }

        /// <summary>
        /// Add a user's profile in the database
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Add(Context c, Profile entity)
        {
            c.Profile.Add(entity);
            c.SaveChanges();
        }

        /// <summary>
        /// Add an user in the database
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public void Add(string userName, string pass, int idRole)
        {
            using (var c = new Context())
            {
                var u = new User()
                {
                    Username = userName,
                    Password = pass,
                    IdRole = idRole,
                    CreatedDate = DateTime.Now
                };

                c.User.Add(u);
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Save the changes in the current context
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Update(Context c, Entity entity)
        {
            c.SaveChanges();
        }

        public void Update(Context c, Profile entity)
        {
            c.SaveChanges();
        }


        /// <summary>
        /// Remove an existing entity
        /// </summary>
        /// <param name="c"></param>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void Remove(int idKey)
        {
            using (var c = new Context())
            {
                var entity = Get(c, idKey);
                c.User.Remove(entity);
                c.SaveChanges();
            }
        }
    }
}