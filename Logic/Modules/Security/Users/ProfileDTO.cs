﻿using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Security.Users
{
    public class ProfileDTO: Profile
    {
        public string RoleName { get; set; }
        public string UserName { get; set; }
    }
}