﻿using Persistence;
using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity = Persistence.Models.Security.Permission;
namespace Logic.Modules.Security
{
    public class PermissionLogic
    {

        public Entity GetLoggedUserPermissions(int idUser)
        {
            using (var c = new Context())
            {
                var idRole = c.User
                    .Where(x => x.IdUser == idUser)
                    .Select(x => x.IdRole).FirstOrDefault();

                return GetPermission(c, idRole);
            }
        }


        public Permission GetPermission(Context c, int idRole)
        {
            return c.Permission.Where(x => x.IdRole == idRole).FirstOrDefault();
        }

        /// <summary>
        /// Add the permissions to the superadmin user
        /// </summary>
        /// <param name="c"></param>
        /// <param name="idRole"></param>
        /// <returns></returns>
        public void AddSuperAdminPermissions(Context c, int idRole) 
        {
            var entity = new Permission(true);
            entity.IdRole = idRole;
            Add(c, entity);
        }

        public void Add(Context c, Permission entity) 
        {
            //Save changes
            c.Permission.Add(entity);
            c.SaveChanges();
        }

        public void Update(Context c, Permission entity)
        {
            //Save changes
            c.SaveChanges();
        }
    }
}