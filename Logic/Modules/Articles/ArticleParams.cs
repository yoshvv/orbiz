﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Articles
{
    public class ArticleParams
    {
        public string Name { get; set; }

        public string Code { get; set; }
    }
}
