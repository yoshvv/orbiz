﻿using LinqKit;
using Logic.Modules.Base;
using Logic.Modules.Suppliers;
using Persistence;
using Persistence.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity = Persistence.Models.Article;
using Parameters = Logic.Modules.Articles.ArticleParams;
namespace Logic.Modules.Articles
{
    public class ArticleLogic
    {
        /// <summary>
        /// List of <see cref="Entity"/>
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public IReadOnlyList<Entity> Query(Parameters p)
        {
            using (var c = new Context())
            {
                return c.Article
                    .AsExpandable()
                    .WhereContains(x => x.Name, p.Name)
                    .WhereContains(x => x.Code+"", p.Code)
                    .ToList();
            }
        }

        public IReadOnlyList<Entity> Query()
        {
            var parameters = new Parameters()
            {
                Name = "",
                Code = "",
            };
            return Query(parameters);
        }

        /// <summary>
        /// Add a new entity of type <see cref="Entity"/> to the context
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Add(Context c, Entity entity)
        {
            c.Article.Add(entity);
            c.SaveChanges();
        }

        /// <summary>
        /// Save the changes in the current context
        /// </summary>
        /// <param name="c"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Update(Context c, Entity entity)
        {
            c.SaveChanges();
        }

        public void UpdateCollection(Context c, int idArticle, List<SupplierPrice> childs)
        {
            //Get the current list of childs
            var current = c.SupplierPrice
                .Where(x => x.IdArticle == idArticle)
                .ToList();

            //If there are no elements it means all the childs will be add, if not 
            //just removed or updated
            if (current.Any())
            {
                //Get the childs to remove
                var childsRemove = current
                    .Where(x => !childs
                                .Select(y => y.IdSupplierPrice)
                                .Contains(x.IdSupplierPrice))
                    .ToList();

                c.SupplierPrice.RemoveRange(childsRemove);

                //Get the childs to update
                var childsUpdate = current
                   .Where(x => childs
                               .Select(y => y.IdSupplierPrice)
                               .Contains(x.IdSupplierPrice))
                   .ToList();

                foreach (var child in childsUpdate)
                {
                    var curr = childs
                        .Where(x => x.IdSupplierPrice == child.IdSupplierPrice)
                        .FirstOrDefault();

                    child.PurchasePrice = curr.PurchasePrice;
                    child.SalePrice = curr.SalePrice;

                }

                //get the childs to add
                var childsToAdd = childs.Where(x => x.IdSupplierPrice == 0).ToList();
                if (childsToAdd.Any())
                {
                    c.SupplierPrice.AddRange(childsToAdd);
                }
            }
            else
            {
                c.SupplierPrice.AddRange(childs);
            }

            //Save changes
            c.SaveChanges();
        }

        /// <summary>
        /// Validate if a Article can be deleted
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CanDelete(int id)
        {
            using (var c = new Context())
            {
                return false;
            }
        }

        /// <summary>
        /// Delete the entity <see cref="Entity"/> given the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Delete(int id)
        {
            using (var c = new Context())
            {
                //getting the supplie's prices for this article
                var articlePrices = c.SupplierPrice
                    .Where(x=>x.IdArticle==id)
                    .ToList();

                //removing the supplier prices of the article
                c.SupplierPrice.RemoveRange(articlePrices);

                c.SaveChanges();

                //remove the Article
                c.Article.Remove(Get(c, id));
                //save changes in the database
                c.SaveChanges();
            }
        }

        /// <summary>
        /// Get the entity <see cref="Entity"/> given the id
        /// </summary>
        /// <param name="c"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity Get(Context c, int id)
        {
            return c.Article
                .Where(x => x.IdArticle == id)
                .FirstOrDefault()
                ;
        }

        public bool ArticleNameIsRepeated(int idKey, string name)
        {
            using (var c = new Context())
            {
                return c.Article
                    .Where(x => x.IdArticle != idKey)
                    .Where(x => x.Name.ToLower() == name.ToLower())
                    .Any();
            }
        }


        /// <summary>
        /// True if the code is repeated in entity <see cref="Article"/>
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool IsCodeRepeated(int code)
        {
            using (var c = new Context())
            {
                return c.Article
                    .Where(x => x.Code == code)
                    .Any();
            }
        }

        /// <summary>
        /// Returns the next available number in entity <see cref="Article"/>
        /// </summary>
        /// <returns></returns>
        public int NextCode()
        {
            using (var c = new Context())
            {
                var maxCode = c.Article.Select(x => (int?)x.Code).Max();

                if (maxCode == null) return 1;
                else return maxCode.Value + 1;
            }
        }
    }
}