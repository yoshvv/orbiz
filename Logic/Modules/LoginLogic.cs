﻿using Logic.Modules.Security;
using Logic.Modules.Security.Users;
using Persistence;
using Persistence.Models;
using Persistence.Models.Files;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules
{
    public class LoginLogic
    {
        public LoginLogic() 
        {
            this.userLogic = new UserLogic();
            this.roleLogic = new RoleLogic();
            this.permissionLogic = new PermissionLogic();
        }

        private UserLogic userLogic { get; }
        private RoleLogic roleLogic { get; }
        private PermissionLogic permissionLogic { get; }

        public void CheckSuperAdmin()
        {
            using (var c = new Context())
            {
                var superAdminQuery = c.User.Where(x => x.Username.ToLower() == "superadmin");
                var superAdminExists = superAdminQuery.Any();
                if (!superAdminExists)
                {
                    //Add the role
                    var searchFor = "superadmin";

                    var role = roleLogic.GetRole(c, searchFor);
                    var roleId = 0;

                    //Checking for superadmin's role
                    if (role == null)
                    {

                        roleLogic.Add(c, searchFor, "User with all the permissions.");
                        roleId = c.Role.Where(x => x.Name.ToLower() == searchFor).Select(x => x.IdRole).FirstOrDefault();
                    }
                    else
                    {
                        roleId = role.IdRole;
                    }

                    //Checking for superadmin permission's role
                    var permission = permissionLogic.GetPermission(c, roleId);

                    if (permission == null)
                    {
                        permissionLogic.AddSuperAdminPermissions(c, roleId);
                    }

                    //Add the user
                    userLogic.Add("Superadmin", "KFOqcmrVdBgQP8T4W4yubZeCSTHW7vLZF1O4Fd/aG6x6HTWRxjzxqAjJ47JJ4zdr", roleId);

                    //create user's profile entity
                    var user = c.User.FirstOrDefault(x => x.Username.ToLower() == "superadmin");

                    var profile = new Profile()
                    {
                        BasicInfo = new BasicInfo()
                        {
                            FirstName = "Super",
                            LastName = "Admin",
                            Avatar = new FileData()
                            {
                                File = null,
                                FileName = "",
                                Type = ""
                             }
                         }   
                    };
                    profile.IdUser = user.IdUser;

                    //save user's profile entity
                    userLogic.Add(c, profile);
                }
            }
        }

        /// <summary>
        /// Check if the username and password corresponds of an user
        /// in the database, and if true returns the ID, if false returns 0.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public int CheckLogin(string userName, string pass)
        {
            using (var c = new Context())
            {   
                var user = c.User
                    .Where(x => x.Username.ToLower() == userName.ToLower())
                    .FirstOrDefault();

                var userId = 0;

                if (user != null && userLogic.IsPasswordTheSame(user.IdUser, pass))
                {
                    userId = user.IdUser;
                }

                return userId;
            }
        }
    }
}