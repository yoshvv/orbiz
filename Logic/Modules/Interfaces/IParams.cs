﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Modules.Interfaces
{
    /// <summary>
    /// Interface to receive params in the <see cref="BaseEntity"/> class.
    /// </summary>
    public interface IParams
    {
        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }

        int? IdBranch { get; set; }
    }
}
