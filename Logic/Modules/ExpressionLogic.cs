﻿using Persistence.Models.Interfaces;
using System;
using System.Linq.Expressions;

namespace Logic.Modules
{
    public class ExpressionLogic
    {
        public static Expression<Func<IBasicInfo, string>> FullName = x => x.BasicInfo.FirstName + " " + x.BasicInfo.LastName;

        public static Expression<Func<IName, string>> Name = x => x.FirstName + " " + x.LastName;
    }
}
