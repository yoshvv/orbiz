﻿namespace Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationPurchaseDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PettyCash", "PurchaseDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PettyCash", "PurchaseDate");
        }
    }
}
