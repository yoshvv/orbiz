﻿namespace Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModInitialMenus : DbMigration
    {
        public override void Up()
        {
            //Adding principal menu
            Sql(@"INSERT INTO MenuItem(Name, DisplayName, Ordering)
                  VALUES('Article','Article',1),
                        ('Security','Security',2),
                        ('Supplier','Supplier',3),
                        ('Reports','Reports',4);");

            //Adding user and role sub menu items (children)
            Sql(@"INSERT INTO SubMenuItem(IdMenuItem, Name, DisplayName, Ordering) 
                VALUES((SELECT IdMenuItem FROM MenuItem WHERE Name = 'Security'), 'Users','Users',1),
                ((SELECT IdMenuItem FROM MenuItem WHERE Name = 'Security'), 'Roles','Roles',2),
                ((SELECT IdMenuItem FROM MenuItem WHERE Name = 'Security'), 'Configuration','Configuration',0),
                ((SELECT IdMenuItem FROM MenuItem WHERE Name = 'Reports'), 'UserReport','Users',1);");
        }

        public override void Down()
        {
        }
    }
}
