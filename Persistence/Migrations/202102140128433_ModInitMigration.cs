﻿namespace Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModInitMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Article",
                c => new
                    {
                        IdArticle = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Description = c.String(nullable: false, maxLength: 250, unicode: false),
                        VAT = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdArticle)
                .Index(t => t.Code, unique: true)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.SupplierPrice",
                c => new
                    {
                        IdSupplierPrice = c.Int(nullable: false, identity: true),
                        PurchasePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        IdArticle = c.Int(nullable: false),
                        IdSupplier = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdSupplierPrice)
                .ForeignKey("dbo.Supplier", t => t.IdSupplier)
                .ForeignKey("dbo.Article", t => t.IdArticle)
                .Index(t => new { t.IdArticle, t.IdSupplier }, unique: true, name: "IX_ArticleSupplierPrice");
            
            CreateTable(
                "dbo.Supplier",
                c => new
                    {
                        IdSupplier = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                        EMail = c.String(),
                        Address_AddressLine1 = c.String(),
                        Address_AddressLine2 = c.String(),
                        Address_ZipCode = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdSupplier)
                .Index(t => t.Code, unique: true)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Branch",
                c => new
                    {
                        IdBranch = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdBranch)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.GlobalConfiguration",
                c => new
                    {
                        IdGlobalConfiguration = c.Int(nullable: false, identity: true),
                        Logo_FileName = c.String(),
                        Logo_Type = c.String(),
                        Logo_File = c.Binary(),
                        ICO_FileName = c.String(),
                        ICO_Type = c.String(),
                        ICO_File = c.Binary(),
                    })
                .PrimaryKey(t => t.IdGlobalConfiguration);
            
            CreateTable(
                "dbo.MenuItem",
                c => new
                    {
                        IdMenuItem = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        DisplayName = c.String(nullable: false),
                        Ordering = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdMenuItem)
                .Index(t => t.Name, unique: true)
                .Index(t => t.Ordering, unique: true);
            
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        IdPermission = c.Int(nullable: false, identity: true),
                        IdRole = c.Int(nullable: false),
                        Article_Visualize = c.Boolean(nullable: false),
                        Article_Add = c.Boolean(nullable: false),
                        Article_Modify = c.Boolean(nullable: false),
                        Article_Remove = c.Boolean(nullable: false),
                        Branch_Visualize = c.Boolean(nullable: false),
                        Branch_Add = c.Boolean(nullable: false),
                        Branch_Modify = c.Boolean(nullable: false),
                        Branch_Remove = c.Boolean(nullable: false),
                        Database_Visualize = c.Boolean(nullable: false),
                        Database_Backup = c.Boolean(nullable: false),
                        Database_Restore = c.Boolean(nullable: false),
                        Configuration_Visualize = c.Boolean(nullable: false),
                        Configuration_Modify = c.Boolean(nullable: false),
                        Menu_Visualize = c.Boolean(nullable: false),
                        Menu_Add = c.Boolean(nullable: false),
                        Menu_Modify = c.Boolean(nullable: false),
                        Menu_Remove = c.Boolean(nullable: false),
                        Roles_Visualize = c.Boolean(nullable: false),
                        Roles_Add = c.Boolean(nullable: false),
                        Roles_Modify = c.Boolean(nullable: false),
                        Roles_Remove = c.Boolean(nullable: false),
                        Supplier_Visualize = c.Boolean(nullable: false),
                        Supplier_Add = c.Boolean(nullable: false),
                        Supplier_Modify = c.Boolean(nullable: false),
                        Supplier_Remove = c.Boolean(nullable: false),
                        User_Visualize = c.Boolean(nullable: false),
                        User_Add = c.Boolean(nullable: false),
                        User_Modify = c.Boolean(nullable: false),
                        User_Remove = c.Boolean(nullable: false),
                        UserReport_Visualize = c.Boolean(nullable: false),
                        UserReport_Generate = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPermission)
                .ForeignKey("dbo.Role", t => t.IdRole)
                .Index(t => t.IdRole, unique: true, name: "IX_PermissionRole");
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        IdRole = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Description = c.String(maxLength: 250, unicode: false),
                    })
                .PrimaryKey(t => t.IdRole)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Profile",
                c => new
                    {
                        IdProfile = c.Int(nullable: false, identity: true),
                        IdUser = c.Int(nullable: false),
                        BasicInfo_FirstName = c.String(nullable: false, maxLength: 50, unicode: false),
                        BasicInfo_LastName = c.String(nullable: false, maxLength: 50, unicode: false),
                        BasicInfo_BirthDate = c.DateTime(),
                        BasicInfo_Email = c.String(),
                        BasicInfo_Phone = c.String(),
                        BasicInfo_Avatar_FileName = c.String(),
                        BasicInfo_Avatar_Type = c.String(),
                        BasicInfo_Avatar_File = c.Binary(),
                    })
                .PrimaryKey(t => t.IdProfile)
                .ForeignKey("dbo.User", t => t.IdUser)
                .Index(t => t.IdUser, unique: true);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        IdUser = c.Int(nullable: false, identity: true),
                        IdRole = c.Int(nullable: false),
                        Username = c.String(nullable: false, maxLength: 50, unicode: false),
                        Password = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdUser)
                .ForeignKey("dbo.Role", t => t.IdRole)
                .Index(t => t.IdRole)
                .Index(t => t.Username, unique: true);
            
            CreateTable(
                "dbo.SubMenuItem",
                c => new
                    {
                        IdSubMenuItem = c.Int(nullable: false, identity: true),
                        IdMenuItem = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        DisplayName = c.String(nullable: false),
                        Ordering = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdSubMenuItem)
                .ForeignKey("dbo.MenuItem", t => t.IdMenuItem)
                .Index(t => new { t.IdMenuItem, t.Name, t.Ordering }, unique: true, name: "IX_SubMenuOrder");
            
            CreateTable(
                "dbo.UserBranch",
                c => new
                    {
                        IdUserBranch = c.Int(nullable: false, identity: true),
                        IdBranch = c.Int(nullable: false),
                        IdUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdUserBranch)
                .ForeignKey("dbo.Branch", t => t.IdBranch)
                .ForeignKey("dbo.User", t => t.IdUser)
                .Index(t => new { t.IdBranch, t.IdUser }, unique: true, name: "IX_UserBranch");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserBranch", "IdUser", "dbo.User");
            DropForeignKey("dbo.UserBranch", "IdBranch", "dbo.Branch");
            DropForeignKey("dbo.SubMenuItem", "IdMenuItem", "dbo.MenuItem");
            DropForeignKey("dbo.Profile", "IdUser", "dbo.User");
            DropForeignKey("dbo.User", "IdRole", "dbo.Role");
            DropForeignKey("dbo.Permission", "IdRole", "dbo.Role");
            DropForeignKey("dbo.SupplierPrice", "IdArticle", "dbo.Article");
            DropForeignKey("dbo.SupplierPrice", "IdSupplier", "dbo.Supplier");
            DropIndex("dbo.UserBranch", "IX_UserBranch");
            DropIndex("dbo.SubMenuItem", "IX_SubMenuOrder");
            DropIndex("dbo.User", new[] { "Username" });
            DropIndex("dbo.User", new[] { "IdRole" });
            DropIndex("dbo.Profile", new[] { "IdUser" });
            DropIndex("dbo.Role", new[] { "Name" });
            DropIndex("dbo.Permission", "IX_PermissionRole");
            DropIndex("dbo.MenuItem", new[] { "Ordering" });
            DropIndex("dbo.MenuItem", new[] { "Name" });
            DropIndex("dbo.Branch", new[] { "Name" });
            DropIndex("dbo.Supplier", new[] { "Name" });
            DropIndex("dbo.Supplier", new[] { "Code" });
            DropIndex("dbo.SupplierPrice", "IX_ArticleSupplierPrice");
            DropIndex("dbo.Article", new[] { "Name" });
            DropIndex("dbo.Article", new[] { "Code" });
            DropTable("dbo.UserBranch");
            DropTable("dbo.SubMenuItem");
            DropTable("dbo.User");
            DropTable("dbo.Profile");
            DropTable("dbo.Role");
            DropTable("dbo.Permission");
            DropTable("dbo.MenuItem");
            DropTable("dbo.GlobalConfiguration");
            DropTable("dbo.Branch");
            DropTable("dbo.Supplier");
            DropTable("dbo.SupplierPrice");
            DropTable("dbo.Article");
        }
    }
}
