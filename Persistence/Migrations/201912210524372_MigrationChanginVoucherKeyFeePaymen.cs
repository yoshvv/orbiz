﻿namespace Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationChanginVoucherKeyFeePaymen : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.FeePayment", name: "IdVoucher", newName: "IdVoucherApplied");
            RenameIndex(table: "dbo.FeePayment", name: "IX_IdVoucher", newName: "IX_IdVoucherApplied");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.FeePayment", name: "IX_IdVoucherApplied", newName: "IX_IdVoucher");
            RenameColumn(table: "dbo.FeePayment", name: "IdVoucherApplied", newName: "IdVoucher");
        }
    }
}
