﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Models.Security
{
    public class Permission
    {
        public Permission() { }

        public Permission(bool isAdmin) 
        {
            //All permissions true
            ModulePermission AllPermissionsTrue() 
            {
                return new ModulePermission()
                {
                Add = true,
                Modify = true,
                Remove = true,
                Visualize = true
                };
            }

            //all report's permissions true
            ReportPermission AllReportTrue()
            {
                return new ReportPermission()
                {
                    Visualize = true,
                    Generate = true
                };
            }

            ConfigurationPermission AllConfigurationTrue()
            {
                return new ConfigurationPermission()
                {
                    Visualize = true,
                    Modify = true
                };
            }

            DatabaseManagement AllDatabaseManagementTrue()
            {
                return new DatabaseManagement()
                {
                    Visualize = true,
                    Backup = true,
                    Restore = true
                };
            }
            
            this.Article = AllPermissionsTrue();
            this.Branch = AllPermissionsTrue();
            this.Database = AllDatabaseManagementTrue();
            this.Configuration = AllConfigurationTrue();
            this.User = AllPermissionsTrue();
            this.Menu = AllPermissionsTrue();
            this.Roles = AllPermissionsTrue();
            this.Supplier = AllPermissionsTrue();
           

            /*REPORTS*/
            this.UserReport = AllReportTrue();
        }

        [Key]
        public int IdPermission { get; set; }

        /// <summary>
        /// Foreign key to role table
        /// </summary>
        [ForeignKey(nameof(Role))]
        [Index("IX_PermissionRole", Order = 1, IsUnique = true)]
        public int IdRole { get; set; }
        public Role Role { get; set; }


        /*****************************/
        /**********FORMS************/
        /*****************************/
        public ModulePermission Article { get; set; } = new ModulePermission();

        public ModulePermission Branch { get; set; } = new ModulePermission();

        public DatabaseManagement Database { get; set; } = new DatabaseManagement();

        public ConfigurationPermission Configuration { get; set; } = new ConfigurationPermission();

        public ModulePermission Menu { get; set; } = new ModulePermission();

        public ModulePermission Roles { get; set; } = new ModulePermission();

        public ModulePermission Supplier { get; set; } = new ModulePermission();

        public ModulePermission User { get; set; } = new ModulePermission();


        /*****************************/
        /**********REPORTS************/
        /*****************************/
        public ReportPermission UserReport { get; set; } = new ReportPermission();
    }

    [ComplexType]
    public class ModulePermission 
    {
        /// <summary>
        /// Permission to see the module
        /// </summary>
        public bool Visualize { get; set; }

        /// <summary>
        /// Permission to add a new entity
        /// </summary>
        public bool Add { get; set; }

        /// <summary>
        /// Permission to modify an existing entity
        /// </summary>
        public bool Modify { get; set; }

        /// <summary>
        /// Permission to delete an existing entity
        /// </summary>
        public bool Remove { get; set; }
    }

    [ComplexType]
    public class ConfigurationPermission
    {
        /// <summary>
        /// Permission to see the module
        /// </summary>
        public bool Visualize { get; set; }

        /// <summary>
        /// Permission to modify an existing entity
        /// </summary>
        public bool Modify { get; set; }
    }

    [ComplexType]
    public class DatabaseManagement
    {
        /// <summary>
        /// Permission to see the database management module
        /// </summary>
        public bool Visualize { get; set; }

        /// <summary>
        /// Permission to create the backup of the current database
        /// </summary>
        public bool Backup { get; set; }

        /// <summary>
        /// Permission to restore a database
        /// </summary>
        public bool Restore { get; set; }
    }

    [ComplexType]
    public class ReportPermission
    {
        /// <summary>
        /// Permission to see the module of the report
        /// </summary>
        public bool Visualize { get; set; }

        /// <summary>
        /// Permission to generate a new report
        /// </summary>
        public bool Generate { get; set; }
    }
}