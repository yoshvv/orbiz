﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Security
{
    public class Role
    {
        [Key]
        public int IdRole { get; set; }

        /// <summary>
        /// Name of the role
        /// </summary>
        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string Description { get; set; }
    }
}