﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Models.Locations
{
    [ComplexType]
    public class AddressInfo
    {
        /// <summary>
        /// Street address line 1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Street address for extra information
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Zip code
        /// </summary>
        public string ZipCode { get; set; }
    }
}