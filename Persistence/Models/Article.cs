﻿using Persistence.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Article
    {
        [Key]
        public int IdArticle { get; set; }

        [Index(IsUnique = true)]
        [Required]
        public int Code { get; set; }

        /// <summary>
        /// Name of this article
        /// </summary>
        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = true)]
        [StringLength(250)]
        public string Description { get; set; }

        public decimal VAT { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// List of prices for every different supplier that sells this article
        /// </summary>
        [InverseProperty(nameof(SupplierPrice.Article))]
        public List<SupplierPrice> SupplierPrices { get; set; }
    }
}