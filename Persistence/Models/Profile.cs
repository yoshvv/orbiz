﻿using Persistence.Models.Files;
using Persistence.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Profile: IBasicInfo
    {
        [Key]
        public int IdProfile { get; set; }

        /// <summary>
        /// Foreign key to user's table
        /// </summary>
        [ForeignKey(nameof(User))]
        [Index(IsUnique = true)]
        public int IdUser { get; set; }
        public User User { get; set; }

        /// <summary>
        /// Basic info of the profile
        /// </summary>
        public BasicInfo BasicInfo { get; set; }
    }

    [ComplexType]
    public class BasicInfo
    {
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// Date of birth with format dd-mm-yyyy
        /// </summary>
        public DateTime? BirthDate { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        /// <summary>
        /// Avatar's profile
        /// </summary>
        public FileData Avatar { get; set; }
    }
}