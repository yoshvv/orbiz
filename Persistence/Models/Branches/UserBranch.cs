﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Branches
{
    /// <summary>
    /// Branches where the user has access
    /// </summary>
    public class UserBranch
    {
        [Key]
        public int IdUserBranch { get; set; }

        [ForeignKey(nameof(Branch))]
        [Index("IX_UserBranch", Order = 1, IsUnique = true)]
        public int IdBranch { get; set; }
        public Branch Branch { get; set; }

        [ForeignKey(nameof(User))]
        [Index("IX_UserBranch", Order = 2, IsUnique = true)]
        public int IdUser { get; set; }
        public User User { get; set; }
    }
}
