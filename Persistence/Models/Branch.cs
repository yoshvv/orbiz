﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class Branch
    {
        /// <summary>
        /// ID key of this table
        /// </summary>
        [Key]
        public int IdBranch { get; set; }

        /// <summary>
        /// Name of this branch
        /// </summary>
        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}