﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Interfaces
{
    public interface IName
    {
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
