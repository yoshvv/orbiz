﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Interfaces
{
    public interface ICashFlow
    {
        /// <summary>
        /// Date then this cash flow was created
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Details of this cash flow
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Value of this cash flow
        /// </summary>
        decimal Value { get; set; }
    }
}