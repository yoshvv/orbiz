﻿using Persistence.Models.Files;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Configuration
{
    public class GlobalConfiguration
    {
        [Key]
        public int IdGlobalConfiguration { get; set; }

        public string AppName { get; set; }

        /// <summary>
        /// Logotype
        /// </summary>
        public FileData Logo { get; set; }

        /// <summary>
        /// Icon for every window
        /// </summary>
        public FileData ICO { get; set; }
    }
}
