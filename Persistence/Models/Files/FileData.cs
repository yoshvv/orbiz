﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Files
{
    [ComplexType]
    public class FileData
    {
        /// <summary>
        /// Name of the file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Format of the file
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// File as an array of bytes
        /// </summary>
        public byte[] File { get; set; }
    }
}
