﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Models.Suppliers
{
    public class SupplierPrice
    {
        [Key]
        public int IdSupplierPrice { get; set; }

        public decimal PurchasePrice { get; set; }

        public decimal SalePrice { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Id of the article where this price belongs
        /// </summary>
        [ForeignKey(nameof(Article))]
        [Index("IX_ArticleSupplierPrice", 0, IsUnique = true)]
        public int IdArticle { get; set; }
        public Article Article { get; set; }

        /// <summary>
        /// Id of the supplier who applies this price for
        /// </summary>
        [ForeignKey(nameof(Supplier))]
        [Index("IX_ArticleSupplierPrice", 1, IsUnique = true)]
        public int IdSupplier { get; set; }
        public Supplier Supplier { get; set; }
    }
}