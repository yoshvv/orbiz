﻿using Persistence.Models.Interfaces;
using Persistence.Models.Locations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models.Suppliers
{
    public class Supplier : IName
    {
        [Key]
        public int IdSupplier { get; set; }

        [Index(IsUnique = true)]
        [Required]
        public int Code { get; set; }

        /// <summary>
        /// Name of this supplier
        /// </summary>
        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string EMail { get; set; }

        public AddressInfo Address { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
