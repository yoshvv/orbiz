﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Models
{
    public class MenuItem
    {
        [Key]
        public int IdMenuItem { get; set; }

        /// <summary>
        /// Unique name of the menu item
        /// </summary>
        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Name that will be display in the menu and in the permission's windows
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        /// <summary>
        /// Custom order of the menu
        /// </summary>
        [Index(IsUnique = true)]
        public int Ordering { get; set; }
    }
}