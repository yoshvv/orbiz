﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class SubMenuItem
    {
        [Key]
        public int IdSubMenuItem { get; set; }

        /// <summary>
        /// Foreign key to menu item table
        /// </summary>
        [ForeignKey(nameof(MenuItem))]
        [Index("IX_SubMenuOrder", Order = 1, IsUnique = true)]
        public int IdMenuItem { get; set; }
        public MenuItem MenuItem { get; set; }

        /// <summary>
        /// Unique name of the sub menu item
        /// </summary>
        [Index("IX_SubMenuOrder", Order = 2, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Name that will be display in the sub menu and in the permission's windows
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string DisplayName { get; set; }

        /// <summary>
        /// Custom order of the sub menu
        /// </summary>
        [Index("IX_SubMenuOrder", Order = 3, IsUnique = true)]
        public int Ordering { get; set; }
    }
}
