﻿using Persistence.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Models
{
    public class User
    {
        [Key]
        public int IdUser { get; set; }

        [ForeignKey(nameof(Role))]
        public int IdRole { get; set; }
        public Role Role { get; set; }

        [Index(IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
