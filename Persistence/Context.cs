﻿using Persistence.Models;
using Persistence.Models.Branches;
using Persistence.Models.Configuration;
using Persistence.Models.Security;
using Persistence.Models.Suppliers;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Persistence
{
    public class Context: DbContext
    {
        public Context() : base("name=Context")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Persistence.Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //We don't want to pluralize our tables
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Remove deleting on cascade
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }

        public DbSet<Article> Article { get; set; }

        public DbSet<Branch> Branch { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<UserBranch> UserBranch { get; set; }

        public DbSet<Profile> Profile { get; set; }

        public DbSet<GlobalConfiguration> GlobalConfiguration { get; set; }

        public DbSet<Role> Role { get; set; }

        public DbSet<Permission> Permission { get; set; }

        public DbSet<MenuItem> MenuItem { get; set; }

        public DbSet<SubMenuItem> SubMenuItem { get; set; }

        public DbSet<Supplier> Supplier { get; set; }

        public DbSet<SupplierPrice> SupplierPrice { get; set; }
    }
}
