﻿using Logic.Modules.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Testing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Decrypt()
        {
            var crypt = new Cryptography();

            var pass = crypt.Decrypt("KFOqcmrVdBgQP8T4W4yubZeCSTHW7vLZF1O4Fd/aG6x6HTWRxjzxqAjJ47JJ4zdr");

            Assert.IsTrue(pass == "superadmin123456");
        }
    }
}
